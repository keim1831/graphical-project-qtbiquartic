#pragma once

#include "../Core/LinearCombination3.h"


namespace cagd
{
    class BiquarticArc3: public LinearCombination3
    {
    public:
        // special constructor
        BiquarticArc3();

        // redeclare and define inherited pure virtual methods
        GLboolean BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const override;
        GLboolean CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const override;

        GLboolean RenderSelectedPoint(GLuint point_selected) const;
    };
}
