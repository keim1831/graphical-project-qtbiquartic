#include"BiquarticPatch3.h"

namespace cagd
{
    BiquarticPatch3::BiquarticPatch3():
        TensorProductSurface3(0.0, 1.0, 0.0, 1.0, 4, 4)
    {

    }

    GLboolean BiquarticPatch3::UBlendingFunctionValues(GLdouble u_knot, RowMatrix<GLdouble> &blending_values) const
    {
        if (u_knot < 0.0 || u_knot > 1.0)
            return GL_FALSE;
        blending_values.ResizeColumns(4);

        GLdouble u2 = u_knot * u_knot, u3 = u2 * u_knot, u4 = u2 * u2;
        GLdouble w = (1.0 - u_knot);

        blending_values[0] = w * w * w * w;
        blending_values[1] = -u4 + 6.0 * u3 - 9.0 * u2 + 4.0 * u_knot;
        blending_values[2] = -u4 - 2.0 * u3 + 3.0 * u2;
        blending_values[3] = u4;

        return GL_TRUE;
    }

    GLboolean BiquarticPatch3::VBlendingFunctionValues(GLdouble v_knot, RowMatrix<GLdouble> &blending_values) const
    {
        if (v_knot < 0.0 || v_knot > 1.0)
            return GL_FALSE;
        blending_values.ResizeColumns(4);

        GLdouble v2 = v_knot * v_knot, v3 = v2 * v_knot, v4 = v2 * v2;
        GLdouble w = (1.0 - v_knot);

        blending_values[0] = w * w * w * w;
        blending_values[1] = -v4 + 6.0 * v3 - 9.0 * v2 + 4.0 * v_knot;
        blending_values[2] = -v4 - 2.0 * v3 + 3.0 * v2;
        blending_values[3] = v4;

        return GL_TRUE;
    }

    GLboolean BiquarticPatch3::CalculatePartialDerivatives(GLuint maximum_order_of_partial_derivatives, GLdouble u, GLdouble v, PartialDerivatives &pd) const
    {
        if (u < 0.0 || u > 1.0 || v < 0.0 || v > 1.0 || maximum_order_of_partial_derivatives > 1)
            return GL_FALSE;

        RowMatrix<GLdouble> u_blending_values(4), d1_u_blending_values(4);
        GLdouble u2 = u * u, u3 = u2 * u, u4 = u2 * u2;
        GLdouble wu = (1.0 - u);

        u_blending_values[0] = wu * wu * wu * wu;
        u_blending_values[1] = -u4 + 6.0 * u3 - 9.0 * u2 + 4.0 * u;
        u_blending_values[2] = -u4 - 2.0 * u3 + 3.0 * u2;
        u_blending_values[3] = u4;

        d1_u_blending_values[0] = -4.0 * wu * wu * wu;
        d1_u_blending_values[1] = -4.0 * u3 + 18.0 * u2 - 18.0 * u + 4.0;
        d1_u_blending_values[2] = -4.0 * u3 - 6.0 * u2 + 6.0 * u;
        d1_u_blending_values[3] = 4.0 * u3;

        RowMatrix<GLdouble> v_blending_values(4), d1_v_blending_values(4);
        GLdouble v2 = v * v, v3 = v2 * v, v4 = v2 * v2;
        GLdouble wv = (1.0 - v);

        v_blending_values[0] = wv * wv * wv * wv;
        v_blending_values[1] = -v4 + 6.0 * v3 - 9.0 * v2 + 4.0 * v;
        v_blending_values[2] = -v4 - 2.0 * v3 + 3.0 * v2;
        v_blending_values[3] = v4;

        d1_v_blending_values[0] = -4.0 * wv * wv * wv;
        d1_v_blending_values[1] = -4.0 * v3 + 18.0 * v2 - 18.0 * v + 4.0;
        d1_v_blending_values[2] = -4.0 * v3 - 6.0 * v2 + 6.0 * v;
        d1_v_blending_values[3] = 4.0 * v3;

        pd.ResizeRows(2);
        pd.LoadNullVectors();

        for (GLuint i = 0; i < 4; ++i)
        {
            DCoordinate3 aux_d0_v, aux_d1_v;
            for (GLuint j = 0; j < 4; ++j)
            {
                aux_d0_v += _data(i, j) * v_blending_values(j);
                aux_d1_v += _data(i, j) * d1_v_blending_values(j);
            }

            pd(0, 0) += aux_d0_v * u_blending_values(i);
            pd(1, 0) += aux_d0_v * d1_u_blending_values(i);
            pd(1, 1) += aux_d1_v * u_blending_values(i);
        }

        return GL_TRUE;
    }

    GLboolean BiquarticPatch3::RenderDataPoints(GLenum render_mode) const
    {
        if (!_vbo_data)
            return GL_FALSE;

        if (render_mode != GL_LINE_STRIP && render_mode != GL_LINE_LOOP && render_mode != GL_POINTS)
            return GL_FALSE;

        glEnableClientState(GL_VERTEX_ARRAY);
            glBindBuffer(GL_ARRAY_BUFFER, _vbo_data);
                glVertexPointer(3, GL_FLOAT, 0, (const GLvoid*)0);

                glPointSize(5.0);
                glDrawArrays(GL_POINTS, 0, _data.GetRowCount() * _data.GetColumnCount());

                for (GLuint i = 0; i < _data.GetRowCount(); ++i)
                {
                    glDrawArrays(render_mode, _data.GetRowCount() * i, _data.GetColumnCount());
                }

                for (GLuint i = 0; i < _data.GetColumnCount(); ++i)
                {
                    glDrawArrays(render_mode, _data.GetRowCount() * _data.GetColumnCount() + i * _data.GetColumnCount(), _data.GetRowCount());
                }
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDisableClientState(GL_VERTEX_ARRAY);

        return GL_TRUE;
    }

    GLboolean BiquarticPatch3::RenderSelectedPoint(GLuint row, GLuint col) const
    {
        if (!_vbo_data)
            return GL_FALSE;

        glBegin(GL_POINTS);
        glVertex3dv(&_data(row, col)[0]);
        glEnd();

        return GL_TRUE;
    }
}
