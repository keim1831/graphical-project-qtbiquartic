#include "BiquarticCompositeSurfaces3.h"

namespace cagd
{
    GLvoid BiquarticCompositeSurface3::join_NN(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;

        for(GLuint i = 0; i < 4; i++)
        {
            // elso sor
            _attributes[patch_ind1].patch->GetData(0, 3-i, pointsHelp);
            _attributes[newIndex].patch->SetData(0, i, pointsHelp);

            // masodik sor
            _attributes[patch_ind1].patch->GetData(1, 3-i, pointsHelp2);
            _attributes[newIndex].patch->SetData(1, i, 2 * pointsHelp - pointsHelp2);

            // harmadik sor
            _attributes[patch_ind2].patch->GetData(0, i, pointsHelp);
            _attributes[patch_ind2].patch->GetData(1, i, pointsHelp2);
            _attributes[newIndex].patch->SetData(2, i, 2* pointsHelp - pointsHelp2);

            // utolso sor
            _attributes[newIndex].patch->SetData(3, i, pointsHelp);
        }
        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[N] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[S] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[N] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[N] = &_attributes[newIndex];


        _attributes[newIndex].neighboursDir[N] = N;
        _attributes[newIndex].neighboursDir[S] = N;

        _attributes[patch_ind1].neighboursDir[N] = N;
        _attributes[patch_ind2].neighboursDir[N] = S;

        cout << "N JOIN N SUCCESFULL";
    }

    GLvoid BiquarticCompositeSurface3::join_SS(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;

        cout << "NN\n";
        for(GLuint i = 0; i < 4; i++)
        {
            // elso sor
            _attributes[patch_ind1].patch->GetData(3, i, pointsHelp);
            _attributes[newIndex].patch->SetData(0, i, pointsHelp);

            // masodik sor
            _attributes[patch_ind1].patch->GetData(2, i, pointsHelp2);
            _attributes[newIndex].patch->SetData(1, i, 2 * pointsHelp - pointsHelp2);

            // harmadik sor
            _attributes[patch_ind2].patch->GetData(3, 3-i, pointsHelp);
            _attributes[patch_ind2].patch->GetData(2, 3-i, pointsHelp2);
            _attributes[newIndex].patch->SetData(2, i, 2* pointsHelp - pointsHelp2);

            // utolso sor
            _attributes[newIndex].patch->SetData(3, i, pointsHelp);
        }
        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[N] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[S] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[S] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[S] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[N] = S;
        _attributes[newIndex].neighboursDir[S] = S;

        _attributes[patch_ind1].neighboursDir[S] = N;
        _attributes[patch_ind2].neighboursDir[S] = S;

        cout << "N JOIN N SUCCESFULL";
    }

    GLvoid BiquarticCompositeSurface3::join_EE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;

        for(GLuint i = 0; i < 4; i++)
        {
            // elso oszlop
            _attributes[patch_ind1].patch->GetData(i, 3, pointsHelp);
            _attributes[newIndex].patch->SetData(i, 0, pointsHelp);

            // masodik oszlop
            _attributes[patch_ind1].patch->GetData(i, 2, pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 1, 2 * pointsHelp - pointsHelp2);

            // harmadik oszlop
            _attributes[patch_ind2].patch->GetData(3-i, 3, pointsHelp);
            _attributes[patch_ind2].patch->GetData(3-i, 2, pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 2, 2* pointsHelp - pointsHelp2);

            cout << "third Row OK\n";

            // az utolso sorat beallitom a masodik folt soranak adataival
            _attributes[newIndex].patch->SetData(i, 3, pointsHelp);
        }
        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[W] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[E] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[E] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[E] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[W] = E;
        _attributes[newIndex].neighboursDir[E] = E;

        _attributes[patch_ind1].neighboursDir[E] = W;
        _attributes[patch_ind2].neighboursDir[E] = E;

        cout << "N JOIN N SUCCESFULL";
    }

    GLvoid BiquarticCompositeSurface3::join_WW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;

        for(GLuint i = 0; i < 4; i++)
        {
            // elso oszlop
            _attributes[patch_ind1].patch->GetData(3-i, 0, pointsHelp);
            _attributes[newIndex].patch->SetData(i, 0, pointsHelp);

            // masodik oszlop
            _attributes[patch_ind1].patch->GetData(3-i, 1, pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 1, 2 * pointsHelp - pointsHelp2);

            // harmadik oszlop
            _attributes[patch_ind2].patch->GetData(i, 0, pointsHelp);
            _attributes[patch_ind2].patch->GetData(i, 1, pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 2, 2* pointsHelp - pointsHelp2);

            cout << "third Row OK\n";

            // az utolso sorat beallitom a masodik folt soranak adataival
            _attributes[newIndex].patch->SetData(i, 3, pointsHelp);
        }
        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[W] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[E] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[W] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[W] = &_attributes[newIndex];


        _attributes[newIndex].neighboursDir[W] = W;
        _attributes[newIndex].neighboursDir[E] = W;

        _attributes[patch_ind1].neighboursDir[W] = W;
        _attributes[patch_ind2].neighboursDir[W] = E;

        cout << "N JOIN N SUCCESFULL";
    }

    GLvoid BiquarticCompositeSurface3::join_NS(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind1].patch->GetData(0, i, pointsHelp);
            _attributes[newIndex].patch->SetData(3, i, pointsHelp);

            cout << "first Row OK\n";

            // a masodik sorat beallitom az elso folt soranak adatai 2*esevel - a 2ik sor
            _attributes[patch_ind1].patch->GetData(1, i, pointsHelp2);
            _attributes[newIndex].patch->SetData(2, i, 2 * pointsHelp - pointsHelp2);

            cout << "second Row OK\n";

            // a harmadik sorat beallitom a masodik folt soranak adatai *2 vel
            _attributes[patch_ind2].patch->GetData(3, i, pointsHelp);
            _attributes[patch_ind2].patch->GetData(2, i, pointsHelp2);
            _attributes[newIndex].patch->SetData(1, i, 2* pointsHelp - pointsHelp2);

            cout << "third Row OK\n";

            // az utolso sorat beallitom a masodik folt soranak adataival
            _attributes[newIndex].patch->SetData(0, i, pointsHelp);
        }
        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[S] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[N] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[N] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[S] = &_attributes[newIndex];


        _attributes[newIndex].neighboursDir[S] = N;
        _attributes[newIndex].neighboursDir[N] = S;

        _attributes[patch_ind1].neighboursDir[N] = S;
        _attributes[patch_ind2].neighboursDir[S] = N;

        cout << "N JOIN N SUCCESFULL";
    }

    GLvoid BiquarticCompositeSurface3::join_NE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind1].patch->GetData(0, i, pointsHelp);
            _attributes[newIndex].patch->SetData(3, i, pointsHelp);

            cout << "first Row OK\n";

            // a masodik sorat beallitom az elso folt soranak adatai 2*esevel - a 2ik sor
            _attributes[patch_ind1].patch->GetData(1, i, pointsHelp2);
            _attributes[newIndex].patch->SetData(2, i, 2 * pointsHelp - pointsHelp2);

            cout << "second Row OK\n";

            // a harmadik sorat beallitom a masodik folt soranak adatai *2 vel
            _attributes[patch_ind2].patch->GetData(3-i, 3, pointsHelp);
            _attributes[patch_ind2].patch->GetData(3-i, 2, pointsHelp2);
            _attributes[newIndex].patch->SetData(1, i, 2* pointsHelp - pointsHelp2);

            cout << "third Row OK\n";

            // az utolso sorat beallitom a masodik folt soranak adataival
            _attributes[newIndex].patch->SetData(0, i, pointsHelp);
        }
        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[S] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[N] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[N] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[E] = &_attributes[newIndex];


        _attributes[newIndex].neighboursDir[S] = N;
        _attributes[newIndex].neighboursDir[N] = E;

        _attributes[patch_ind1].neighboursDir[N] = S;
        _attributes[patch_ind2].neighboursDir[E] = N;

        cout << "N JOIN N SUCCESFULL";
    }

    GLvoid BiquarticCompositeSurface3::join_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind1].patch->GetData(0, i, pointsHelp);
            _attributes[newIndex].patch->SetData(3, i, pointsHelp);

            cout << "first Row OK\n";

            // a masodik sorat beallitom az elso folt soranak adatai 2*esevel - a 2ik sor
            _attributes[patch_ind1].patch->GetData(1, i, pointsHelp2);
            _attributes[newIndex].patch->SetData(2, i, 2 * pointsHelp - pointsHelp2);

            cout << "second Row OK\n";

            // a harmadik sorat beallitom a masodik folt soranak adatai *2 vel
            _attributes[patch_ind2].patch->GetData(i, 0, pointsHelp);
            _attributes[patch_ind2].patch->GetData(i, 1, pointsHelp2);
            _attributes[newIndex].patch->SetData(1, i, 2* pointsHelp - pointsHelp2);

            cout << "third Row OK\n";

            // az utolso sorat beallitom a masodik folt soranak adataival
            _attributes[newIndex].patch->SetData(0, i, pointsHelp);
        }
        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[S] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[N] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[N] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[W] = &_attributes[newIndex];


        _attributes[newIndex].neighboursDir[S] = N;
        _attributes[newIndex].neighboursDir[N] = W;

        _attributes[patch_ind1].neighboursDir[N] = S;
        _attributes[patch_ind2].neighboursDir[W] = N;

        cout << "N JOIN N SUCCESFULL";
    }

    GLvoid BiquarticCompositeSurface3::join_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind1].patch->GetData(3, i, pointsHelp);
            _attributes[newIndex].patch->SetData(0, i, pointsHelp);

            cout << "first Row OK\n";

            // a masodik sorat beallitom az elso folt soranak adatai 2*esevel - a 2ik sor
            _attributes[patch_ind1].patch->GetData(2, i, pointsHelp2);
            _attributes[newIndex].patch->SetData(1, i, 2 * pointsHelp - pointsHelp2);

            cout << "second Row OK\n";

            // a harmadik sorat beallitom a masodik folt soranak adatai *2 vel
            _attributes[patch_ind2].patch->GetData(i, 3, pointsHelp);
            _attributes[patch_ind2].patch->GetData(i, 2, pointsHelp2);
            _attributes[newIndex].patch->SetData(2, i, 2* pointsHelp - pointsHelp2);

            cout << "third Row OK\n";

            // az utolso sorat beallitom a masodik folt soranak adataival
            _attributes[newIndex].patch->SetData(3, i, pointsHelp);
        }
        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[N] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[S] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[S] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[E] = &_attributes[newIndex];


        _attributes[newIndex].neighboursDir[N] = S;
        _attributes[newIndex].neighboursDir[S] = E;

        _attributes[patch_ind1].neighboursDir[S] = N;
        _attributes[patch_ind2].neighboursDir[E] = S;

        cout << "N JOIN N SUCCESFULL";
    }

    GLvoid BiquarticCompositeSurface3::join_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind1].patch->GetData(3, i, pointsHelp);
            _attributes[newIndex].patch->SetData(0, i, pointsHelp);

            cout << "first Row OK\n";

            // a masodik sorat beallitom az elso folt soranak adatai 2*esevel - a 2ik sor
            _attributes[patch_ind1].patch->GetData(2, i, pointsHelp2);
            _attributes[newIndex].patch->SetData(1, i, 2 * pointsHelp - pointsHelp2);

            cout << "second Row OK\n";

            // a harmadik sorat beallitom a masodik folt soranak adatai *2 vel
            _attributes[patch_ind2].patch->GetData(3-i, 0, pointsHelp);
            _attributes[patch_ind2].patch->GetData(3-i, 1, pointsHelp2);
            _attributes[newIndex].patch->SetData(2, i, 2* pointsHelp - pointsHelp2);

            cout << "third Row OK\n";

            // az utolso sorat beallitom a masodik folt soranak adataival
            _attributes[newIndex].patch->SetData(3, i, pointsHelp);
        }
        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[N] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[S] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[S] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[W] = &_attributes[newIndex];


        _attributes[newIndex].neighboursDir[N] = S;
        _attributes[newIndex].neighboursDir[S] = W;

        _attributes[patch_ind1].neighboursDir[S] = N;
        _attributes[patch_ind2].neighboursDir[W] = S;
        cout << "N JOIN N SUCCESFULL";
    }

    GLvoid BiquarticCompositeSurface3::join_EW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;

        cout << "EW\n";
        for(GLuint i = 0; i < 4; i++)
        {
            // az elso oszlopat beallitom az elso folt oszlopanak adataival
            _attributes[patch_ind1].patch->GetData(i, 3, pointsHelp);
            _attributes[newIndex].patch->SetData(i, 0, pointsHelp);

            cout << "first Col OK\n";

            // a masodik oszlopat beallitom az elso folt oszlopanak adatai 2*esevel
            _attributes[patch_ind1].patch->GetData(i, 2, pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 1, 2 * pointsHelp - pointsHelp2);

            cout << "second Row OK\n";

            // a harmadik oszlopat beallitom a masodik folt oszlopanak adatai *2 vel
            _attributes[patch_ind2].patch->GetData(i, 0, pointsHelp);
            _attributes[patch_ind2].patch->GetData(i, 1, pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 2, 2* pointsHelp - pointsHelp2);

            cout << "third Row OK\n";

            // az utolso sorat beallitom a masodik folt soranak adataival
            _attributes[newIndex].patch->SetData(i, 3, pointsHelp);
        }
        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[W] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[E] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[E] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[W] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[W] = E;
        _attributes[newIndex].neighboursDir[E] = W;

        _attributes[patch_ind1].neighboursDir[E] = W;
        _attributes[patch_ind2].neighboursDir[W] = E;

        cout << "N JOIN N SUCCESFULL";
    }


    GLvoid BiquarticCompositeSurface3::join_N_NE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(0, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(1, 3, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(0, 2, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 2, pointsHelp4);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, pointsHelp);
        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2*pointsHelp - pointsHelp3);
        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);


        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2*pointsHelp - pointsHelp3);

        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2 * pointsHelp - pointsHelp3);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 3, pointsHelp);

        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(0, i, pointsHelp);
            _attributes[patch_ind2].patch->GetData(1, i, pointsHelp2);

            _attributes[newIndex].patch->SetData(2, i, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(3, i, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[NW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[S] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[N] = &_attributes[newIndex];


        _attributes[newIndex].neighboursDir[NW] = NE;
        _attributes[newIndex].neighboursDir[S] = N;

        _attributes[patch_ind1].neighboursDir[NE] = NW;
        _attributes[patch_ind2].neighboursDir[N] = S;

        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_N_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(0, 0, pointsHelp);
        _attributes[patch_ind1].patch->GetData(1, 0, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(0, 1, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 1, pointsHelp4);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, pointsHelp);
        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2*pointsHelp - pointsHelp3);
        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);


        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2*pointsHelp - pointsHelp3);

        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2 * pointsHelp - pointsHelp3);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 0, pointsHelp);

        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(0, i, pointsHelp);
            _attributes[patch_ind2].patch->GetData(1, i, pointsHelp2);

            _attributes[newIndex].patch->SetData(2, i, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(3, i, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[NE] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[S] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NW] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[N] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[NE] = NW;
        _attributes[newIndex].neighboursDir[S] = N;

        _attributes[patch_ind1].neighboursDir[NW] = NE;
        _attributes[patch_ind2].neighboursDir[N] = S;

        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_N_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(3, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(3, 2, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(2, 3, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(2, 2, pointsHelp4);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, pointsHelp);
        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2*pointsHelp - pointsHelp3);
        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);


        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2*pointsHelp - pointsHelp3);

        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2 * pointsHelp - pointsHelp3);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 3, pointsHelp);

        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(0, i, pointsHelp);
            _attributes[patch_ind2].patch->GetData(1, i, pointsHelp2);

            _attributes[newIndex].patch->SetData(2, i, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(3, i, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[NW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[S] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[SE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[N] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[NW] = SE;
        _attributes[newIndex].neighboursDir[S] = N;

        _attributes[patch_ind1].neighboursDir[SE] = NW;
        _attributes[patch_ind2].neighboursDir[N] = S;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_N_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(3, 0, pointsHelp);
        _attributes[patch_ind1].patch->GetData(3, 1, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(2, 0, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(2, 1, pointsHelp4);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, pointsHelp);
        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2*pointsHelp - pointsHelp3);
        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);


        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2*pointsHelp - pointsHelp3);

        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2 * pointsHelp - pointsHelp3);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 0, pointsHelp);

        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(0, i, pointsHelp);
            _attributes[patch_ind2].patch->GetData(1, i, pointsHelp2);

            _attributes[newIndex].patch->SetData(2, i, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(3, i, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[NE] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[S] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[SW] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[N] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[NE] = SW;
        _attributes[newIndex].neighboursDir[S] = N;

        _attributes[patch_ind1].neighboursDir[SW] = NE;
        _attributes[patch_ind2].neighboursDir[N] = S;
        cout << "N JOIN N SUCCESFULL";

    }


    GLvoid BiquarticCompositeSurface3::join_S_NE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(0, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(1, 3, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(0, 2, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 2, pointsHelp4);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, pointsHelp);
        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2*pointsHelp - pointsHelp3);
        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp);


        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2*pointsHelp - pointsHelp3);

        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2 * pointsHelp - pointsHelp3);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 0, pointsHelp);

        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(3, i, pointsHelp);
            _attributes[patch_ind2].patch->GetData(2, i, pointsHelp2);

            _attributes[newIndex].patch->SetData(1, i, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(0, i, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[SW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[N] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[S] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[SW] = NE;
        _attributes[newIndex].neighboursDir[N] = S;

        _attributes[patch_ind1].neighboursDir[NE] = SW;
        _attributes[patch_ind2].neighboursDir[S] = N;

        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_S_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(0, 0, pointsHelp);
        _attributes[patch_ind1].patch->GetData(1, 0, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(0, 1, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 1, pointsHelp4);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, pointsHelp);
        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2*pointsHelp - pointsHelp3);
        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);


        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2*pointsHelp - pointsHelp3);

        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2 * pointsHelp - pointsHelp3);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 3, pointsHelp);

        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(3, i, pointsHelp);
            _attributes[patch_ind2].patch->GetData(2, i, pointsHelp2);

            _attributes[newIndex].patch->SetData(1, i, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(0, i, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[SW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[E] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NW] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[S] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[SW] = NW;
        _attributes[newIndex].neighboursDir[E] = S;

        _attributes[patch_ind1].neighboursDir[NW] = SW;
        _attributes[patch_ind2].neighboursDir[S] = E;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_S_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(3, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(2, 3, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(3, 2, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(2, 2, pointsHelp4);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, pointsHelp);
        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2*pointsHelp - pointsHelp3);
        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);


        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2*pointsHelp - pointsHelp3);

        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2 * pointsHelp - pointsHelp3);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 3, pointsHelp);

        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(0, i, pointsHelp);
            _attributes[patch_ind2].patch->GetData(1, i, pointsHelp2);

            _attributes[newIndex].patch->SetData(1, i, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(0, i, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[SW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[N] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[SE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[S] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[SW] = SE;
        _attributes[newIndex].neighboursDir[N] = S;

        _attributes[patch_ind1].neighboursDir[SE] = SW;
        _attributes[patch_ind2].neighboursDir[S] = N;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_S_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(3, 0, pointsHelp);
        _attributes[patch_ind1].patch->GetData(2, 0, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(3, 1, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(2, 1, pointsHelp4);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, pointsHelp);
        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2*pointsHelp - pointsHelp3);
        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp);


        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2*pointsHelp - pointsHelp3);

        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2 * pointsHelp - pointsHelp3);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 0, pointsHelp);

        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(3, i, pointsHelp);
            _attributes[patch_ind2].patch->GetData(2, i, pointsHelp2);

            _attributes[newIndex].patch->SetData(1, i, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(0, i, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[SE] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[N] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[SW] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[S] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[SE] = SW;
        _attributes[newIndex].neighboursDir[N] = S;

        _attributes[patch_ind1].neighboursDir[SW] = SE;
        _attributes[patch_ind2].neighboursDir[S] = N;

        cout << "N JOIN N SUCCESFULL";

    }


    GLvoid BiquarticCompositeSurface3::join_E_NE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(0, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(0, 2, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(1, 3, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 2, pointsHelp4);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, pointsHelp);
        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2*pointsHelp - pointsHelp3);
        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);


        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2*pointsHelp - pointsHelp3);

        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2 * pointsHelp - pointsHelp3);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 3, pointsHelp);

        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(i, 3, pointsHelp);
            _attributes[patch_ind2].patch->GetData(i, 2, pointsHelp2);

            _attributes[newIndex].patch->SetData(i, 1, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 0, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[SE] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[W] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[E] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[SE] = NE;
        _attributes[newIndex].neighboursDir[W] = E;

        _attributes[patch_ind1].neighboursDir[NE] = SE;
        _attributes[patch_ind2].neighboursDir[E] = W;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_E_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(0, 0, pointsHelp);
        _attributes[patch_ind1].patch->GetData(0, 1, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(1, 0, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 1, pointsHelp4);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, pointsHelp);
        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2*pointsHelp - pointsHelp3);
        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);


        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2*pointsHelp - pointsHelp3);

        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2 * pointsHelp - pointsHelp3);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 3, pointsHelp);

        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(i, 3, pointsHelp);
            _attributes[patch_ind2].patch->GetData(i, 2, pointsHelp2);

            _attributes[newIndex].patch->SetData(i, 1, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 0, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[NE] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[W] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NW] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[E] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[NE] = NW;
        _attributes[newIndex].neighboursDir[W] = E;

        _attributes[patch_ind1].neighboursDir[NW] = NE;
        _attributes[patch_ind2].neighboursDir[E] = W;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_E_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(3, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(3, 2, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(2, 3, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(2, 2, pointsHelp4);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, pointsHelp);
        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2*pointsHelp - pointsHelp3);
        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);


        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2*pointsHelp - pointsHelp3);

        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2 * pointsHelp - pointsHelp3);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 3, pointsHelp);

        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(i, 3, pointsHelp);
            _attributes[patch_ind2].patch->GetData(i, 2, pointsHelp2);

            _attributes[newIndex].patch->SetData(i, 1, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 0, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[NE] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[W] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[SE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[E] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[NE] = SE;
        _attributes[newIndex].neighboursDir[W] = E;

        _attributes[patch_ind1].neighboursDir[SE] = NE;
        _attributes[patch_ind2].neighboursDir[E] = W;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_E_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(3, 0, pointsHelp);
        _attributes[patch_ind1].patch->GetData(3, 1, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(2, 0, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(2, 1, pointsHelp4);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, pointsHelp);
        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2*pointsHelp - pointsHelp3);
        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);


        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2*pointsHelp - pointsHelp3);

        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2 * pointsHelp - pointsHelp3);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 3, pointsHelp);

        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(i, 3, pointsHelp);
            _attributes[patch_ind2].patch->GetData(i, 2, pointsHelp2);

            _attributes[newIndex].patch->SetData(i, 1, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 0, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[SE] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[W] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[SW] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[E] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[SE] = SW;
        _attributes[newIndex].neighboursDir[W] = E;

        _attributes[patch_ind1].neighboursDir[SW] = SE;
        _attributes[patch_ind2].neighboursDir[E] = W;
        cout << "N JOIN N SUCCESFULL";

    }


    GLvoid BiquarticCompositeSurface3::join_W_NE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(0, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(0, 2, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(1, 3, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 2, pointsHelp4);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, pointsHelp);
        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2*pointsHelp - pointsHelp3);
        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);


        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2*pointsHelp - pointsHelp3);

        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2 * pointsHelp - pointsHelp3);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 0, pointsHelp);

        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(i, 0, pointsHelp);
            _attributes[patch_ind2].patch->GetData(i, 1, pointsHelp2);

            _attributes[newIndex].patch->SetData(i, 2, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 3, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[NW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[E] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[W] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[NW] = NE;
        _attributes[newIndex].neighboursDir[E] = W;

        _attributes[patch_ind1].neighboursDir[NE] = NW;
        _attributes[patch_ind2].neighboursDir[W] = E;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_W_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(0, 0, pointsHelp);
        _attributes[patch_ind1].patch->GetData(0, 1, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(1, 0, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 1, pointsHelp4);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, pointsHelp);
        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2*pointsHelp - pointsHelp3);
        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);


        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2*pointsHelp - pointsHelp3);

        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2 * pointsHelp - pointsHelp3);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 0, pointsHelp);

        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(i, 0, pointsHelp);
            _attributes[patch_ind2].patch->GetData(i, 1, pointsHelp2);

            _attributes[newIndex].patch->SetData(i, 2, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 3, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[SW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[E] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NW] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[W] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[SW] = NW;
        _attributes[newIndex].neighboursDir[E] = W;

        _attributes[patch_ind1].neighboursDir[NW] = SW;
        _attributes[patch_ind2].neighboursDir[W] = E;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_W_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(3, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(3, 2, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(2, 3, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(2, 2, pointsHelp4);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, pointsHelp);
        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2*pointsHelp - pointsHelp3);
        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);


        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2*pointsHelp - pointsHelp3);

        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2 * pointsHelp - pointsHelp3);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 0, pointsHelp);

        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(i, 0, pointsHelp);
            _attributes[patch_ind2].patch->GetData(i, 1, pointsHelp2);

            _attributes[newIndex].patch->SetData(i, 2, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 3, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[SW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[E] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[SE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[W] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[SW] = SE;
        _attributes[newIndex].neighboursDir[E] = W;

        _attributes[patch_ind1].neighboursDir[SE] = SW;
        _attributes[patch_ind2].neighboursDir[W] = E;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_W_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(3, 0, pointsHelp);
        _attributes[patch_ind1].patch->GetData(3, 1, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(2, 0, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(2, 1, pointsHelp4);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, pointsHelp);
        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2*pointsHelp - pointsHelp3);
        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);


        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2*pointsHelp - pointsHelp3);

        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, 2 * pointsHelp - pointsHelp3);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 0, pointsHelp);

        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2 * pointsHelp - pointsHelp3);

        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[patch_ind2].patch->GetData(i, 0, pointsHelp);
            _attributes[patch_ind2].patch->GetData(i, 1, pointsHelp2);

            _attributes[newIndex].patch->SetData(i, 2, 2 * pointsHelp - pointsHelp2);
            _attributes[newIndex].patch->SetData(i, 3, pointsHelp);
        }


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[NW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[E] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[SW] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[W] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[NW] = SW;
        _attributes[newIndex].neighboursDir[E] = W;

        _attributes[patch_ind1].neighboursDir[SW] = NW;
        _attributes[patch_ind2].neighboursDir[W] = E;
        cout << "N JOIN N SUCCESFULL";

    }


    GLvoid BiquarticCompositeSurface3::join_NE_NE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;


        _attributes[patch_ind1].patch->GetData(0, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(1, 3, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(0, 2, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 2, pointsHelp4);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, pointsHelp);
        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2*pointsHelp - pointsHelp3);
        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);


        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2*pointsHelp - pointsHelp3);

        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2 * pointsHelp - pointsHelp3);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 0, pointsHelp2);

        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp2);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp2);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(1, 0, pointsHelp2);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, 2 * pointsHelp - pointsHelp2);



        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        _attributes[patch_ind2].patch->GetData(0, 3, pointsHelp);
        _attributes[patch_ind2].patch->GetData(0, 2, pointsHelp2);
        _attributes[patch_ind2].patch->GetData(1, 3, pointsHelp3);
        _attributes[patch_ind2].patch->GetData(1, 2, pointsHelp4);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, pointsHelp);
        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2*pointsHelp - pointsHelp3);
        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);


        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, ( _attributes[newIndex].patch->operator()(2,2) + (2*pointsHelp - pointsHelp3)) /2);

        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, ( _attributes[newIndex].patch->operator()(1,2) + (2 * pointsHelp - pointsHelp3)) /2);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, ( _attributes[newIndex].patch->operator()(0,3) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(3, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 3, pointsHelp2);

        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp2);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0,( _attributes[newIndex].patch->operator()(3,0) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(2, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp2);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 2,( _attributes[newIndex].patch->operator()(2,1) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(1, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp2);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1,( _attributes[newIndex].patch->operator()(1,1) + (2 * pointsHelp - pointsHelp2)) /2);

        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[NW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[SE] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[NE] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[NW] = NE;
        _attributes[newIndex].neighboursDir[SE] = NE;

        _attributes[patch_ind1].neighboursDir[NE] = NW;
        _attributes[patch_ind2].neighboursDir[NE] = SE;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_NE_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(0, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(1, 3, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(0, 2, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 2, pointsHelp4);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, pointsHelp);
        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2*pointsHelp - pointsHelp3);
        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);


        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2*pointsHelp - pointsHelp3);

        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2 * pointsHelp - pointsHelp3);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 0, pointsHelp2);

        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp2);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp2);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(1, 0, pointsHelp2);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, 2 * pointsHelp - pointsHelp2);



        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        _attributes[patch_ind2].patch->GetData(0, 0, pointsHelp);
        _attributes[patch_ind2].patch->GetData(0, 1, pointsHelp2);
        _attributes[patch_ind2].patch->GetData(1, 0, pointsHelp3);
        _attributes[patch_ind2].patch->GetData(1, 1, pointsHelp4);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, pointsHelp);
        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2*pointsHelp - pointsHelp3);
        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp);


        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, ( _attributes[newIndex].patch->operator()(2,2) + (2*pointsHelp - pointsHelp3)) /2);

        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, ( _attributes[newIndex].patch->operator()(2,1) + (2 * pointsHelp - pointsHelp3)) /2);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, ( _attributes[newIndex].patch->operator()(3,0) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 3, pointsHelp2);

        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp2);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2,( _attributes[newIndex].patch->operator()(1,2) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(2, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp2);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 2,( _attributes[newIndex].patch->operator()(1,1) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp2);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3,( _attributes[newIndex].patch->operator()(0,3) + (2 * pointsHelp - pointsHelp2)) /2);



        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[NW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[SE] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[NW] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[NW] = NE;
        _attributes[newIndex].neighboursDir[SE] = NW;

        _attributes[patch_ind1].neighboursDir[NE] = NW;
        _attributes[patch_ind2].neighboursDir[NW] = SE;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_NE_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(0, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(1, 3, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(0, 2, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 2, pointsHelp4);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, pointsHelp);
        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2*pointsHelp - pointsHelp3);
        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp);


        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2*pointsHelp - pointsHelp3);

        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2 * pointsHelp - pointsHelp3);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 3, pointsHelp2);

        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp2);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp2);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp2);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3,2 * pointsHelp - pointsHelp2);



        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        _attributes[patch_ind2].patch->GetData(3, 3, pointsHelp);
        _attributes[patch_ind2].patch->GetData(3, 2, pointsHelp2);
        _attributes[patch_ind2].patch->GetData(2, 3, pointsHelp3);
        _attributes[patch_ind2].patch->GetData(2, 2, pointsHelp4);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, pointsHelp);
        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2*pointsHelp - pointsHelp3);
        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);


        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, ( _attributes[newIndex].patch->operator()(1,1) + (2*pointsHelp - pointsHelp3)) /2);

        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, ( _attributes[newIndex].patch->operator()(1,2) + (2 * pointsHelp - pointsHelp3)) /2);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, ( _attributes[newIndex].patch->operator()(0,3) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 0, pointsHelp2);

        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp2);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1,( _attributes[newIndex].patch->operator()(2,1) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(1, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp2);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2,( _attributes[newIndex].patch->operator()(2,2) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(1, 0, pointsHelp2);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0,( _attributes[newIndex].patch->operator()(3,0) + (2 * pointsHelp - pointsHelp2)) /2);



        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[SE] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[NW] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[SE] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[SE] = NE;
        _attributes[newIndex].neighboursDir[NW] = SE;

        _attributes[patch_ind1].neighboursDir[NE] = SE;
        _attributes[patch_ind2].neighboursDir[SE] = NW;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_NE_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {
        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(0, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(0, 2, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(1, 3, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 2, pointsHelp4);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, pointsHelp);
        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2*pointsHelp - pointsHelp3);
        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);


        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2*pointsHelp - pointsHelp3);

        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2 * pointsHelp - pointsHelp3);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 0, pointsHelp2);

        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp2);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp2);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 0, pointsHelp2);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, 2 * pointsHelp - pointsHelp2);



        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        _attributes[patch_ind2].patch->GetData(3, 0, pointsHelp);
        _attributes[patch_ind2].patch->GetData(2, 0, pointsHelp2);
        _attributes[patch_ind2].patch->GetData(3, 1, pointsHelp3);
        _attributes[patch_ind2].patch->GetData(2, 1, pointsHelp4);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, pointsHelp);
        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2*pointsHelp - pointsHelp3);
        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);


        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, ( _attributes[newIndex].patch->operator()(1,2) + (2*pointsHelp - pointsHelp3)) /2);

        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, ( _attributes[newIndex].patch->operator()(2,2) + (2 * pointsHelp - pointsHelp3)) /2);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, ( _attributes[newIndex].patch->operator()(3,3) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 3, pointsHelp2);

        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp2);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1,( _attributes[newIndex].patch->operator()(1,1) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(2, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp2);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1,( _attributes[newIndex].patch->operator()(2,1) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp2);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0,( _attributes[newIndex].patch->operator()(0,0) + (2 * pointsHelp - pointsHelp2)) /2);



        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[SW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[NE] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[SW] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[SW] = NE;
        _attributes[newIndex].neighboursDir[NE] = SW;

        _attributes[patch_ind1].neighboursDir[NE] = SW;
        _attributes[patch_ind2].neighboursDir[SW] = NE;
        cout << "N JOIN N SUCCESFULL";

    }


    GLvoid BiquarticCompositeSurface3::join_NW_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;


        _attributes[patch_ind1].patch->GetData(0, 0, pointsHelp);
        _attributes[patch_ind1].patch->GetData(1, 0, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(0, 1, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 1, pointsHelp4);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, pointsHelp);
        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2*pointsHelp - pointsHelp3);
        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);


        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2*pointsHelp - pointsHelp3);

        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2 * pointsHelp - pointsHelp3);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 3, pointsHelp2);

        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp2);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp2);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp2);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, 2 * pointsHelp - pointsHelp2);



        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        _attributes[patch_ind2].patch->GetData(0, 0, pointsHelp);
        _attributes[patch_ind2].patch->GetData(0, 1, pointsHelp2);
        _attributes[patch_ind2].patch->GetData(1, 0, pointsHelp3);
        _attributes[patch_ind2].patch->GetData(1, 1, pointsHelp4);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, pointsHelp);
        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2*pointsHelp - pointsHelp3);
        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);


        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, ( _attributes[newIndex].patch->operator()(2,1) + (2*pointsHelp - pointsHelp3)) /2);

        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, ( _attributes[newIndex].patch->operator()(1,1) + (2 * pointsHelp - pointsHelp3)) /2);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, ( _attributes[newIndex].patch->operator()(0,0) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 0, pointsHelp2);

        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 0, pointsHelp2);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2,( _attributes[newIndex].patch->operator()(2,2) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(1, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(1, 0, pointsHelp2);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2,( _attributes[newIndex].patch->operator()(1,2) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(3, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp2);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3,( _attributes[newIndex].patch->operator()(3,3) + (2 * pointsHelp - pointsHelp2)) /2);


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[NE] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[SW] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NW] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[NW] = &_attributes[newIndex];


        _attributes[newIndex].neighboursDir[NE] = NW;
        _attributes[newIndex].neighboursDir[SW] = NW;

        _attributes[patch_ind1].neighboursDir[NW] = NE;
        _attributes[patch_ind2].neighboursDir[NW] = SW;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_NW_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;


        _attributes[patch_ind1].patch->GetData(0, 0, pointsHelp);
        _attributes[patch_ind1].patch->GetData(1, 0, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(0, 1, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 1, pointsHelp4);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, pointsHelp);
        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2*pointsHelp - pointsHelp3);
        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);


        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2*pointsHelp - pointsHelp3);

        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2 * pointsHelp - pointsHelp3);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(3, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 3, pointsHelp2);

        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp2);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp2);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp2);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, 2 * pointsHelp - pointsHelp2);



        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        _attributes[patch_ind2].patch->GetData(3, 3, pointsHelp);
        _attributes[patch_ind2].patch->GetData(3, 2, pointsHelp2);
        _attributes[patch_ind2].patch->GetData(2, 3, pointsHelp3);
        _attributes[patch_ind2].patch->GetData(2, 2, pointsHelp4);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, pointsHelp);
        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2*pointsHelp - pointsHelp3);
        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);


        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, ( _attributes[newIndex].patch->operator()(1,1) + (2*pointsHelp - pointsHelp3)) /2);

        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, ( _attributes[newIndex].patch->operator()(1,2) + (2 * pointsHelp - pointsHelp3)) /2);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, ( _attributes[newIndex].patch->operator()(0,3) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 0, pointsHelp2);

        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp2);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1,( _attributes[newIndex].patch->operator()(2,1) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(1, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp2);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2,( _attributes[newIndex].patch->operator()(2,2) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(1, 0, pointsHelp2);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0,( _attributes[newIndex].patch->operator()(3,0) + (2 * pointsHelp - pointsHelp2)) /2);




        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[SE] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[NW] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NW] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[SE] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[SE] = NW;
        _attributes[newIndex].neighboursDir[NW] = SE;

        _attributes[patch_ind1].neighboursDir[NW] = SE;
        _attributes[patch_ind2].neighboursDir[SE] = NW;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_NW_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;


        _attributes[patch_ind1].patch->GetData(0, 0, pointsHelp);
        _attributes[patch_ind1].patch->GetData(1, 0, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(0, 1, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(1, 1, pointsHelp4);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, pointsHelp);
        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2*pointsHelp - pointsHelp3);
        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);


        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2*pointsHelp - pointsHelp3);

        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2 * pointsHelp - pointsHelp3);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 0, pointsHelp2);

        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp2);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp2);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 0, pointsHelp2);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, 2 * pointsHelp - pointsHelp2);



        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        _attributes[patch_ind2].patch->GetData(3, 0, pointsHelp);
        _attributes[patch_ind2].patch->GetData(2, 0, pointsHelp2);
        _attributes[patch_ind2].patch->GetData(3, 1, pointsHelp3);
        _attributes[patch_ind2].patch->GetData(2, 1, pointsHelp4);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, pointsHelp);
        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2*pointsHelp - pointsHelp3);
        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);


        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, ( _attributes[newIndex].patch->operator()(1,2) + (2*pointsHelp - pointsHelp3)) /2);

        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, ( _attributes[newIndex].patch->operator()(2,2) + (2 * pointsHelp - pointsHelp3)) /2);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, ( _attributes[newIndex].patch->operator()(3,3) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 3, pointsHelp2);

        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp2);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1,( _attributes[newIndex].patch->operator()(1,1) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(2, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp2);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1,( _attributes[newIndex].patch->operator()(2,1) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp2);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0,( _attributes[newIndex].patch->operator()(0,0) + (2 * pointsHelp - pointsHelp2)) /2);






        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[SW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[NE] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[NW] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[SW] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[SW] = NW;
        _attributes[newIndex].neighboursDir[NE] = SW;

        _attributes[patch_ind1].neighboursDir[NW] = SW;
        _attributes[patch_ind2].neighboursDir[SW] = NE;
        cout << "N JOIN N SUCCESFULL";

    }


    GLvoid BiquarticCompositeSurface3::join_SE_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;


        _attributes[patch_ind1].patch->GetData(3, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(2, 3, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(3, 2, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(2, 2, pointsHelp4);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, pointsHelp);
        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2*pointsHelp - pointsHelp3);
        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);


        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2*pointsHelp - pointsHelp3);

        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2 * pointsHelp - pointsHelp3);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 0, pointsHelp2);

        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp2);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp2);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 0, pointsHelp2);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, 2 * pointsHelp - pointsHelp2);



        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        _attributes[patch_ind2].patch->GetData(3, 3, pointsHelp);
        _attributes[patch_ind2].patch->GetData(3, 2, pointsHelp2);
        _attributes[patch_ind2].patch->GetData(2, 3, pointsHelp3);
        _attributes[patch_ind2].patch->GetData(2, 2, pointsHelp4);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, pointsHelp);
        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2*pointsHelp - pointsHelp3);
        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);


        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, ( _attributes[newIndex].patch->operator()(1,2) + (2*pointsHelp - pointsHelp3)) /2);

        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, ( _attributes[newIndex].patch->operator()(2,2) + (2 * pointsHelp - pointsHelp3)) /2);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, ( _attributes[newIndex].patch->operator()(3,3) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 3, pointsHelp2);

        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(1, 3, pointsHelp2);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1,( _attributes[newIndex].patch->operator()(1,1) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(2, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp2);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1,( _attributes[newIndex].patch->operator()(2,1) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp2);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0,( _attributes[newIndex].patch->operator()(0,0) + (2 * pointsHelp - pointsHelp2)) /2);


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[SW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[NE] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[SE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[SE] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[SW] = SE;
        _attributes[newIndex].neighboursDir[NE] = SE;

        _attributes[patch_ind1].neighboursDir[SE] = SW;
        _attributes[patch_ind2].neighboursDir[SE] = NE;
        cout << "N JOIN N SUCCESFULL";

    }

    GLvoid BiquarticCompositeSurface3::join_SE_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;

        _attributes[patch_ind1].patch->GetData(3, 3, pointsHelp);
        _attributes[patch_ind1].patch->GetData(3, 2, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(2, 3, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(2, 2, pointsHelp4);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, pointsHelp);
        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2*pointsHelp - pointsHelp3);
        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);


        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2*pointsHelp - pointsHelp3);

        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2 * pointsHelp - pointsHelp3);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 0, pointsHelp2);

        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp2);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 2, pointsHelp2);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);
        _attributes[newIndex].patch->GetData(1, 0, pointsHelp2);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, 2 * pointsHelp - pointsHelp2);



        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        _attributes[patch_ind2].patch->GetData(3, 0, pointsHelp);
        _attributes[patch_ind2].patch->GetData(2, 0, pointsHelp2);
        _attributes[patch_ind2].patch->GetData(3, 1, pointsHelp3);
        _attributes[patch_ind2].patch->GetData(2, 1, pointsHelp4);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, pointsHelp);
        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2*pointsHelp - pointsHelp3);
        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp);


        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, ( _attributes[newIndex].patch->operator()(2,2) + (2*pointsHelp - pointsHelp3)) /2);

        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, ( _attributes[newIndex].patch->operator()(2,1) + (2 * pointsHelp - pointsHelp3)) /2);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, ( _attributes[newIndex].patch->operator()(3,0) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 3, pointsHelp2);

        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp2);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2,( _attributes[newIndex].patch->operator()(1,2) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(2, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp2);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1,( _attributes[newIndex].patch->operator()(1,1) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp2);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3,( _attributes[newIndex].patch->operator()(0,3) + (2 * pointsHelp - pointsHelp2)) /2);



        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[NW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[SE] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[SE] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[SW] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[NW] = SE;
        _attributes[newIndex].neighboursDir[SE] = SW;

        _attributes[patch_ind1].neighboursDir[SE] = NW;
        _attributes[patch_ind2].neighboursDir[SW] = SE;
        cout << "N JOIN N SUCCESFULL";

    }


    GLvoid BiquarticCompositeSurface3::join_SW_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex)
    {

        DCoordinate3 pointsHelp;
        DCoordinate3 pointsHelp2;
        DCoordinate3 pointsHelp3;
        DCoordinate3 pointsHelp4;


        _attributes[patch_ind1].patch->GetData(3, 0, pointsHelp);
        _attributes[patch_ind1].patch->GetData(3, 1, pointsHelp2);
        _attributes[patch_ind1].patch->GetData(2, 0, pointsHelp3);
        _attributes[patch_ind1].patch->GetData(2, 1, pointsHelp4);

        // [0,0]
        _attributes[newIndex].patch->SetData(0, 0, pointsHelp);
        // [0,1]
        _attributes[newIndex].patch->SetData(0, 1, 2*pointsHelp - pointsHelp3);
        // [1,0]
        _attributes[newIndex].patch->SetData(1, 0, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(1, 0, pointsHelp);


        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1, 2*pointsHelp - pointsHelp3);

        // [2,0]
        _attributes[newIndex].patch->SetData(2, 0, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(2, 0, pointsHelp);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, 2 * pointsHelp - pointsHelp3);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(0, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 0, pointsHelp2);

        // [0,2]
        _attributes[newIndex].patch->SetData(0, 2, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(1, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(1, 0, pointsHelp2);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 0, pointsHelp2);

        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, 2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(0, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(0, 1, pointsHelp2);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3, 2 * pointsHelp - pointsHelp2);



        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        _attributes[patch_ind2].patch->GetData(3, 0, pointsHelp);
        _attributes[patch_ind2].patch->GetData(2, 0, pointsHelp2);
        _attributes[patch_ind2].patch->GetData(3, 1, pointsHelp3);
        _attributes[patch_ind2].patch->GetData(2, 1, pointsHelp4);

        // [3,3]
        _attributes[newIndex].patch->SetData(3, 3, pointsHelp);
        // [2,3]
        _attributes[newIndex].patch->SetData(2, 3, 2*pointsHelp - pointsHelp3);
        // [3,2]
        _attributes[newIndex].patch->SetData(3, 2, 2*pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp);


        // [2,2]
        _attributes[newIndex].patch->SetData(2, 2, ( _attributes[newIndex].patch->operator()(2,2) + (2*pointsHelp - pointsHelp3)) /2);

        // [3,1]
        _attributes[newIndex].patch->SetData(3, 1, 2 * pointsHelp - pointsHelp2);

        pointsHelp2 = pointsHelp3;
        pointsHelp3 = 2*pointsHelp3 - pointsHelp4;
        pointsHelp4 = pointsHelp2;

        pointsHelp2 = pointsHelp;
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp);

        // [2,1]
        _attributes[newIndex].patch->SetData(2, 1, ( _attributes[newIndex].patch->operator()(2,1) + (2 * pointsHelp - pointsHelp3)) /2);

        // [3,0]
        _attributes[newIndex].patch->SetData(3, 0, ( _attributes[newIndex].patch->operator()(3,0) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(2, 3, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 3, pointsHelp2);

        // [1,3]
        _attributes[newIndex].patch->SetData(1, 3,2 * pointsHelp - pointsHelp2);


        _attributes[newIndex].patch->GetData(2, 2, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 2, pointsHelp2);

        // [1,2]
        _attributes[newIndex].patch->SetData(1, 2,( _attributes[newIndex].patch->operator()(1,2) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(2, 1, pointsHelp);
        _attributes[newIndex].patch->GetData(3, 1, pointsHelp2);

        // [1,1]
        _attributes[newIndex].patch->SetData(1, 1,( _attributes[newIndex].patch->operator()(1,1) + (2 * pointsHelp - pointsHelp2)) /2);


        _attributes[newIndex].patch->GetData(1, 3, pointsHelp);
        _attributes[newIndex].patch->GetData(2, 3, pointsHelp2);

        // [0,3]
        _attributes[newIndex].patch->SetData(0, 3,( _attributes[newIndex].patch->operator()(0,3) + (2 * pointsHelp - pointsHelp2)) /2);


        // beallitom az uj foltnak a szomszedait
        _attributes[newIndex].neighbours[NW] = &_attributes[patch_ind1];
        _attributes[newIndex].neighbours[SE] = &_attributes[patch_ind2];

        // beallitom a kapott foltok uj szomszedjat
        _attributes[patch_ind1].neighbours[SW] = &_attributes[newIndex];
        _attributes[patch_ind2].neighbours[SW] = &_attributes[newIndex];

        _attributes[newIndex].neighboursDir[NW] = SW;
        _attributes[newIndex].neighboursDir[SE] = SW;

        _attributes[patch_ind1].neighboursDir[SW] = NW;
        _attributes[patch_ind2].neighboursDir[SW] = SE;
        cout << "N JOIN N SUCCESFULL";

    }

}
