#pragma once

#include "../Core/TensorProductSurfaces3.h"

namespace cagd
{
    class BiquarticPatch3: public TensorProductSurface3
    {

    public:
        BiquarticPatch3();

        GLboolean UBlendingFunctionValues(GLdouble u_knot, RowMatrix<GLdouble> &blending_values) const override;
        GLboolean VBlendingFunctionValues(GLdouble v_knot, RowMatrix<GLdouble> &blending_values) const override;
        GLboolean CalculatePartialDerivatives(GLuint maximum_order_of_partial_derivatives, GLdouble u, GLdouble v, PartialDerivatives &pd) const override;

        GLboolean RenderDataPoints(GLenum render_mode = GL_LINE_STRIP) const;
        GLboolean RenderSelectedPoint(GLuint row, GLuint col) const;
    };

}
