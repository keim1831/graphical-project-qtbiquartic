#include "BiquarticCompositeSurfaces3.h"

namespace cagd
{
    GLvoid BiquarticCompositeSurface3::generateIsoLinesUpdateVBO(PatchAttributes *attr)
    {
        if (attr->u_iso_lines)
        {
            delete attr->u_iso_lines;
        }
        attr->u_iso_lines = attr->patch->GenerateUIsoparametricLines(_iso_line_count_u, 1, _u_div_point_count);
        if (attr->v_iso_lines)
        {
            delete attr->v_iso_lines;
        }
        attr->v_iso_lines = attr->patch->GenerateVIsoparametricLines(_iso_line_count_v, 1, _v_div_point_count);

        if (attr->u_iso_lines)
        {
            for (GLuint i = 0; i < (*attr->u_iso_lines).GetColumnCount(); ++i)
            {
                if ((*attr->u_iso_lines)[i])
                {
                    if (!(*attr->u_iso_lines)[i]->UpdateVertexBufferObjects(0.2))
                    {
                        cout << "Could not update isoline's vbo\n";
                    }
                }
            }
        }

        if (attr->v_iso_lines)
        {
            for (GLuint i = 0; i < (*attr->v_iso_lines).GetColumnCount(); ++i)
            {
                if ((*attr->v_iso_lines)[i])
                {
                    if (!(*attr->v_iso_lines)[i]->UpdateVertexBufferObjects(0.2))
                    {
                        cout << "Could not update isoline's vbo\n";
                    }
                }
            }
        }
    }

    BiquarticCompositeSurface3::PatchAttributes* BiquarticCompositeSurface3::changeAllNeighboursClockwise(PatchAttributes* pattr1, PatchAttributes* pattr2,
                                                                                                               PatchAttributes *nextNeighbour,
                                                                    DCoordinate3 commonPoint0, DCoordinate3 commonPoint1,
                                                                    DCoordinate3 otherPoint0, DCoordinate3 otherPoint1, Direction neighbourDir)
    {
        do
        {
            switch(neighbourDir)
            {
            case W:
                nextNeighbour->patch->SetData(3, 0, commonPoint0);
                nextNeighbour->patch->SetData(3, 1, 2.0 * commonPoint0 - otherPoint0);

                nextNeighbour->patch->SetData(2, 0, commonPoint1);
                nextNeighbour->patch->SetData(2, 1, 2.0 * commonPoint1 - otherPoint1);

                nextNeighbour->patch->GetData(2, 0, otherPoint0);
                nextNeighbour->patch->GetData(2, 1, otherPoint1);
                nextNeighbour->patch->GetData(3, 1, commonPoint1);
                break;
            case S:
                nextNeighbour->patch->SetData(3, 3, commonPoint0);
                nextNeighbour->patch->SetData(2, 3, 2.0 * commonPoint0 - otherPoint0);

                nextNeighbour->patch->SetData(3, 2, commonPoint1);
                nextNeighbour->patch->SetData(2, 2, 2.0 * commonPoint1 - otherPoint1);

                nextNeighbour->patch->GetData(3, 2, otherPoint0);
                nextNeighbour->patch->GetData(2, 2, otherPoint1);
                nextNeighbour->patch->GetData(2, 3, commonPoint1);
                break;
            case E:
                nextNeighbour->patch->SetData(0, 3, commonPoint0);
                nextNeighbour->patch->SetData(0, 2, 2.0 * commonPoint0 - otherPoint0);

                nextNeighbour->patch->SetData(1, 3, commonPoint1);
                nextNeighbour->patch->SetData(1, 2, 2.0 * commonPoint1 - otherPoint1);

                nextNeighbour->patch->GetData(1, 3, otherPoint0);
                nextNeighbour->patch->GetData(1, 2, otherPoint1);
                nextNeighbour->patch->GetData(0, 2, commonPoint1);
                break;
            case N:
                nextNeighbour->patch->SetData(0, 0, commonPoint0);
                nextNeighbour->patch->SetData(1, 0, 2.0 * commonPoint0 - otherPoint0);

                nextNeighbour->patch->SetData(0, 1, commonPoint1);
                nextNeighbour->patch->SetData(1, 1, 2.0 * commonPoint1 - otherPoint1);

                nextNeighbour->patch->GetData(0, 1, otherPoint0);
                nextNeighbour->patch->GetData(1, 1, otherPoint1);
                nextNeighbour->patch->GetData(1, 0, commonPoint1);
                break;
            default:
                break;
            }

            nextNeighbour->patch->UpdateVertexBufferObjectsOfData();
            if (nextNeighbour->img)
            {
                delete nextNeighbour->img;
            }
            nextNeighbour->img = nextNeighbour->patch->GenerateImage(_u_div_point_count, _v_div_point_count);
            nextNeighbour->img->UpdateVertexBufferObjects();
            generateIsoLinesUpdateVBO(nextNeighbour);

            Direction nextDir = getNextDirClockwise(neighbourDir);

            neighbourDir = nextNeighbour->neighboursDir[nextDir];
            nextNeighbour = nextNeighbour->neighbours[nextDir];
        }
        while (nextNeighbour != pattr1 && nextNeighbour != pattr2 && nextNeighbour != nullptr);
        return nextNeighbour;
    }

    BiquarticCompositeSurface3::PatchAttributes* BiquarticCompositeSurface3::changeAllNeighboursCounterClockwise(PatchAttributes* pattr1, PatchAttributes* pattr2, PatchAttributes *nextNeighbour,
                                                                    DCoordinate3 commonPoint0, DCoordinate3 commonPoint1,
                                                                    DCoordinate3 otherPoint0, DCoordinate3 otherPoint1, Direction neighbourDir)
    {
        do
        {
            switch(neighbourDir)
            {
            case W:
                nextNeighbour->patch->SetData(0, 0, commonPoint0);
                nextNeighbour->patch->SetData(0, 1, 2.0 * commonPoint0 - otherPoint0);

                nextNeighbour->patch->SetData(1, 0, commonPoint1);
                nextNeighbour->patch->SetData(1, 1, 2.0 * commonPoint1 - otherPoint1);

                nextNeighbour->patch->GetData(1, 0, otherPoint0);
                nextNeighbour->patch->GetData(1, 1, otherPoint1);
                nextNeighbour->patch->GetData(0, 1, commonPoint1);
                break;
            case S:
                nextNeighbour->patch->SetData(3, 0, commonPoint0);
                nextNeighbour->patch->SetData(2, 0, 2.0 * commonPoint0 - otherPoint0);

                nextNeighbour->patch->SetData(3, 1, commonPoint1);
                nextNeighbour->patch->SetData(2, 1, 2.0 * commonPoint1 - otherPoint1);

                nextNeighbour->patch->GetData(3, 1, otherPoint0);
                nextNeighbour->patch->GetData(2, 1, otherPoint1);
                nextNeighbour->patch->GetData(2, 0, commonPoint1);
                break;
            case E:
                nextNeighbour->patch->SetData(3, 3, commonPoint0);
                nextNeighbour->patch->SetData(3, 2, 2.0 * commonPoint0 - otherPoint0);

                nextNeighbour->patch->SetData(2, 3, commonPoint1);
                nextNeighbour->patch->SetData(2, 2, 2.0 * commonPoint1 - otherPoint1);

                nextNeighbour->patch->GetData(2, 3, otherPoint0);
                nextNeighbour->patch->GetData(2, 2, otherPoint1);
                nextNeighbour->patch->GetData(3, 2, commonPoint1);
                break;
            case N:
                nextNeighbour->patch->SetData(0, 3, commonPoint0);
                nextNeighbour->patch->SetData(1, 3, 2.0 * commonPoint0 - otherPoint0);

                nextNeighbour->patch->SetData(0, 2, commonPoint1);
                nextNeighbour->patch->SetData(1, 2, 2.0 * commonPoint1 - otherPoint1);

                nextNeighbour->patch->GetData(0, 2, otherPoint0);
                nextNeighbour->patch->GetData(1, 2, otherPoint1);
                nextNeighbour->patch->GetData(1, 3, commonPoint1);
                break;
            default:
                break;
            }

            nextNeighbour->patch->UpdateVertexBufferObjectsOfData();
            if (nextNeighbour->img)
            {
                delete nextNeighbour->img;
            }
            nextNeighbour->img = nextNeighbour->patch->GenerateImage(_u_div_point_count, _v_div_point_count);
            nextNeighbour->img->UpdateVertexBufferObjects();
            generateIsoLinesUpdateVBO(nextNeighbour);

            Direction nextDir = getNextDirCounterClockwise(neighbourDir);

            neighbourDir = nextNeighbour->neighboursDir[nextDir];
            nextNeighbour = nextNeighbour->neighbours[nextDir];
        }
        while (nextNeighbour != pattr1 && nextNeighbour != pattr2 && nextNeighbour != nullptr);
        return nextNeighbour;
    }

    BiquarticCompositeSurface3::Direction BiquarticCompositeSurface3::getNextDirClockwise8(Direction dir)
    {
        switch(dir)
        {
        case NE:
            return N;
        case SE:
            return E;
        case SW:
            return S;
        case NW:
            return W;
        case N:
            return NW;
        case W:
            return SW;
        case S:
            return SE;
        case E:
            return NE;
        default:
            return NoDir;
        }
    }

    BiquarticCompositeSurface3::Direction BiquarticCompositeSurface3::getNextDirCounterClockwise8(Direction dir)
    {
        switch(dir)
        {
        case NW:
            return N;
        case SW:
            return W;
        case SE:
            return S;
        case NE:
            return E;
        case N:
            return NE;
        case E:
            return SE;
        case S:
            return SW;
        case W:
            return NW;
        default:
            return NoDir;
        }
    }

    BiquarticCompositeSurface3::Direction BiquarticCompositeSurface3::getNextDirClockwise(Direction dir)
    {
        switch(dir)
        {
        case N:
            return W;
        case E:
            return N;
        case S:
            return E;
        case W:
            return S;
        default:
            return NoDir;
        }
    }

    BiquarticCompositeSurface3::Direction BiquarticCompositeSurface3::getNextDirCounterClockwise(Direction dir)
    {
        switch(dir)
        {
        case N:
            return E;
        case W:
            return N;
        case S:
            return W;
        case E:
            return S;
        default:
            return NoDir;
        }
    }

    int BiquarticCompositeSurface3::materialToInt(const Material* mat)
    {
        if (mat == &MatFBBrass)
            return 0;
        if (mat == &MatFBGold)
            return 1;
        if (mat == &MatFBSilver)
            return 2;
        if (mat == &MatFBEmerald)
            return 3;
        if (mat == &MatFBPearl)
            return 4;
        if (mat == &MatFBTurquoise)
            return 5;
        return 0;
    }

    Material* BiquarticCompositeSurface3::intToMaterial(const int& i)
    {
        switch(i)
        {
        case 0:
            return &MatFBBrass;
        case 1:
            return &MatFBGold;
        case 2:
            return &MatFBSilver;
        case 3:
            return &MatFBEmerald;
        case 4:
            return &MatFBPearl;
        case 5:
            return &MatFBTurquoise;
        }
        return &MatFBBrass;
    }

    BiquarticCompositeSurface3::Direction BiquarticCompositeSurface3::intToDir(const int &i)
    {
        switch(i)
        {
        case 0:
            return N;
        case 1:
            return NW;
        case 2:
            return W;
        case 3:
            return SW;
        case 4:
            return S;
        case 5:
            return SE;
        case 6:
            return E;
        case 7:
            return NE;
        }
        return NoDir;
    }
}
