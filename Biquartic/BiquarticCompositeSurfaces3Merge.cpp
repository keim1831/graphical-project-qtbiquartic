#include "BiquarticCompositeSurfaces3.h"

namespace cagd
{
    GLvoid BiquarticCompositeSurface3::changeAllDiagonalNeighbours(PatchAttributes *attr, Direction connDir,
                                                                   const DCoordinate3 &pCommon, const DCoordinate3 &pvCW, const DCoordinate3 &pvCCW)
    {
        DCoordinate3 commonPoint0CW, commonPoint1CW, otherPoint0CW, otherPoint1CW;
        switch(connDir)
        {
        case SE:
            attr->patch->SetData(3, 3, pCommon);
            attr->patch->SetData(2, 3, 2.0 * pCommon - pvCW);
            attr->patch->SetData(3, 2, 2.0 * pCommon - pvCCW);

            attr->patch->UpdateVertexBufferObjectsOfData();
            if (attr->img)
            {
                delete attr->img;
            }
            attr->img = attr->patch->GenerateImage(_u_div_point_count, _v_div_point_count);
            attr->img->UpdateVertexBufferObjects();

            generateIsoLinesUpdateVBO(attr);

            commonPoint0CW = (*attr->patch)(3, 3);
            commonPoint1CW = (*attr->patch)(2, 3);
            otherPoint0CW = (*attr->patch)(3, 2);
            otherPoint1CW = (*attr->patch)(2, 2);
            break;

        case NE:
            attr->patch->SetData(0, 3, pCommon);
            attr->patch->SetData(0, 2, 2.0 * pCommon - pvCW);
            attr->patch->SetData(1, 3, 2.0 * pCommon - pvCCW);

            attr->patch->UpdateVertexBufferObjectsOfData();
            if (attr->img)
            {
                delete attr->img;
            }
            attr->img = attr->patch->GenerateImage(_u_div_point_count, _v_div_point_count);
            attr->img->UpdateVertexBufferObjects();

            generateIsoLinesUpdateVBO(attr);

            commonPoint0CW = (*attr->patch)(0, 3);
            commonPoint1CW = (*attr->patch)(0, 2);
            otherPoint0CW = (*attr->patch)(1, 3);
            otherPoint1CW = (*attr->patch)(1, 2);
            break;

        case NW:
            attr->patch->SetData(0, 0, pCommon);
            attr->patch->SetData(1, 0, 2.0 * pCommon - pvCW);
            attr->patch->SetData(0, 1, 2.0 * pCommon - pvCCW);

            attr->patch->UpdateVertexBufferObjectsOfData();
            if (attr->img)
            {
                delete attr->img;
            }
            attr->img = attr->patch->GenerateImage(_u_div_point_count, _v_div_point_count);
            attr->img->UpdateVertexBufferObjects();

            generateIsoLinesUpdateVBO(attr);

            commonPoint0CW = (*attr->patch)(0, 0);
            commonPoint1CW = (*attr->patch)(1, 0);
            otherPoint0CW = (*attr->patch)(0, 1);
            otherPoint1CW = (*attr->patch)(1, 1);
            break;

        case SW:
            attr->patch->SetData(3, 0, pCommon);
            attr->patch->SetData(3, 1, 2.0 * pCommon - pvCW);
            attr->patch->SetData(2, 0, 2.0 * pCommon - pvCCW);

            attr->patch->UpdateVertexBufferObjectsOfData();
            if (attr->img)
            {
                delete attr->img;
            }
            attr->img = attr->patch->GenerateImage(_u_div_point_count, _v_div_point_count);
            attr->img->UpdateVertexBufferObjects();

            generateIsoLinesUpdateVBO(attr);

            commonPoint0CW = (*attr->patch)(3, 0);
            commonPoint1CW = (*attr->patch)(3, 1);
            otherPoint0CW = (*attr->patch)(2, 0);
            otherPoint1CW = (*attr->patch)(2, 1);
            break;
        default:
            return;
        }

        PatchAttributes *clockWiseNeigh = attr->neighbours[getNextDirClockwise8(connDir)];
        if (clockWiseNeigh)
        {
            changeAllNeighboursClockwise(attr, attr, clockWiseNeigh, commonPoint0CW, commonPoint1CW,
                                          otherPoint0CW, otherPoint1CW, attr->neighboursDir[getNextDirClockwise8(connDir)]);
        }

        PatchAttributes *counterClockWiseNeigh = attr->neighbours[getNextDirCounterClockwise8(connDir)];
        if (counterClockWiseNeigh)
        {
            changeAllNeighboursCounterClockwise(attr, attr, counterClockWiseNeigh, commonPoint0CW, otherPoint0CW,
                                                 commonPoint1CW, otherPoint1CW, attr->neighboursDir[getNextDirCounterClockwise8(connDir)]);
        }
    }

    GLboolean BiquarticCompositeSurface3::setMergingPoints(PatchAttributes *pattr1, PatchAttributes *pattr2, Direction dir2,
                                                        DCoordinate3 &p0, DCoordinate3 &p1, DCoordinate3 &p2, DCoordinate3& p3,
                                                        const DCoordinate3 &pv0, const DCoordinate3 &pv1, const DCoordinate3 &pv2, const DCoordinate3 &pv3,
                                                        GLboolean &cw, GLboolean &ccw)
    {
        switch(dir2)
        {
        case N:
        {
            DCoordinate3 v0 = 0.5 * (pv0 + (*pattr2->patch)(1,3));
            DCoordinate3 v1 = 0.5 * (pv1 + (*pattr2->patch)(1,2));
            DCoordinate3 v2 = 0.5 * (pv2 + (*pattr2->patch)(1,1));
            DCoordinate3 v3 = 0.5 * (pv3 + (*pattr2->patch)(1,0));

            p0 = v0;
            p1 = v1;
            p2 = v2;
            p3 = v3;

            DCoordinate3 &r00 = (*pattr2->patch)(0,0);
            DCoordinate3 &r01 = (*pattr2->patch)(0,1);
            DCoordinate3 &r02 = (*pattr2->patch)(0,2);
            DCoordinate3 &r03 = (*pattr2->patch)(0,3);

            r03 = v0;
            r02 = v1;
            r01 = v2;
            r00 = v3;

            if (pattr2->neighbours[W])
            {
                ccw = (changeAllNeighboursClockwise(pattr1, pattr2, pattr2->neighbours[W],
                                                         r00, (*pattr2->patch)(1, 0),
                                                         r01, (*pattr2->patch)(1, 1),
                                                         pattr2->neighboursDir[W])) != pattr1;
            }
            else if (pattr2->neighbours[NW])
            {
                changeAllDiagonalNeighbours(pattr2->neighbours[NW], pattr2->neighboursDir[NW], r00, (*pattr2->patch)(1,0), r01);
            }

            if (pattr2->neighbours[E])
            {
                cw = changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr2->neighbours[E],
                                                              r03, (*pattr2->patch)(1, 3),
                                                              r02, (*pattr2->patch)(1, 2),
                                                              pattr2->neighboursDir[E]) != pattr1;
            }
            else if (pattr2->neighbours[NE])
            {
                changeAllDiagonalNeighbours(pattr2->neighbours[NE], pattr2->neighboursDir[NE], r03, r02, (*pattr2->patch)(1, 3));
            }

            return GL_TRUE;
        }

        case S:
        {
            DCoordinate3 v0 = 0.5 * (pv0 + (*pattr2->patch)(2,0));
            DCoordinate3 v1 = 0.5 * (pv1 + (*pattr2->patch)(2,1));
            DCoordinate3 v2 = 0.5 * (pv2 + (*pattr2->patch)(2,2));
            DCoordinate3 v3 = 0.5 * (pv3 + (*pattr2->patch)(2,3));

            p0 = v0;
            p1 = v1;
            p2 = v2;
            p3 = v3;

            DCoordinate3 &r30 = (*pattr2->patch)(3,0);
            DCoordinate3 &r31 = (*pattr2->patch)(3,1);
            DCoordinate3 &r32 = (*pattr2->patch)(3,2);
            DCoordinate3 &r33 = (*pattr2->patch)(3,3);

            r30 = v0;
            r31 = v1;
            r32 = v2;
            r33 = v3;

            if (pattr2->neighbours[E])
            {
                ccw = changeAllNeighboursClockwise(pattr1, pattr2, pattr2->neighbours[E],
                                                        r33, (*pattr2->patch)(2, 3),
                                                        r32, (*pattr2->patch)(2, 2),
                                                        pattr2->neighboursDir[E]) != pattr1;
            }
            else if (pattr2->neighbours[SE])
            {
                changeAllDiagonalNeighbours(pattr2->neighbours[SE], pattr2->neighboursDir[SE], r33, (*pattr2->patch)(2, 3), r32);
            }

            if (pattr2->neighbours[W])
            {
                cw = changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr2->neighbours[W],
                                                              r30, (*pattr2->patch)(2, 0),
                                                              r31, (*pattr2->patch)(2, 1),
                                                              pattr2->neighboursDir[W]) != pattr1;
            }
            else if (pattr2->neighbours[SW])
            {
                changeAllDiagonalNeighbours(pattr2->neighbours[SW], pattr2->neighboursDir[SW], r30, r31, (*pattr2->patch)(2, 0));
            }

            return GL_TRUE;
        }

        case W:
        {
            DCoordinate3 v0 = 0.5 * (pv0 + (*pattr2->patch)(0,1));
            DCoordinate3 v1 = 0.5 * (pv1 + (*pattr2->patch)(1,1));
            DCoordinate3 v2 = 0.5 * (pv2 + (*pattr2->patch)(2,1));
            DCoordinate3 v3 = 0.5 * (pv3 + (*pattr2->patch)(3,1));

            p0 = v0;
            p1 = v1;
            p2 = v2;
            p3 = v3;

            DCoordinate3 &r00 = (*pattr2->patch)(0,0);
            DCoordinate3 &r10 = (*pattr2->patch)(1,0);
            DCoordinate3 &r20 = (*pattr2->patch)(2,0);
            DCoordinate3 &r30 = (*pattr2->patch)(3,0);

            r00 = v0;
            r10 = v1;
            r20 = v2;
            r30 = v3;

            if (pattr2->neighbours[S])
            {
                ccw = changeAllNeighboursClockwise(pattr1, pattr2, pattr2->neighbours[S],
                                                        r30, (*pattr2->patch)(3, 1),
                                                        r20, (*pattr2->patch)(2, 1),
                                                        pattr2->neighboursDir[S]) != pattr1;
            }
            else if (pattr2->neighbours[SW])
            {
                changeAllDiagonalNeighbours(pattr2->neighbours[SW], pattr2->neighboursDir[SW], r30, (*pattr2->patch)(3, 1), r20);
            }

            if (pattr2->neighbours[N])
            {
                cw = changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr2->neighbours[N],
                                                              r00, (*pattr2->patch)(0, 1),
                                                              r10, (*pattr2->patch)(1, 1),
                                                              pattr2->neighboursDir[N]) != pattr1;
            }
            else if (pattr2->neighbours[NW])
            {
               changeAllDiagonalNeighbours(pattr2->neighbours[NW], pattr2->neighboursDir[NW], r00, r10, (*pattr2->patch)(0, 1));
            }

            return GL_TRUE;
        }

        case E:
        {
            DCoordinate3 v0 = 0.5 * (pv0 + (*pattr2->patch)(3,2));
            DCoordinate3 v1 = 0.5 * (pv1 + (*pattr2->patch)(2,2));
            DCoordinate3 v2 = 0.5 * (pv2 + (*pattr2->patch)(1,2));
            DCoordinate3 v3 = 0.5 * (pv3 + (*pattr2->patch)(0,2));

            p0 = v0;
            p1 = v1;
            p2 = v2;
            p3 = v3;

            DCoordinate3 &r33 = (*pattr2->patch)(3,3);
            DCoordinate3 &r23 = (*pattr2->patch)(2,3);
            DCoordinate3 &r13 = (*pattr2->patch)(1,3);
            DCoordinate3 &r03 = (*pattr2->patch)(0,3);

            r33 = v0;
            r23 = v1;
            r13 = v2;
            r03 = v3;

            if (pattr2->neighbours[N])
            {
                ccw = changeAllNeighboursClockwise(pattr1, pattr2, pattr2->neighbours[N],
                                                        r03, (*pattr2->patch)(0, 2),
                                                        r13, (*pattr2->patch)(1, 2),
                                                        pattr2->neighboursDir[N]) != pattr1;
            }
            else if (pattr2->neighbours[NE])
            {
                changeAllDiagonalNeighbours(pattr2->neighbours[NE], pattr2->neighboursDir[NE], r03, (*pattr2->patch)(0, 2), r13);
            }

            if (pattr2->neighbours[S])
            {
                cw = changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr2->neighbours[S],
                                                              r33, (*pattr2->patch)(3, 2),
                                                              r23, (*pattr2->patch)(2, 2),
                                                              pattr2->neighboursDir[S]) != pattr1;
            }
            else if (pattr2->neighbours[SE])
            {
                changeAllDiagonalNeighbours(pattr2->neighbours[SE], pattr2->neighboursDir[SE], r33, r23, (*pattr2->patch)(3, 2));
            }

            return GL_TRUE;
        }

        default:
            return GL_FALSE;
        }
    }

    GLboolean BiquarticCompositeSurface3::setMergingPointsDiagonal(PatchAttributes *pattr1, PatchAttributes *pattr2, Direction dir2,
                                                                DCoordinate3 &pm0, DCoordinate3 &pm1, DCoordinate3 &pm2,
                                                                GLboolean &cw, GLboolean &ccw)
    {
        switch(dir2)
        {
        case SW:
        {
            DCoordinate3 &r30 = (*pattr2->patch)(3,0);
            DCoordinate3 &r20 = (*pattr2->patch)(2,0);
            DCoordinate3 &r31 = (*pattr2->patch)(3,1);

            DCoordinate3 first = 0.5 * (pm2 + r20);
            DCoordinate3 second = 0.5 * (pm0 + r31);

            DCoordinate3 commonPoint = 0.5 * (first + second);

            pm1 = commonPoint;
            r30 = commonPoint;

            DCoordinate3 firstDiff = first - commonPoint;
            DCoordinate3 secondDiff = second - commonPoint;

            pm0 += firstDiff;
            r31 += firstDiff;

            pm2 += secondDiff;
            r20 += secondDiff;

            if (pattr2->neighbours[S])
            {
                ccw = changeAllNeighboursClockwise(pattr1, pattr2, pattr2->neighbours[S],
                                                        r30, (*pattr2->patch)(3, 1),
                                                        r20, (*pattr2->patch)(2, 1),
                                                        pattr2->neighboursDir[S]) != pattr1;
            }

            if (pattr2->neighbours[W])
            {
                cw = changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr2->neighbours[W],
                                                              r30, (*pattr2->patch)(2, 0),
                                                              r31, (*pattr2->patch)(2, 1),
                                                              pattr2->neighboursDir[W]) != pattr1;
            }

            return GL_TRUE;
        }

        case NW:
        {
            DCoordinate3 &r00 = (*pattr2->patch)(0,0);
            DCoordinate3 &r01 = (*pattr2->patch)(0,1);
            DCoordinate3 &r10 = (*pattr2->patch)(1,0);

            DCoordinate3 first = 0.5 * (pm2 + r01);
            DCoordinate3 second = 0.5 * (pm0 + r10);

            DCoordinate3 commonPoint = 0.5 * (first + second);

            pm1 = commonPoint;
            r00 = commonPoint;

            DCoordinate3 firstDiff = first - commonPoint;
            DCoordinate3 secondDiff = second - commonPoint;

            pm0 += firstDiff;
            r10 += firstDiff;

            pm2 += secondDiff;
            r01 += secondDiff;

            if (pattr2->neighbours[N])
            {
                cw = changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr2->neighbours[N],
                                                              r00, (*pattr2->patch)(0, 1),
                                                              r10, (*pattr2->patch)(1, 1),
                                                              pattr2->neighboursDir[N]) != pattr1;
            }

            if (pattr2->neighbours[W])
            {
                ccw = (changeAllNeighboursClockwise(pattr1, pattr2, pattr2->neighbours[W],
                                                         r00, (*pattr2->patch)(1, 0),
                                                         r01, (*pattr2->patch)(1, 1),
                                                         pattr2->neighboursDir[W])) != pattr1;
            }
            return GL_TRUE;
        }

        case NE:
        {
            DCoordinate3 &r03 = (*pattr2->patch)(0,3);
            DCoordinate3 &r13 = (*pattr2->patch)(1,3);
            DCoordinate3 &r02 = (*pattr2->patch)(0,2);

            DCoordinate3 first = 0.5 * (pm2 + r13);
            DCoordinate3 second = 0.5 * (pm0 + r02);

            DCoordinate3 commonPoint = 0.5 * (first + second);

            pm1 = commonPoint;
            r03 = commonPoint;

            DCoordinate3 firstDiff = first - commonPoint;
            DCoordinate3 secondDiff = second - commonPoint;

            pm0 += firstDiff;
            r02 += firstDiff;

            pm2 += secondDiff;
            r13 += secondDiff;

            if (pattr2->neighbours[N])
            {
                ccw = changeAllNeighboursClockwise(pattr1, pattr2, pattr2->neighbours[N],
                                                        r03, (*pattr2->patch)(0, 2),
                                                        r13, (*pattr2->patch)(1,2),
                                                        pattr2->neighboursDir[N]) != pattr1;
            }

            if (pattr2->neighbours[E])
            {
                cw = changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr2->neighbours[E],
                                                              r03, (*pattr2->patch)(1, 3),
                                                              r02, (*pattr2->patch)(1, 2),
                                                              pattr2->neighboursDir[E]) != pattr1;
            }
            return GL_TRUE;
        }

        case SE:
        {
            DCoordinate3 &r33 = (*pattr2->patch)(3,3);
            DCoordinate3 &r32 = (*pattr2->patch)(3,2);
            DCoordinate3 &r23 = (*pattr2->patch)(2,3);

            DCoordinate3 first = 0.5 * (pm2 + r32);
            DCoordinate3 second = 0.5 * (pm0 + r23);

            DCoordinate3 commonPoint = 0.5 * (first + second);

            pm1 = commonPoint;
            r33 = commonPoint;

            DCoordinate3 firstDiff = first - commonPoint;
            DCoordinate3 secondDiff = second - commonPoint;

            pm0 += firstDiff;
            r23 += firstDiff;

            pm2 += secondDiff;
            r32 += secondDiff;

            if (pattr2->neighbours[S])
            {
                cw = changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr2->neighbours[S],
                                                              r33, (*pattr2->patch)(3, 2),
                                                              r23, (*pattr2->patch)(2, 2),
                                                              pattr2->neighboursDir[S]) != pattr1;
            }

            if (pattr2->neighbours[E])
            {
                ccw = changeAllNeighboursClockwise(pattr1, pattr2, pattr2->neighbours[E],
                                                        r33, (*pattr2->patch)(2, 3),
                                                        r32, (*pattr2->patch)(2, 2),
                                                        pattr2->neighboursDir[E]) != pattr1;
            }
            return GL_TRUE;
        }
        default:
            return GL_FALSE;
        }
    }
}
