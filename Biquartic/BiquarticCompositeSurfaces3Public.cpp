#include "BiquarticCompositeSurfaces3.h"
#include <iostream>
#include <ctime>
#include <fstream>
#include <QTransform>
#include <QVector3D>
#include <QMatrix4x4>
#include "../Core/Constants.h"
using namespace std;

namespace cagd
{
    BiquarticCompositeSurface3::~BiquarticCompositeSurface3()
    {
        _attributes.clear();
    }

    BiquarticCompositeSurface3::BiquarticCompositeSurface3(ShaderProgram shaders[], const size_t min_patch_count)
    {
        srand((GLuint) time(NULL));

        _shaders = shaders;
        _attributes.reserve(min_patch_count);
        PatchAttributes firstAttr;

        if (firstAttr.patch)
        {
            cout << "First biquartic patch created" << endl;
            firstAttr.patch->SetData(0, 0, -2.0, -2.0, 0.0);
            firstAttr.patch->SetData(0, 1, -2.0, -1.0, 0.0);
            firstAttr.patch->SetData(0, 2, -2.0, 1.0, 0.0);
            firstAttr.patch->SetData(0, 3, -2.0, 2.0, 0.0);

            firstAttr.patch->SetData(1, 0, -1.0, -2.0, 0.0);
            firstAttr.patch->SetData(1, 1, -1.0, -1.0, 2.0);
            firstAttr.patch->SetData(1, 2, -1.0, 1.0, 2.0);
            firstAttr.patch->SetData(1, 3, -1.0, 2.0, 0.0);

            firstAttr.patch->SetData(2, 0, 1.0, -2.0, 0.0);
            firstAttr.patch->SetData(2, 1, 1.0, -1.0, 2.0);
            firstAttr.patch->SetData(2, 2, 1.0, 1.0, 2.0);
            firstAttr.patch->SetData(2, 3, 1.0, 2.0, 0.0);

            firstAttr.patch->SetData(3, 0, 2.0, -2.0, 0.0);
            firstAttr.patch->SetData(3, 1, 2.0, -1.0, 0.0);
            firstAttr.patch->SetData(3, 2, 2.0, 1.0, 0.0);
            firstAttr.patch->SetData(3, 3, 2.0, 2.0, 0.0);
            firstAttr.img = firstAttr.patch->GenerateImage(_u_div_point_count, _v_div_point_count);
        }

        _attributes.push_back(firstAttr);
        _attributes[0].patch->UpdateVertexBufferObjectsOfData();
        _attributes[0].img->UpdateVertexBufferObjects();
        _attributes[0].u_iso_lines = _attributes[0].patch->GenerateUIsoparametricLines(_iso_line_count_u, 1, _u_div_point_count);
        _attributes[0].v_iso_lines = _attributes[0].patch->GenerateVIsoparametricLines(_iso_line_count_v, 1, _v_div_point_count);
        generateIsoLinesUpdateVBO(&_attributes[0]);
        cout << "First Patch\n";
    }

    GLvoid BiquarticCompositeSurface3::renderAllPatches(int selected, int render_data, int render_iso, int render_deriv, int render_normals,
                                                        int row, int col, bool is_selected, int tex, bool preview)
    {
        glEnable(GL_NORMALIZE);

        // render patch
        glEnable(GL_LIGHTING);
        int index = 0;

        if (_alpha_on)
        {
            glEnable(GL_BLEND);
            glDepthMask(GL_FALSE);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE);
        }

        if (shader_ind != -1)
        {
            _shaders[shader_ind].Enable();

            if (shader_ind == 2)
            {
                _shaders[2].SetUniformVariable1f("scale_factor", _reflection_lines_unif_params[0]);
                _shaders[2].SetUniformVariable1f("smoothing", _reflection_lines_unif_params[1]);
                _shaders[2].SetUniformVariable1f("shading", _reflection_lines_unif_params[2]);
            }
            else if (shader_ind == 3)
                _shaders[3].SetUniformVariable4fv("default_outline_color", 1, _toon_def_color);
        }

        if (tex)
            glEnable(GL_TEXTURE_2D);

        if (!preview)
        {
            for (vector<PatchAttributes>::iterator it = _attributes.begin(); it != _attributes.end(); ++it, ++index)
            {
                if (index != selected)
                    renderSelectedPatch(&(*it), (*it->material));
                else
                    renderSelectedPatch(&(*it), MatFBRuby);
            }
        }
        else if (_op.newAttr)
        {
            for (vector<PatchAttributes>::iterator it = _attributes.begin(); it != _attributes.end(); ++it, ++index)
            {
                renderSelectedPatch(&(*it), (*it->material));
            }
            renderSelectedPatch(_op.newAttr, MatFBRuby);
        }


        if (tex)
            glDisable(GL_TEXTURE_2D);

        if(shader_ind != -1)
        {
            _shaders[shader_ind].Disable();
        }

        if (_alpha_on)
        {
            glDepthMask(GL_TRUE);
            glDisable(GL_BLEND);
        }

        glDisable(GL_LIGHTING);

        index = 0;

        // render isolines/derivatives/normals

        if (!preview)
        {
            for (vector<PatchAttributes>::iterator it = _attributes.begin(); it != _attributes.end(); ++it, ++index)
            {
                renderSelectedPatchLines(&(*it), render_data, render_iso, render_deriv, render_normals);
            }

            if (is_selected)
            {
                glPointSize(8.0);
                glColor3f(0.12f, 0.89f, 0.34f);
                _attributes[selected].patch->RenderSelectedPoint(row, col);
            }
        }
        else if (_op.newAttr)
        {
            for (vector<PatchAttributes>::iterator it = _attributes.begin(); it != _attributes.end(); ++it, ++index)
            {
                renderSelectedPatchLines(&(*it), render_data, render_iso, render_deriv, render_normals);
            }
            renderSelectedPatchLines(_op.newAttr, render_data, render_iso, render_deriv, render_normals);
        }

        glDisable(GL_NORMALIZE);
    }

    GLvoid BiquarticCompositeSurface3::renderSelectedPatch(PatchAttributes* pattr, Material &mat)
    {
        mat.Apply();
        if (pattr->img)
        {
            pattr->img->Render();
        }
    }

    GLvoid BiquarticCompositeSurface3::renderSelectedPatchLines(PatchAttributes* pattr, int render_data, int render_iso, int render_deriv, int render_normals)
    {
        if (pattr->patch)
        {
            glLineWidth(0.1f);
            if (render_data)
            {
                glColor3f(0.45f, 0.76f, 0.71f);
                pattr->patch->RenderDataPoints();
            }

            if (render_iso)
            {
                glColor3f(0.55f, 0.88f, 0.21f);

                if (pattr->u_iso_lines && pattr->v_iso_lines)
                {
                    for (GLuint i = 0; i < pattr->u_iso_lines->GetColumnCount(); ++i)
                    {
                        if ((*pattr->u_iso_lines)[i])
                        {
                            (*pattr->u_iso_lines)[i]->RenderDerivatives(0, GL_LINE_STRIP);
                        }
                     }

                    for (GLuint i = 0; i < pattr->v_iso_lines->GetColumnCount(); ++i)
                    {
                        if ((*pattr->v_iso_lines)[i])
                        {
                            (*pattr->v_iso_lines)[i]->RenderDerivatives(0, GL_LINE_STRIP);
                        }
                    }
                }
            }

            if (render_deriv)
            {
                glColor3f(0.55f, 0.34f, 0.56f);

                if (pattr->u_iso_lines && pattr->v_iso_lines)
                {
                    for (GLuint i = 0; i < pattr->u_iso_lines->GetColumnCount(); ++i)
                    {
                        if ((*pattr->u_iso_lines)[i])
                        {
                            (*pattr->u_iso_lines)[i]->RenderDerivatives(1, GL_LINES);
                        }
                     }

                    for (GLuint i = 0; i < pattr->v_iso_lines->GetColumnCount(); ++i)
                    {
                        if ((*pattr->v_iso_lines)[i])
                        {
                           (*pattr->v_iso_lines)[i]->RenderDerivatives(1, GL_LINES);
                        }
                    }
                }
            }

            if (render_normals)
            {
                glColor3f(0.33f, 0.66f, 0.71f);
                pattr->img->RenderNormal();
            }
        }
    }

    GLvoid BiquarticCompositeSurface3::renderClickableObjects()
    {
        int index = 0;
        for (vector<PatchAttributes>::iterator it = _attributes.begin(); it != _attributes.end(); ++it, ++index)
        {
            if (it->patch)
            {
                for (int i = 0; i < 4; ++i)
                {
                    for (int j = 0; j < 4; ++j)
                    {
                        glLoadName(index * 16 + 4 * i + j);
                        it->patch->RenderSelectedPoint(i, j);
                    }
                }
            }
        }
    }

    GLboolean BiquarticCompositeSurface3::addToDataPointValue(const GLuint &patchInd, const GLuint &dataPointRow, const GLuint &dataPointCol,
                                                               const GLuint &pointComponentInd, const GLdouble &diff)
    {
        if (patchInd >= _attributes.size())
            return GL_FALSE;

        PatchAttributes *pattr1 = &_attributes[patchInd];
        DCoordinate3 &p = (*pattr1->patch)(dataPointRow, dataPointCol);

        GLdouble val = p[pointComponentInd] + diff;

        if (p[pointComponentInd] == val)
            return GL_FALSE;

        p[pointComponentInd] = val;

        pattr1->patch->UpdateVertexBufferObjectsOfData();
        if (_attributes[patchInd].img)
            delete  _attributes[patchInd].img;
        pattr1->img = _attributes[patchInd].patch->GenerateImage(_u_div_point_count, _v_div_point_count);
        pattr1->img->UpdateVertexBufferObjects();
        generateIsoLinesUpdateVBO(pattr1);

        bool neighDir1 = false, neighDir2 = false;
        PatchAttributes *neighDiag;

        if (dataPointRow <= 1 && dataPointCol >= 2) // NE
        {
            DCoordinate3 &p03 = (*pattr1->patch)(0,3);
            DCoordinate3 &p02 = (*pattr1->patch)(0,2);
            DCoordinate3 &p13 = (*pattr1->patch)(1,3);

            neighDiag = pattr1->neighbours[NE];

            if (pattr1->neighbours[E])
            {
                neighDir1 = true;
                PatchAttributes *nextNeighbour = pattr1->neighbours[E];
                DCoordinate3 commonPoint0 = p03;
                DCoordinate3 commonPoint1 = (*pattr1->patch)(1,3);
                DCoordinate3 otherPoint0 = p02;
                DCoordinate3 otherPoint1 = (*pattr1->patch)(1,2);
                Direction neighbourDir = pattr1->neighboursDir[E];
                changeAllNeighboursCounterClockwise(pattr1, pattr1, nextNeighbour, commonPoint0, commonPoint1 ,otherPoint0, otherPoint1, neighbourDir);

                Direction diagonalDir = getNextDirCounterClockwise8(pattr1->neighboursDir[E]);
                PatchAttributes *nnDiag = nextNeighbour->neighbours[diagonalDir];
                if (nnDiag && pattr1->neighbours[N] != nnDiag &&
                        (!neighDiag || neighDiag->neighbours[getNextDirCounterClockwise8(pattr1->neighboursDir[NE])] != nnDiag))
                {
                    cout << "Done1\n";
                    changeAllDiagonalNeighbours(nnDiag, nextNeighbour->neighboursDir[diagonalDir], p03, p13, 2.0 * p03 - p02);
                }
            }

            if (pattr1->neighbours[N])
            {
                neighDir2 = true;
                PatchAttributes *nextNeighbour = pattr1->neighbours[N];
                DCoordinate3 commonPoint0 = p03;
                DCoordinate3 commonPoint1 = (*pattr1->patch)(0,2);
                DCoordinate3 otherPoint0 = p13;
                DCoordinate3 otherPoint1 = (*pattr1->patch)(1,2);
                Direction neighbourDir = pattr1->neighboursDir[N];
                changeAllNeighboursClockwise(pattr1, pattr1, nextNeighbour, commonPoint0, commonPoint1 ,otherPoint0, otherPoint1, neighbourDir);

                Direction diagonalDir = getNextDirClockwise8(pattr1->neighboursDir[N]);
                PatchAttributes *nnDiag = nextNeighbour->neighbours[diagonalDir];
                if (nnDiag && pattr1->neighbours[E] != nnDiag &&
                        (!neighDiag || neighDiag->neighbours[getNextDirClockwise8(pattr1->neighboursDir[NE])] != nnDiag))
                {
                    cout << "Done2\n";
                    changeAllDiagonalNeighbours(nnDiag, nextNeighbour->neighboursDir[diagonalDir], p03, 2.0 * p03 - p13, p02);
                }
            }

            if (neighDiag &&
                    (!neighDir1 || (pattr1->neighbours[E]->neighbours[getNextDirCounterClockwise(pattr1->neighboursDir[E])] != neighDiag)) &&
                    (!neighDir2 || (pattr1->neighbours[N]->neighbours[getNextDirClockwise(pattr1->neighboursDir[N])] != neighDiag)))
            {
                Direction connDir = pattr1->neighboursDir[NE];
                changeAllDiagonalNeighbours(neighDiag, connDir, p03, p02, p13);
            }
        }
        else if (dataPointRow <= 1 && dataPointCol <= 1) // NW
        {
            DCoordinate3 &p00 = (*pattr1->patch)(0,0);
            DCoordinate3 &p10 = (*pattr1->patch)(1,0);
            DCoordinate3 &p01 = (*pattr1->patch)(0,1);

            neighDiag = pattr1->neighbours[NW];

            if (pattr1->neighbours[W])
            {
                neighDir1 = true;
                PatchAttributes *nextNeighbour = pattr1->neighbours[W];
                DCoordinate3 commonPoint0 = p00;
                DCoordinate3 commonPoint1 = (*pattr1->patch)(1,0);
                DCoordinate3 otherPoint0 = p01;
                DCoordinate3 otherPoint1 = (*pattr1->patch)(1,1);
                Direction neighbourDir = pattr1->neighboursDir[W];
                changeAllNeighboursClockwise(pattr1, pattr1, nextNeighbour, commonPoint0, commonPoint1 ,otherPoint0, otherPoint1, neighbourDir);

                Direction diagonalDir = getNextDirClockwise8(pattr1->neighboursDir[W]);
                PatchAttributes *nnDiag = nextNeighbour->neighbours[diagonalDir];
                if (nnDiag && pattr1->neighbours[N] != nnDiag &&
                        (!neighDiag || neighDiag->neighbours[getNextDirClockwise8(pattr1->neighboursDir[NW])] != nnDiag))
                {
                    cout << "Done3\n";
                    changeAllDiagonalNeighbours(nnDiag, nextNeighbour->neighboursDir[diagonalDir], p00, 2.0 * p00 - p01, p10);
                }
            }

            if (pattr1->neighbours[N])
            {
                neighDir2 = true;
                PatchAttributes *nextNeighbour = pattr1->neighbours[N];
                DCoordinate3 commonPoint0 = p00;
                DCoordinate3 commonPoint1 = (*pattr1->patch)(0,1);
                DCoordinate3 otherPoint0 = p10;
                DCoordinate3 otherPoint1 = (*pattr1->patch)(1,1);
                Direction neighbourDir = pattr1->neighboursDir[N];
                changeAllNeighboursCounterClockwise(pattr1, pattr1, nextNeighbour, commonPoint0, commonPoint1 ,otherPoint0, otherPoint1, neighbourDir);

                Direction diagonalDir = getNextDirCounterClockwise8(pattr1->neighboursDir[N]);
                PatchAttributes *nnDiag = nextNeighbour->neighbours[diagonalDir];
                if (nnDiag && pattr1->neighbours[W] != nnDiag &&
                        (!neighDiag || neighDiag->neighbours[getNextDirCounterClockwise8(pattr1->neighboursDir[NW])] != nnDiag))
                {
                    cout << "Done4\n";
                    changeAllDiagonalNeighbours(nnDiag, nextNeighbour->neighboursDir[diagonalDir], p00, p01, 2.0 * p00 - p10);
                }
            }

            if (neighDiag &&
                    (!neighDir1 || (pattr1->neighbours[W]->neighbours[getNextDirClockwise(pattr1->neighboursDir[W])] != neighDiag)) &&
                    (!neighDir2 || (pattr1->neighbours[N]->neighbours[getNextDirCounterClockwise(pattr1->neighboursDir[N])] != neighDiag)))
            {
                Direction connDir = pattr1->neighboursDir[NW];
                changeAllDiagonalNeighbours(neighDiag, connDir, p00, p10, p01);
            }
        }
        else if (dataPointRow >= 2 && dataPointCol <= 1) // SW
        {
            DCoordinate3 &p30 = (*pattr1->patch)(3,0);
            DCoordinate3 &p31 = (*pattr1->patch)(3,1);
            DCoordinate3 &p20 = (*pattr1->patch)(2,0);

            neighDiag = pattr1->neighbours[SW];

            if (pattr1->neighbours[S])
            {
                neighDir1 = true;
                PatchAttributes *nextNeighbour = pattr1->neighbours[S];
                DCoordinate3 commonPoint0 = p30;
                DCoordinate3 commonPoint1 = (*pattr1->patch)(3,1);
                DCoordinate3 otherPoint0 = p20;
                DCoordinate3 otherPoint1 = (*pattr1->patch)(2,1);
                Direction neighbourDir = pattr1->neighboursDir[S];
                changeAllNeighboursClockwise(pattr1, pattr1, nextNeighbour, commonPoint0, commonPoint1 ,otherPoint0, otherPoint1, neighbourDir);

                Direction diagonalDir = getNextDirClockwise8(pattr1->neighboursDir[S]);
                PatchAttributes *nnDiag = nextNeighbour->neighbours[diagonalDir];
                if (nnDiag && pattr1->neighbours[W] != nnDiag &&
                        (!neighDiag || neighDiag->neighbours[getNextDirClockwise8(pattr1->neighboursDir[SW])] != nnDiag))
                {
                    cout << "Done5\n";
                    changeAllDiagonalNeighbours(nnDiag, nextNeighbour->neighboursDir[diagonalDir], p30, 2.0 * p30 - p20, p31);
                }
            }

            if (pattr1->neighbours[W])
            {
                neighDir2 = true;
                PatchAttributes *nextNeighbour = pattr1->neighbours[W];
                DCoordinate3 commonPoint0 = p30;
                DCoordinate3 commonPoint1 = (*pattr1->patch)(2,0);
                DCoordinate3 otherPoint0 = p31;
                DCoordinate3 otherPoint1 = (*pattr1->patch)(2,1);
                Direction neighbourDir = pattr1->neighboursDir[W];
                changeAllNeighboursCounterClockwise(pattr1, pattr1, nextNeighbour, commonPoint0, commonPoint1 ,otherPoint0, otherPoint1, neighbourDir);

                Direction diagonalDir = getNextDirCounterClockwise8(pattr1->neighboursDir[W]);
                PatchAttributes *nnDiag = nextNeighbour->neighbours[diagonalDir];
                if (nnDiag && pattr1->neighbours[S] != nnDiag &&
                        (!neighDiag || neighDiag->neighbours[getNextDirCounterClockwise8(pattr1->neighboursDir[SW])] != nnDiag))
                {
                    cout << "Done6\n";
                    changeAllDiagonalNeighbours(nnDiag, nextNeighbour->neighboursDir[diagonalDir], p30, p20, 2.0 * p30 - p31);
                }
            }

            if (pattr1->neighbours[SW] &&
                    (!neighDir1 || (pattr1->neighbours[S]->neighbours[getNextDirClockwise(pattr1->neighboursDir[S])] != neighDiag)) &&
                    (!neighDir2 || (pattr1->neighbours[W]->neighbours[getNextDirCounterClockwise(pattr1->neighboursDir[W])] != neighDiag)))
            {
                Direction connDir = pattr1->neighboursDir[SW];
                changeAllDiagonalNeighbours(neighDiag, connDir, p30, p31, p20);
            }
        }
        else if (dataPointRow >= 2 && dataPointCol >= 2) // SE
        {
            DCoordinate3 &p33 = (*pattr1->patch)(3,3);
            DCoordinate3 &p23 = (*pattr1->patch)(2,3);
            DCoordinate3 &p32 = (*pattr1->patch)(3,2);

            neighDiag = pattr1->neighbours[SE];

            if (pattr1->neighbours[S])
            {
                neighDir1 = true;
                PatchAttributes *nextNeighbour = pattr1->neighbours[S];
                DCoordinate3 commonPoint0 = p33;
                DCoordinate3 commonPoint1 = (*pattr1->patch)(3,2);
                DCoordinate3 otherPoint0 = p23;
                DCoordinate3 otherPoint1 = (*pattr1->patch)(2,2);
                Direction neighbourDir = pattr1->neighboursDir[S];
                changeAllNeighboursCounterClockwise(pattr1, pattr1, nextNeighbour, commonPoint0, commonPoint1 ,otherPoint0, otherPoint1, neighbourDir);

                Direction diagonalDir = getNextDirCounterClockwise8(pattr1->neighboursDir[S]);
                PatchAttributes *nnDiag = nextNeighbour->neighbours[diagonalDir];
                if (nnDiag && pattr1->neighbours[E] != nnDiag &&
                        (!neighDiag || neighDiag->neighbours[getNextDirCounterClockwise8(pattr1->neighboursDir[SE])] != nnDiag))
                {
                    cout << "Done7\n";
                    changeAllDiagonalNeighbours(nnDiag, nextNeighbour->neighboursDir[diagonalDir], p33, p32, 2.0 * p33 - p23);
                }
            }

            if (pattr1->neighbours[E])
            {
                neighDir2 = true;
                PatchAttributes *nextNeighbour = pattr1->neighbours[E];
                DCoordinate3 commonPoint0 = p33;
                DCoordinate3 commonPoint1 = (*pattr1->patch)(2,3);
                DCoordinate3 otherPoint0 = p32;
                DCoordinate3 otherPoint1 = (*pattr1->patch)(2,2);
                Direction neighbourDir = pattr1->neighboursDir[E];
                changeAllNeighboursClockwise(pattr1, pattr1, nextNeighbour, commonPoint0, commonPoint1 ,otherPoint0, otherPoint1, neighbourDir);

                Direction diagonalDir = getNextDirClockwise8(pattr1->neighboursDir[E]);
                PatchAttributes *nnDiag = nextNeighbour->neighbours[diagonalDir];
                if (nnDiag && pattr1->neighbours[S] != nnDiag &&
                        (!neighDiag || neighDiag->neighbours[getNextDirClockwise8(pattr1->neighboursDir[SE])] != nnDiag))
                {
                    cout << "Done8\n";
                    changeAllDiagonalNeighbours(nnDiag, nextNeighbour->neighboursDir[diagonalDir], p33, 2.0 * p33 - p32, p23);
                }
            }

            if (neighDiag &&
                    (!neighDir1 || (pattr1->neighbours[S]->neighbours[getNextDirCounterClockwise(pattr1->neighboursDir[S])] != neighDiag)) &&
                    (!neighDir2 || (pattr1->neighbours[E]->neighbours[getNextDirClockwise(pattr1->neighboursDir[E])] != neighDiag)))
            {
                Direction connDir = pattr1->neighboursDir[SE];
                changeAllDiagonalNeighbours(neighDiag, connDir, p33, p23, p32);
            }
        }
        return GL_TRUE;
    }

    GLboolean BiquarticCompositeSurface3::getDataPointValues(const GLuint &patchInd, const GLuint &dataPointRow, const GLuint &dataPointCol,
                                                             GLdouble &x, GLdouble &y, GLdouble &z)
    {
        if (patchInd >= _attributes.size())
            return GL_FALSE;

        DCoordinate3 p = (*_attributes[patchInd].patch)(dataPointRow, dataPointCol);
        x = p[0];
        y = p[1];
        z = p[2];
        return GL_TRUE;
    }

    Material* BiquarticCompositeSurface3::getMaterial(const GLuint &patchInd)
    {
        if (patchInd >= _attributes.size())
            return nullptr;
        return _attributes[patchInd].material;
    }

    GLboolean BiquarticCompositeSurface3::setMaterial(const GLuint &patchInd, Material *mat)
    {
        if (patchInd >= _attributes.size() || _attributes[patchInd].material == mat)
            return GL_FALSE;

        _attributes[patchInd].material = mat;
        return GL_TRUE;
    }

    GLboolean BiquarticCompositeSurface3::insertNewIsolatedPatch()
    {
        PatchAttributes newAttr;

        if (newAttr.patch)
        {

            cout << "New biquartic patch created" << endl;
            double a = (rand() % 2 == 0) ? 1.0 : -1.0;
            double x_min = a * (rand() % 9) + rand() / (double) RAND_MAX * (rand() % 9) * (-a);
            a = (rand() % 2 == 0) ? 1.0 : -1.0;
            double y_min = a * (rand() % 9) + rand() / (double) RAND_MAX * (rand() % 9) * (-a);
            double x = x_min;

            for (int  i = 0; i < 4; ++i)
            {
                for (int j = 0; j < 4; ++j)
                {
                    newAttr.patch->SetData(i, j, x, y_min + j, -1.0 + 2.0 * rand() / (double) RAND_MAX);
                }
                x += 1.0;
            }


            newAttr.img = newAttr.patch->GenerateImage(_u_div_point_count, _v_div_point_count);
        }

        _attributes.push_back(newAttr);
        _attributes[_attributes.size() - 1].patch->UpdateVertexBufferObjectsOfData();
        _attributes[_attributes.size() - 1].img->UpdateVertexBufferObjects();
        generateIsoLinesUpdateVBO(&_attributes[_attributes.size() - 1]);
        return GL_TRUE;
    }

    GLboolean BiquarticCompositeSurface3::mergeExistingPatches(const GLuint &patch_ind1, Direction dir1, const GLuint &patch_ind2, Direction dir2)
    {
        if (patch_ind1 == patch_ind2 || patch_ind1 >= _attributes.size() || patch_ind2 >= _attributes.size() ||
            _attributes[patch_ind1].neighbours[dir1] || _attributes[patch_ind2].neighbours[dir2])
        {
            cout << "NO MERGE\n";
            return GL_FALSE;
        }

        PatchAttributes *pattr1 = &_attributes[patch_ind1];
        PatchAttributes *pattr2 = &_attributes[patch_ind2];

        GLboolean cw = true, ccw = true, ok = false;

        switch(dir1)
        {
        case N:
        {
            DCoordinate3 &p00 = (*pattr1->patch)(0,0);
            DCoordinate3 &p01 = (*pattr1->patch)(0,1);
            DCoordinate3 &p02 = (*pattr1->patch)(0,2);
            DCoordinate3 &p03 = (*pattr1->patch)(0,3);

            DCoordinate3 pv0 = (*pattr1->patch)(1,0);
            DCoordinate3 pv1 = (*pattr1->patch)(1,1);
            DCoordinate3 pv2 = (*pattr1->patch)(1,2);
            DCoordinate3 pv3 = (*pattr1->patch)(1,3);

            ok = setMergingPoints(pattr1, pattr2, dir2, p00, p01, p02, p03, pv0, pv1, pv2, pv3, cw, ccw);

            if (ok)
            {
                if (pattr1->neighbours[W])
                {
                    if (cw)
                    {
                        changeAllNeighboursClockwise(pattr1, pattr2, pattr1->neighbours[W],
                                                          p00, (*pattr1->patch)(1, 0),
                                                          p01, (*pattr1->patch)(1, 1),
                                                          pattr1->neighboursDir[W]);
                    }
                }
                else if (pattr1->neighbours[NW] && pattr1->neighbours[NW] != pattr2->neighbours[getNextDirCounterClockwise(dir2)])
                {
                    changeAllDiagonalNeighbours(pattr1->neighbours[NW], pattr1->neighboursDir[NW], p00, pv0, p01);
                }

                if (pattr1->neighbours[E])
                {
                    if (ccw)
                    {
                        changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr1->neighbours[E],
                                                                 p03, (*pattr1->patch)(1, 3),
                                                                 p02, (*pattr1->patch)(1, 2),
                                                                 pattr1->neighboursDir[E]);
                    }
                }
                else if (pattr1->neighbours[NE] && pattr1->neighbours[NE] != pattr2->neighbours[getNextDirClockwise(dir2)])
                {
                    changeAllDiagonalNeighbours(pattr1->neighbours[NE], pattr1->neighboursDir[NE], p03, p02, pv3);
                }
            }
        } break;

        case S:
        {
            DCoordinate3 &p33 = (*pattr1->patch)(3,3);
            DCoordinate3 &p32 = (*pattr1->patch)(3,2);
            DCoordinate3 &p31 = (*pattr1->patch)(3,1);
            DCoordinate3 &p30 = (*pattr1->patch)(3,0);

            DCoordinate3 pv0 = (*pattr1->patch)(2,3);
            DCoordinate3 pv1 = (*pattr1->patch)(2,2);
            DCoordinate3 pv2 = (*pattr1->patch)(2,1);
            DCoordinate3 pv3 = (*pattr1->patch)(2,0);

            ok = setMergingPoints(pattr1, pattr2, dir2, p33, p32, p31, p30, pv0, pv1, pv2, pv3, cw, ccw);

            if (ok)
            {
                if (pattr1->neighbours[E])
                {
                    if (cw)
                    {
                        changeAllNeighboursClockwise(pattr1, pattr2, pattr1->neighbours[E],
                                                          p33, (*pattr1->patch)(2, 3),
                                                          p32, (*pattr1->patch)(2, 2),
                                                          pattr1->neighboursDir[E]);
                    }
                }
                else if (pattr1->neighbours[SE] && pattr1->neighbours[SE] != pattr2->neighbours[getNextDirCounterClockwise(dir2)])
                {
                    changeAllDiagonalNeighbours(pattr1->neighbours[SE], pattr1->neighboursDir[SE], p33, pv0, p32);
                }

                if (pattr1->neighbours[W])
                {
                    if (ccw)
                    {
                        changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr1->neighbours[W],
                                                                 p30, (*pattr1->patch)(2, 0),
                                                                 p31, (*pattr1->patch)(2, 1),
                                                                 pattr1->neighboursDir[W]);
                    }
                 }
                 else if (pattr1->neighbours[SW] && pattr1->neighbours[SW] != pattr2->neighbours[getNextDirClockwise(dir2)])
                 {
                    changeAllDiagonalNeighbours(pattr1->neighbours[SW], pattr1->neighboursDir[SW], p30, p31, pv3);
                 }
            }
        } break;

        case E:
        {
            DCoordinate3 &p03 = (*pattr1->patch)(0,3);
            DCoordinate3 &p13 = (*pattr1->patch)(1,3);
            DCoordinate3 &p23 = (*pattr1->patch)(2,3);
            DCoordinate3 &p33 = (*pattr1->patch)(3,3);

            DCoordinate3 pv0 = (*pattr1->patch)(0,2);
            DCoordinate3 pv1 = (*pattr1->patch)(1,2);
            DCoordinate3 pv2 = (*pattr1->patch)(2,2);
            DCoordinate3 pv3 = (*pattr1->patch)(3,2);

            ok = setMergingPoints(pattr1, pattr2, dir2, p03, p13, p23, p33, pv0, pv1, pv2, pv3, cw, ccw);

            if (ok)
            {
                if (pattr1->neighbours[N])
                {
                    if (cw)
                    {
                        changeAllNeighboursClockwise(pattr1, pattr2, pattr1->neighbours[N],
                                                          p03, (*pattr1->patch)(0, 2),
                                                          p13, (*pattr1->patch)(1, 2),
                                                          pattr1->neighboursDir[N]);
                    }
                }
                else if (pattr1->neighbours[NE] && pattr1->neighbours[NE] != pattr2->neighbours[getNextDirCounterClockwise(dir2)])
                {
                    changeAllDiagonalNeighbours(pattr1->neighbours[NE], pattr1->neighboursDir[NE], p03, pv0, p13);
                }

                if (pattr1->neighbours[S])
                {
                    if (ccw)
                    {
                        changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr1->neighbours[S],
                                                                 p33, (*pattr1->patch)(3, 2),
                                                                 p23, (*pattr1->patch)(2, 2),
                                                                 pattr1->neighboursDir[S]);
                    }
                }
                else if (pattr1->neighbours[SE] && pattr1->neighbours[SE] != pattr2->neighbours[getNextDirClockwise(dir2)])
                {
                   changeAllDiagonalNeighbours(pattr1->neighbours[SE], pattr1->neighboursDir[SE], p33, p23, pv3);
                }
            }
        } break;

        case W:
        {
            DCoordinate3 &p30 = (*pattr1->patch)(3,0);
            DCoordinate3 &p20 = (*pattr1->patch)(2,0);
            DCoordinate3 &p10 = (*pattr1->patch)(1,0);
            DCoordinate3 &p00 = (*pattr1->patch)(0,0);

            DCoordinate3 pv0 = (*pattr1->patch)(3,1);
            DCoordinate3 pv1 = (*pattr1->patch)(2,1);
            DCoordinate3 pv2 = (*pattr1->patch)(1,1);
            DCoordinate3 pv3 = (*pattr1->patch)(0,1);

            ok = setMergingPoints(pattr1, pattr2, dir2, p30, p20, p10, p00, pv0, pv1, pv2, pv3, cw, ccw);

            if (ok)
            {
                if (pattr1->neighbours[S])
                {
                    if (cw)
                    {
                        changeAllNeighboursClockwise(pattr1, pattr1, pattr1->neighbours[S],
                                                          p30, (*pattr1->patch)(3, 1),
                                                          p20, (*pattr1->patch)(2, 1),
                                                          pattr1->neighboursDir[S]);
                    }
                }
                else if (pattr1->neighbours[SW] && pattr1->neighbours[SW] != pattr2->neighbours[getNextDirCounterClockwise(dir2)])
                {
                    changeAllDiagonalNeighbours(pattr1->neighbours[SW], pattr1->neighboursDir[SW], p30, pv0, p20);
                }

                if (pattr1->neighbours[N])
                {
                    if (ccw)
                    {
                        changeAllNeighboursCounterClockwise(pattr1, pattr1, pattr1->neighbours[N],
                                                                 p00, (*pattr1->patch)(0, 1),
                                                                 p10, (*pattr1->patch)(1, 1),
                                                                 pattr1->neighboursDir[N]);
                    }
                }
                else if (pattr1->neighbours[NW] && pattr1->neighbours[NW] != pattr2->neighbours[getNextDirClockwise(dir2)])
                {
                   changeAllDiagonalNeighbours(pattr1->neighbours[NW], pattr1->neighboursDir[NW], p00, p10, pv3);
                }
            }
        } break;

        case NE:
        {
            DCoordinate3 &p03 = (*pattr1->patch)(0,3);
            DCoordinate3 &p02 = (*pattr1->patch)(0,2);
            DCoordinate3 &p13 = (*pattr1->patch)(1,3);

            ok = setMergingPointsDiagonal(pattr1, pattr2, dir2, p02, p03, p13, cw, ccw);

            if (ok)
            {
                if (pattr1->neighbours[E] && ccw)
                {
                    changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr1->neighbours[E],
                                                             p03, (*pattr1->patch)(1, 3),
                                                             p02, (*pattr1->patch)(1, 2),
                                                             pattr1->neighboursDir[E]);
                }

                if (pattr1->neighbours[N] && cw)
                {
                    changeAllNeighboursClockwise(pattr1, pattr2, pattr1->neighbours[N],
                                                      p03, (*pattr1->patch)(0, 2),
                                                      p13, (*pattr1->patch)(1, 2),
                                                      pattr1->neighboursDir[N]);
                }
            }
        } break;

        case NW:
        {
            DCoordinate3 &p00 = (*pattr1->patch)(0,0);
            DCoordinate3 &p10 = (*pattr1->patch)(1,0);
            DCoordinate3 &p01 = (*pattr1->patch)(0,1);

            ok = setMergingPointsDiagonal(pattr1, pattr2, dir2, p10, p00, p01, cw, ccw);

            if (ok)
            {
                if (pattr1->neighbours[W] && cw)
                {
                    changeAllNeighboursClockwise(pattr1, pattr2, pattr1->neighbours[W],
                                                      p00, (*pattr1->patch)(1, 0),
                                                      p01, (*pattr1->patch)(1, 1),
                                                      pattr1->neighboursDir[W]);
                }

                if (pattr1->neighbours[N] && ccw)
                {
                    changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr1->neighbours[N],
                                                             p00, (*pattr1->patch)(0, 1),
                                                             p10, (*pattr1->patch)(1, 1),
                                                             pattr1->neighboursDir[N]);
                }
            }
        } break;

        case SW:
        {
            DCoordinate3 &p30 = (*pattr1->patch)(3,0);
            DCoordinate3 &p31 = (*pattr1->patch)(3,1);
            DCoordinate3 &p20 = (*pattr1->patch)(2,0);

            ok = setMergingPointsDiagonal(pattr1, pattr2, dir2, p31, p30, p20, cw, ccw);

            if (ok)
            {
                if (pattr1->neighbours[S] && cw)
                {
                    changeAllNeighboursClockwise(pattr1, pattr2, pattr1->neighbours[S],
                                                      p30, (*pattr1->patch)(3, 1),
                                                      p20, (*pattr1->patch)(2, 1),
                                                      pattr1->neighboursDir[S]);
                }

                if (pattr1->neighbours[W] && ccw)
                {
                    changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr1->neighbours[W],
                                                             p30, (*pattr1->patch)(2, 0),
                                                             p31, (*pattr1->patch)(2, 1),
                                                             pattr1->neighboursDir[W]);
                }
            }
        } break;

        case SE:
        {
            DCoordinate3 &p33 = (*pattr1->patch)(3,3);
            DCoordinate3 &p23 = (*pattr1->patch)(2,3);
            DCoordinate3 &p32 = (*pattr1->patch)(3,2);

            ok = setMergingPointsDiagonal(pattr1, pattr2, dir2, p23, p33, p32, cw, ccw);

            if (ok)
            {
                if (pattr1->neighbours[S] && ccw)
                {
                    changeAllNeighboursCounterClockwise(pattr1, pattr2, pattr1->neighbours[S],
                                                             p33, (*pattr1->patch)(3, 2),
                                                             p23, (*pattr1->patch)(2, 2),
                                                             pattr1->neighboursDir[S]);
                }

                if (pattr1->neighbours[E] && cw)
                {
                    changeAllNeighboursClockwise(pattr1, pattr2, pattr1->neighbours[E],
                                                      p33, (*pattr1->patch)(2, 3),
                                                      p32, (*pattr1->patch)(2, 2),
                                                      pattr1->neighboursDir[E]);
                }
            }
         } break;

        default:
            break;
        }

        if (ok)
        {
            pattr1->neighbours[dir1] = pattr2;
            pattr2->neighbours[dir2] = pattr1;

            pattr1->neighboursDir[dir1] = dir2;
            pattr2->neighboursDir[dir2] = dir1;

            pattr1->patch->UpdateVertexBufferObjectsOfData();
            if (pattr1->img)
            {
                delete pattr1->img;
            }
            pattr1->img = pattr1->patch->GenerateImage(_u_div_point_count, _v_div_point_count);
            pattr1->img->UpdateVertexBufferObjects();

            generateIsoLinesUpdateVBO(pattr1);

            pattr2->patch->UpdateVertexBufferObjectsOfData();
            if (pattr2->img)
            {
                delete pattr2->img;
            }
            pattr2->img = pattr2->patch->GenerateImage(_u_div_point_count, _v_div_point_count);
            pattr2->img->UpdateVertexBufferObjects();

            generateIsoLinesUpdateVBO(pattr2);
            cout << "MERGE\n";
            return GL_TRUE;
        }

        cout << "NO MERGE\n";
        return GL_FALSE;
    }

    GLboolean BiquarticCompositeSurface3::joinExistingPatches(const GLuint &patch_ind1, Direction dir1, const GLuint &patch_ind2, Direction dir2)
    {
        cout << patch_ind1 << " " << dir1 << " JOIN " << dir2 << patch_ind2 << "\n";

        if (patch_ind1 >= _attributes.size() || patch_ind2 >= _attributes.size() || _attributes[patch_ind1].neighbours[dir1] || _attributes[patch_ind2].neighbours[dir2])
        {
            cout << "CAN'T JOIN, SORRY\n";
            return GL_FALSE;
        }

        PatchAttributes newAttr;

        GLuint newIndex = (GLuint) _attributes.size();

        _attributes.push_back(newAttr);

        if (dir1 == N)
        {
            if(dir2 == N)
            {
                join_NN(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == S)
            {
                join_NS(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == E)
            {
                join_NE(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == W)
            {
                join_NW(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == NE)
            {
                join_N_NE(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == NW)
            {
                join_N_NW(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == SE)
            {
                join_N_SE(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == SW)
            {
                join_N_SW(patch_ind2, patch_ind1, newIndex);
            }
        }
        else if(dir1 == S)
        {
            if(dir2 == N)
            {
                join_NS(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == S)
            {
                join_SS(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == E)
            {
                join_SE(patch_ind1,patch_ind2,newIndex);
            }
            else if(dir2 == W)
            {
                join_SW(patch_ind1,patch_ind2,newIndex);
            }
            else if(dir2 == NE)
            {
                join_S_NE(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == NW)
            {
                join_S_NW(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == SE)
            {
                join_S_SE(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == SW)
            {
                join_S_SW(patch_ind2, patch_ind1, newIndex);
            }
        }
        else if(dir1 == E)
        {
            if(dir2 == N)
            {
                join_NE(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == S)
            {
                join_SE(patch_ind2,patch_ind1,newIndex);
            }
            else if(dir2 == E)
            {
                join_EE(patch_ind1,patch_ind2,newIndex);
            }
            else if(dir2 == W)
            {
                join_EW(patch_ind1,patch_ind2,newIndex);
            }
            else if(dir2 == NE)
            {
                join_E_NE(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == NW)
            {
                join_E_NW(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == SE)
            {
                join_E_SE(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == SW)
            {
                join_E_SW(patch_ind2, patch_ind1, newIndex);
            }
        }
        else if(dir1 == W)
        {
            if(dir2 == N)
            {
                join_NW(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == S)
            {
                join_SW(patch_ind2,patch_ind1,newIndex);
            }
            else if(dir2 == E)
            {
                join_EW(patch_ind2,patch_ind1,newIndex);
            }
            else if(dir2 == W)
            {
                join_WW(patch_ind1,patch_ind2,newIndex);
            }
            else if(dir2 == NE)
            {
                join_W_NE(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == NW)
            {
                join_W_NW(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == SE)
            {
                join_W_SE(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == SW)
            {
                join_W_SW(patch_ind2, patch_ind1, newIndex);
            }
        }
        else if(dir1 == NE)
        {
            if(dir2 == N)
            {
                join_N_NE(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == S)
            {
                join_S_NE(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == E)
            {
                join_E_NE(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == W)
            {
                join_W_NE(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == NE)
            {
                join_NE_NE(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == NW)
            {
                join_NE_NW(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == SE)
            {
                join_NE_SE(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == SW)
            {
                join_NE_SW(patch_ind1, patch_ind2, newIndex);
            }
        }
        else if(dir1 == NW)
        {
            if(dir2 == N)
            {
                join_N_NW(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == S)
            {
                join_S_NW(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == E)
            {
                join_E_NW(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == W)
            {
                join_W_NW(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == NE)
            {
                join_NE_NW(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == NW)
            {
                join_NW_NW(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == SE)
            {
                join_NW_SE(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == SW)
            {
                join_NW_SW(patch_ind1, patch_ind2, newIndex);
            }
        }
        else if(dir1 == SE)
        {
            if(dir2 == N)
            {
                join_N_SE(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == S)
            {
                join_S_SE(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == E)
            {
                join_E_SE(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == W)
            {
                join_W_SE(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == NE)
            {
                join_NE_SE(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == NW)
            {
                join_NW_SE(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == SE)
            {
                join_SE_SE(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == SW)
            {
                join_SE_SW(patch_ind1, patch_ind2, newIndex);
            }
        }
        else if(dir1 == SW)
        {
            if(dir2 == N)
            {
                join_N_SW(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == S)
            {
                join_S_SW(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == E)
            {
                join_E_SW(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == W)
            {
                join_W_SW(patch_ind1, patch_ind2, newIndex);
            }
            else if(dir2 == NE)
            {
                join_NE_SW(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == NW)
            {
                join_NW_SW(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == SE)
            {
                join_SE_SW(patch_ind2, patch_ind1, newIndex);
            }
            else if(dir2 == SW)
            {
                join_SW_SW(patch_ind1, patch_ind2, newIndex);
            }
        }

        _attributes[newIndex].patch->UpdateVertexBufferObjectsOfData();
        if (_attributes[newIndex].img)
        {
            delete _attributes[newIndex].img;
        }
        _attributes[newIndex].img = _attributes[newIndex].patch->GenerateImage(_u_div_point_count, _v_div_point_count);
        _attributes[newIndex].img->UpdateVertexBufferObjects();

        generateIsoLinesUpdateVBO(&_attributes[newIndex]);

        return GL_TRUE;

    }

    GLvoid BiquarticCompositeSurface3::moveSelectedPatch(const GLuint &patchInd, Direction dir)
    {
        if (patchInd >= _attributes.size() || patchInd < 0)
            return;

        cout << "move\n";

        for (int i = 0; i < 8; ++i)
        {
            if (_attributes[patchInd].neighbours[i])
                return;
        }

        DCoordinate3 pointsHelp;

        switch(dir)
        {
        case N:
            for(GLuint i = 0; i < 4; i++)
            {
                for(GLuint j = 0; j < 4; j++)
                {
                    _attributes[patchInd].patch->GetData(i,j,pointsHelp);
                    pointsHelp.x() -= moveSize;
                    _attributes[patchInd].patch->SetData(i,j,pointsHelp);
                }
            }
            break;
        case W:
            for(GLuint i = 0; i < 4; i++)
            {
                for(GLuint j = 0; j < 4; j++)
                {
                    _attributes[patchInd].patch->GetData(i,j,pointsHelp);
                    pointsHelp.y() -= moveSize;
                    _attributes[patchInd].patch->SetData(i,j,pointsHelp);
                }
            }
            break;
        case S:
            for(GLuint i = 0; i < 4; i++)
            {
                for(GLuint j = 0; j < 4; j++)
                {
                    _attributes[patchInd].patch->GetData(i,j,pointsHelp);
                    pointsHelp.x() += moveSize;
                    _attributes[patchInd].patch->SetData(i,j,pointsHelp);
                }
            }
            break;
        case E:
            for(GLuint i = 0; i < 4; i++)
            {
                for(GLuint j = 0; j < 4; j++)
                {
                    _attributes[patchInd].patch->GetData(i,j,pointsHelp);
                    pointsHelp.y() += moveSize;
                    _attributes[patchInd].patch->SetData(i,j,pointsHelp);
                }
            }
            break;
        default:
            return;
        }

        _attributes[patchInd].patch->UpdateVertexBufferObjectsOfData();
        if (_attributes[patchInd].img)
        {
            delete _attributes[patchInd].img;
        }
        _attributes[patchInd].img = _attributes[patchInd].patch->GenerateImage(_u_div_point_count, _v_div_point_count);
        _attributes[patchInd].img->UpdateVertexBufferObjects();

        generateIsoLinesUpdateVBO(&_attributes[patchInd]);
    }


    GLboolean BiquarticCompositeSurface3::deletePatch(const GLuint &patchInd)
    {
        if (patchInd >= _attributes.size() || patchInd < 0)
            return GL_FALSE;

        PatchAttributes *lastAttr = &_attributes[_attributes.size() - 1];
        PatchAttributes *attrToDelete = &_attributes[patchInd];

        for (int i = 0; i < 8; ++i)
        {
            if (attrToDelete->neighbours[i])
            {
                attrToDelete->neighbours[i]->neighbours[attrToDelete->neighboursDir[i]] = nullptr;
            }
        }

        if (lastAttr != attrToDelete)
        {
            _attributes[patchInd] = (*lastAttr);

            if (_attributes[patchInd].patch)
                _attributes[patchInd].patch->UpdateVertexBufferObjectsOfData();
            if (_attributes[patchInd].img)
                _attributes[patchInd].img->UpdateVertexBufferObjects();
            generateIsoLinesUpdateVBO(&_attributes[patchInd]);

            for (int i = 0; i < 8; ++i)
            {
                if (lastAttr->neighbours[i])
                {
                     lastAttr->neighbours[i]->neighbours[lastAttr->neighboursDir[i]] = &_attributes[patchInd];
                }
            }
        }

        _attributes.resize(_attributes.size() - 1);

        return GL_TRUE;
    }


    GLboolean BiquarticCompositeSurface3::setShader(const int shaderInd)
    {
        if (shaderInd > 3 || shaderInd == shader_ind)
            return GL_FALSE;

        if (shaderInd < 0)
        {
            shader_ind = -1;
            return GL_TRUE;
        }

        shader_ind = shaderInd;
        return GL_TRUE;
    }

    int BiquarticCompositeSurface3::getShaderIndex()
    {
        return shader_ind + 1;
    }

    GLboolean BiquarticCompositeSurface3::loadSurface(const string &fname)
    {
        cout << "load\n";

        fstream in(fname.c_str(), ios_base::in);

        if (!in.is_open())
        {
            cout << "not open\n";
            return GL_FALSE;
        }

        GLuint n;
        in >> n;

        _attributes.resize(n);

        for (GLuint i = 0; i < n; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                for (int k = 0; k < 4; ++k)
                {
                    in >> (*_attributes[i].patch)(j,k);
                }
            }

            int ind;
            in >> ind;

            _attributes[i].material = intToMaterial(ind);

            for (int j = 0; j < 8; ++j)
            {
                in >> ind;
                if (ind == -1)
                    _attributes[i].neighbours[j] = nullptr;
                else
                    _attributes[i].neighbours[j] = &_attributes[ind];
            }

            for (int j = 0; j < 8; ++j)
            {
                in >> ind;
                _attributes[i].neighboursDir[j] = intToDir(ind);
            }

            _attributes[i].patch->UpdateVertexBufferObjectsOfData();
            if (_attributes[i].img)
                delete _attributes[i].img;
            _attributes[i].img = _attributes[i].patch->GenerateImage(_u_div_point_count, _v_div_point_count);
            _attributes[i].img->UpdateVertexBufferObjects();
            generateIsoLinesUpdateVBO(&_attributes[i]);
        }

        in.close();

        return GL_TRUE;
    }

    GLboolean BiquarticCompositeSurface3::saveSurface(const string& fname)
    {
        cout << "save\n";

        if (_attributes.size() == 0)
        {
            cout << "No patches!\n";
            return GL_FALSE;
        }

        fstream out(fname.c_str(), ios_base::out);

        if (!out.is_open())
            return GL_FALSE;

        // nr of patches
        out << _attributes.size() << "\n\n";

        for (vector<PatchAttributes>::iterator it = _attributes.begin(); it != _attributes.end(); ++it)
        {
            // data points
            for (GLuint i = 0; i < 4; ++i)
            {
                for (GLuint j = 0; j < 4; ++j)
                {
                    out << (*it->patch)(i,j) << " ";
                }
                out << "\n";
            }

            // material
            out << materialToInt(it->material) << "\n";

            // neighbours
            for (int i = 0; i < 8; ++i)
            {
                if (it->neighbours[i])
                {
                    for (GLuint j = 0; j < _attributes.size(); ++j)
                    {
                        if (&_attributes[j] == it->neighbours[i])
                        {
                            out << j << " ";
                            break;
                        }
                    }
                }
                else
                {
                    out << -1 << " ";
                }
            }
            out << "\n";

            // neighbours' directions
            for (int i = 0; i < 8; ++i)
            {
                out << it->neighboursDir[i] << " ";
            }
            out << "\n\n";
        }

        out.close();

        return GL_TRUE;
    }


    GLboolean BiquarticCompositeSurface3::continueExistingPatchPreview(const GLuint &index, Direction dir)
    {
        cout << "CONTIUNE " << index << " " << dir << "\n";

        if (index >= _attributes.size() || _attributes[index].neighbours[dir]) {
            cout << "CAN'T CONTINUE\n\n";
            return GL_FALSE;
        }

        _op.firstDir = dir;
        _op.firstInd = index;
        _op.newAttr = new PatchAttributes();

        switch (dir)
        {
        case N:
            continue_N();
            break;
        case S:
            continue_S();
            break;
        case E:
            continue_E();
            break;
        case W:
            continue_W();
            break;
        case NE:
            continue_NE();
            break;
        case SE:
            continue_SE();
            break;
        case SW:
            continue_SW();
            break;
        case NW:
            continue_NW();
            break;
        default:
            break;
        }

        _op.newAttr->img = _op.newAttr->patch->GenerateImage(_u_div_point_count, _v_div_point_count);
        _op.newAttr->patch->UpdateVertexBufferObjectsOfData();
        _op.newAttr->img->UpdateVertexBufferObjects();
        generateIsoLinesUpdateVBO(_op.newAttr);
        cout << "CONTINUE SUCCESSFUL" << "\n";
        return GL_TRUE;
    }

    GLvoid BiquarticCompositeSurface3::continueExistingPatch()
    {
        GLuint newIndex = (GLuint) _attributes.size();
        _attributes.push_back(*_op.newAttr);

        PatchAttributes *pattr = &_attributes[_op.firstInd];
        PatchAttributes *newAttr = &_attributes[newIndex];

        newAttr->neighbours[_op.secondDir] = pattr;
        newAttr->neighboursDir[_op.secondDir] = _op.firstDir;

        pattr->neighbours[_op.firstDir] = newAttr;
        pattr->neighboursDir[_op.firstDir] = _op.secondDir;

        newAttr->img = newAttr->patch->GenerateImage(_u_div_point_count, _v_div_point_count);
        newAttr->patch->UpdateVertexBufferObjectsOfData();
        newAttr->img->UpdateVertexBufferObjects();
        generateIsoLinesUpdateVBO(newAttr);

        delete _op.newAttr, _op.newAttr = nullptr;
        cout << "CONTINUE SUCCESSFUL" << "\n";
    }

    GLboolean BiquarticCompositeSurface3::setIsoLineCountU(const int& count)
    {
        if (count <= 0 || _iso_line_count_u == (GLuint) count)
            return GL_FALSE;
        _iso_line_count_u = count;
        for (GLuint i = 0; i < _attributes.size(); ++i)
        {
            generateIsoLinesUpdateVBO(&_attributes[i]);
        }
        return GL_TRUE;
    }

    GLboolean BiquarticCompositeSurface3::setIsoLineCountV(const int& count)
    {
        if (count <= 0 || _iso_line_count_v == (GLuint) count)
            return GL_FALSE;
        _iso_line_count_v = count;
        for (GLuint i = 0; i < _attributes.size(); ++i)
        {
            generateIsoLinesUpdateVBO(&_attributes[i]);
        }
        return GL_TRUE;
    }

    bool BiquarticCompositeSurface3::set_def_toon_color_r(GLfloat value)
    {
        if (_toon_def_color[0] != value)
        {
            _toon_def_color[0] = value;
            return true;
        }
        return false;
    }

    bool BiquarticCompositeSurface3::set_def_toon_color_g(GLfloat value)
    {
        if (_toon_def_color[1] != value)
        {
            _toon_def_color[1] = value;
            return true;
        }
        return false;
    }

    bool BiquarticCompositeSurface3::set_def_toon_color_b(GLfloat value)
    {
        if (_toon_def_color[2] != value)
        {
            _toon_def_color[2] = value;
            return true;
        }
        return false;
    }

    bool BiquarticCompositeSurface3::set_def_toon_color_a(GLfloat value)
    {
        if (_toon_def_color[3] != value)
        {
            _toon_def_color[3] = value;
            return true;
        }
        return false;
    }

    bool BiquarticCompositeSurface3::set_rl_scale(GLfloat value)
    {
        if (_reflection_lines_unif_params[0] != value)
        {
            _reflection_lines_unif_params[0] = value;
            return true;
        }
        return false;
    }

    bool BiquarticCompositeSurface3::set_rl_smooth(GLfloat value)
    {
        if (_reflection_lines_unif_params[1] != value)
        {
            _reflection_lines_unif_params[1] = value;
            return true;
        }
        return false;
    }

    bool BiquarticCompositeSurface3::set_rl_shade(GLfloat value)
    {
        if (_reflection_lines_unif_params[2] != value)
        {
            _reflection_lines_unif_params[2] = value;
            return true;
        }
        return false;
    }

    bool BiquarticCompositeSurface3::set_alpha_checked(int ch)
    {
        if (ch != _alpha_on)
        {
            _alpha_on = ch;
            return true;
        }
        return false;
    }

    GLboolean BiquarticCompositeSurface3::getPointInfo( GLdouble &x,  GLdouble &y,  GLdouble &z, double zoom, int &patchInd, int &row, int &col, int rotx, int roty, int rotz, double trax, double tray, double traz)
    {
        GLuint len = (GLuint) _attributes.size();
        GLdouble px, py, pz;
        GLdouble error = POINT_ERR / zoom;
        QMatrix4x4 m;

        m.rotate(-rotx, 1.0, 0.0, 0.0);
        m.rotate(-roty, 0.0, 1.0, 0.0);
        m.rotate(-rotz, 0.0, 0.0, 1.0);

        for (GLuint k = 0; k < len; ++k)
        {
            for (int i = 0; i < 4; ++i)
            {
                for (int j = 0; j < 4; ++j)
                {
                    DCoordinate3 d = _attributes[k].patch->operator()(i,j);
                    QVector3D h1(d.x() * zoom + trax, d.y() * zoom + tray, d.z() * zoom + traz);

                    // cout << "before rotation: " << d.x() <<" "<< d.y()<<" "<< d.z()<<endl;
                    h1 = h1*m;

                    px = h1.x();
                    py = h1.y();
                    pz = h1.z();

                    // cout << "after rotation: " << px <<" "<< py <<" "<< pz <<endl;

                    if (abs(px - x) < error && abs(py - y) < error && abs(pz - z) < error)
                    {
                        patchInd = k;
                        row = i;
                        col = j;
                        return GL_TRUE;
                    }
                }
            }
        }


        return GL_FALSE;
    }

    GLvoid BiquarticCompositeSurface3::cancelOperation(const int &last_op)
    {
        switch (last_op)
        {
        // continue
        case 0:
            delete _op.newAttr, _op.newAttr = nullptr;
            return;
        default:
            return;
        }
    }
}
