#include "BiquarticCompositeSurfaces3.h"

namespace cagd
{
    BiquarticCompositeSurface3::PatchAttributes::PatchAttributes()
    {
        patch = new BiquarticPatch3();
        material = &MatFBBrass;
        img = nullptr;
        u_iso_lines = nullptr;
        v_iso_lines = nullptr;
        for (int i = 0; i < 8; ++i)
        {
            neighbours[i] = nullptr;
            neighboursDir[i] = NoDir;
        }
    }

    BiquarticCompositeSurface3::PatchAttributes::PatchAttributes(const PatchAttributes& pa)
    {
        if (pa.patch)
        {
            this->patch = new BiquarticPatch3(*(pa.patch));
            this->u_iso_lines = this->patch->GenerateUIsoparametricLines(12, 1, 30);
            this->v_iso_lines = this->patch->GenerateVIsoparametricLines(12, 1, 30);

            if (u_iso_lines)
            {
                for (GLuint i = 0; i < (*u_iso_lines).GetColumnCount(); ++i)
                {
                    if ((*u_iso_lines)[i])
                    {
                        if (!(*u_iso_lines)[i]->UpdateVertexBufferObjects(0.2))
                        {
                            cout << "Could not update isoline's vbo\n";
                        }
                    }
                }
            }

            if (v_iso_lines)
            {
                for (GLuint i = 0; i < (*v_iso_lines).GetColumnCount(); ++i)
                {
                    if ((*v_iso_lines)[i])
                    {
                        if (!(*v_iso_lines)[i]->UpdateVertexBufferObjects(0.2))
                        {
                            cout << "Could not update isoline's vbo\n";
                        }
                    }
                }
            }
        }
        else
        {
            this->patch = nullptr;
            this->u_iso_lines = nullptr;
            this->v_iso_lines = nullptr;
        }

        if (pa.img)
            this->img = new TriangulatedMesh3(*(pa.img));
        else
            img = nullptr;

        if (pa.material)
            this->material = pa.material;
        else
            material = &MatFBBrass;

        for (int i = 0; i < 8; ++i)
        {
            this->neighbours[i] = pa.neighbours[i];
            this->neighboursDir[i] = pa.neighboursDir[i];
        }
    }

    BiquarticCompositeSurface3::PatchAttributes& BiquarticCompositeSurface3::PatchAttributes::operator=(const PatchAttributes& pa)
    {
        if (pa.patch)
        {
            this->patch = new BiquarticPatch3(*(pa.patch));
            this->u_iso_lines = this->patch->GenerateUIsoparametricLines(12, 1, 30);
            this->v_iso_lines = this->patch->GenerateVIsoparametricLines(12, 1, 30);
            if (u_iso_lines)
            {
                for (GLuint i = 0; i < (*u_iso_lines).GetColumnCount(); ++i)
                {
                    if ((*u_iso_lines)[i])
                    {
                        if (!(*u_iso_lines)[i]->UpdateVertexBufferObjects(0.2))
                        {
                            cout << "Could not update isoline's vbo\n";
                        }
                    }
                }
            }

            if (v_iso_lines)
            {
                for (GLuint i = 0; i < (*v_iso_lines).GetColumnCount(); ++i)
                {
                    if ((*v_iso_lines)[i])
                    {
                        if (!(*v_iso_lines)[i]->UpdateVertexBufferObjects(0.2))
                        {
                            cout << "Could not update isoline's vbo\n";
                        }
                    }
                }
            }
        }
        else
        {
            patch = nullptr;
            this->u_iso_lines = nullptr;
            this->v_iso_lines = nullptr;
        }

        if (pa.img)
            this->img = new TriangulatedMesh3(*(pa.img));
        else
            img = nullptr;

        if (pa.material)
            this->material = pa.material;
        else
            material = &MatFBBrass;

        for (int i = 0; i < 8; ++i)
        {
            this->neighbours[i] = pa.neighbours[i];
            this->neighboursDir[i] = pa.neighboursDir[i];
        }

        return *this;
    }

    BiquarticCompositeSurface3::PatchAttributes::~PatchAttributes()
    {
        if (patch)
            delete patch, patch = nullptr;
        if (img)
            delete img, img = nullptr;
        if (u_iso_lines)
            delete u_iso_lines, u_iso_lines = nullptr;
        if (v_iso_lines)
            delete v_iso_lines, v_iso_lines = nullptr;
    }
}
