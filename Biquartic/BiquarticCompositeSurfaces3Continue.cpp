#include "BiquarticCompositeSurfaces3.h"

namespace cagd
{
    GLvoid BiquarticCompositeSurface3::continue_N()
    {
        for (GLuint i = 0; i < 4; i++)
        {
            _op.newAttr->patch->SetData(3, i, (*_attributes[_op.firstInd].patch)(0,i));
            _op.newAttr->patch->SetData(2, i, 2 * (*_op.newAttr->patch)(3,i) - (*_attributes[_op.firstInd].patch)(1,i));
            _op.newAttr->patch->SetData(1, i, 2 * (*_op.newAttr->patch)(2,i) - (*_op.newAttr->patch)(3,i));
            _op.newAttr->patch->SetData(0, i, 2 * (*_op.newAttr->patch)(1,i) - (*_op.newAttr->patch)(2,i));
        }

        _op.secondDir = S;
    }

    GLvoid BiquarticCompositeSurface3::continue_S()
    {
        for (GLuint i = 0; i < 4; i++)
        {
            _op.newAttr->patch->SetData(0, i, (*_attributes[_op.firstInd].patch)(3,i));
            _op.newAttr->patch->SetData(1, i, 2 * (*_op.newAttr->patch)(0,i) - (*_attributes[_op.firstInd].patch)(2,i));
            _op.newAttr->patch->SetData(2, i, 2 * (*_op.newAttr->patch)(1,i) - (*_op.newAttr->patch)(0,i));
            _op.newAttr->patch->SetData(3, i, 2 * (*_op.newAttr->patch)(2,i) - (*_op.newAttr->patch)(1,i));
        }

        _op.secondDir = N;
    }

    GLvoid BiquarticCompositeSurface3::continue_W()
    {
        for (GLuint i = 0; i < 4; i++)
        {
            _op.newAttr->patch->SetData(i, 3, (*_attributes[_op.firstInd].patch)(i,0));
            _op.newAttr->patch->SetData(i, 2, 2 * (*_op.newAttr->patch)(i,3) - (*_attributes[_op.firstInd].patch)(i,1));
            _op.newAttr->patch->SetData(i, 1, 2 * (*_op.newAttr->patch)(i,2) - (*_op.newAttr->patch)(i,3));
            _op.newAttr->patch->SetData(i, 0, 2 * (*_op.newAttr->patch)(i,1) - (*_op.newAttr->patch)(i,2));
        }

        _op.secondDir = E;
    }

    GLvoid BiquarticCompositeSurface3::continue_E()
    {
        for (GLuint i = 0; i < 4; i++)
        {
            _op.newAttr->patch->SetData(i, 0, (*_attributes[_op.firstInd].patch)(i,3));
            _op.newAttr->patch->SetData(i, 1, 2 * (*_op.newAttr->patch)(i,0) - (*_attributes[_op.firstInd].patch)(i,2));
            _op.newAttr->patch->SetData(i, 2, 2 * (*_op.newAttr->patch)(i,1) - (*_op.newAttr->patch)(i,0));
            _op.newAttr->patch->SetData(i, 3, 2 * (*_op.newAttr->patch)(i,2) - (*_op.newAttr->patch)(i,1));
        }

        _op.secondDir = W;
    }

    GLvoid BiquarticCompositeSurface3::continue_NE()
    {
        for (GLuint i = 0; i < 4; i++)
        {
            // N

            DCoordinate3 temp1[4];
            BiquarticPatch3 patch1;

            temp1[0] = (*_attributes[_op.firstInd].patch)(1,3);
            temp1[1] = 2 * (temp1[0]) - (*_attributes[_op.firstInd].patch)(1,2);
            temp1[2] = 2 * (temp1[1]) - (temp1[0]);
            temp1[3] = 2 * (temp1[2]) - (temp1[1]);

            patch1.SetData(3, 0, (*_attributes[_op.firstInd].patch)(0,3));
            patch1.SetData(3, 1, 2 * (patch1)(3,0) - (*_attributes[_op.firstInd].patch)(0,2));
            patch1.SetData(3, 2, 2 * (patch1)(3,1) - (patch1)(3,0));
            patch1.SetData(3, 3, 2 * (patch1)(3,2) - (patch1)(3,1));

            for (GLuint i = 0; i < 4; i++)
            {
                patch1.SetData(2, i, 2 * (patch1)(3,i) - (temp1[i]));
                patch1.SetData(1, i, 2 * (patch1)(2,i) - (patch1)(3,i));
                patch1.SetData(0, i, 2 * (patch1)(1,i) - (patch1)(2,i));
            }

            // E

            DCoordinate3 temp2[4];
            BiquarticPatch3 patch2;

            temp2[3] = (*_attributes[_op.firstInd].patch)(0,2);
            temp2[2] = 2 * (temp2[3]) - (*_attributes[_op.firstInd].patch)(1,2);
            temp2[1] = 2 * (temp2[2]) - (temp2[3]);
            temp2[0] = 2 * (temp2[1]) - (temp2[2]);

            patch2.SetData(3, 0, (*_attributes[_op.firstInd].patch)(0,3));
            patch2.SetData(2, 0, 2 * (patch2)(3,0) - (*_attributes[_op.firstInd].patch)(1,3));
            patch2.SetData(1, 0, 2 * (patch2)(2,0) - (patch2)(3,0));
            patch2.SetData(0, 0, 2 * (patch2)(1,0) - (patch2)(2,0));

            for (GLuint i = 0; i < 4; i++)
            {
                patch2.SetData(i, 1, 2 * (patch2)(i,0) - (temp2[i]));
                patch2.SetData(i, 2, 2 * (patch2)(i,1) - (patch2)(i,0));
                patch2.SetData(i, 3, 2 * (patch2)(i,2) - (patch2)(i,1));
            }

            // calculating average

            for (GLuint i = 0; i < 4; i++)
            {
                for (GLuint j = 0; j < 4; j++)
                {
                     _op.newAttr->patch->SetData(i, j, ((patch1)(i,j) + (patch2)(i,j)) / 2);
                }
            }
        }

        _op.secondDir = SW;
    }

    GLvoid BiquarticCompositeSurface3::continue_SE()
    {
        // S

        DCoordinate3 temp1[4];
        BiquarticPatch3 patch1;

        temp1[0] = (*_attributes[_op.firstInd].patch)(2,3);
        temp1[1] = 2 * (temp1[0]) - (*_attributes[_op.firstInd].patch)(2,2);
        temp1[2] = 2 * (temp1[1]) - (temp1[0]);
        temp1[3] = 2 * (temp1[2]) - (temp1[1]);

        patch1.SetData(0, 0, (*_attributes[_op.firstInd].patch)(3,3));
        patch1.SetData(0, 1, 2 * (patch1)(0,0) - (*_attributes[_op.firstInd].patch)(3,2));
        patch1.SetData(0, 2, 2 * (patch1)(0,1) - (patch1)(0,0));
        patch1.SetData(0, 3, 2 * (patch1)(0,2) - (patch1)(0,1));

        for (GLuint i = 0; i < 4; i++)
        {
            patch1.SetData(1, i, 2 * (patch1)(0,i) - (temp1[i]));
            patch1.SetData(2, i, 2 * (patch1)(1,i) - (patch1)(0,i));
            patch1.SetData(3, i, 2 * (patch1)(2,i) - (patch1)(1,i));
        }

        // E

        DCoordinate3 temp2[4];
        BiquarticPatch3 patch2;

        temp2[0] = (*_attributes[_op.firstInd].patch)(3,2);
        temp2[1] = 2 * (temp2[0]) - (*_attributes[_op.firstInd].patch)(2,2);
        temp2[2] = 2 * (temp2[1]) - (temp2[0]);
        temp2[3] = 2 * (temp2[2]) - (temp2[1]);

        patch2.SetData(0, 0, (*_attributes[_op.firstInd].patch)(3,3));
        patch2.SetData(1, 0, 2 * (patch2)(0,0) - (*_attributes[_op.firstInd].patch)(2,3));
        patch2.SetData(2, 0, 2 * (patch2)(1,0) - (patch2)(0,0));
        patch2.SetData(3, 0, 2 * (patch2)(2,0) - (patch2)(1,0));

        for (GLuint i = 0; i < 4; i++)
        {
            patch2.SetData(i, 1, 2 * (patch2)(i,0) - (temp2[i]));
            patch2.SetData(i, 2, 2 * (patch2)(i,1) - (patch2)(i,0));
            patch2.SetData(i, 3, 2 * (patch2)(i,2) - (patch2)(i,1));
        }

        // calculating average

        for (GLuint i = 0; i < 4; i++)
        {
            for (GLuint j = 0; j < 4; j++)
            {
                 _op.newAttr->patch->SetData(i, j, ((patch1)(i,j) + (patch2)(i,j)) / 2);
            }
        }

        _op.secondDir = NW;
    }

    GLvoid BiquarticCompositeSurface3::continue_SW()
    {
        // S

        DCoordinate3 temp1[4];
        BiquarticPatch3 patch1;

        temp1[3] = (*_attributes[_op.firstInd].patch)(2,0);
        temp1[2] = 2 * (temp1[3]) - (*_attributes[_op.firstInd].patch)(2,1);
        temp1[1] = 2 * (temp1[2]) - (temp1[3]);
        temp1[0] = 2 * (temp1[1]) - (temp1[2]);

        patch1.SetData(0, 3, (*_attributes[_op.firstInd].patch)(3,0));
        patch1.SetData(0, 2, 2 * (patch1)(0,3) - (*_attributes[_op.firstInd].patch)(3,1));
        patch1.SetData(0, 1, 2 * (patch1)(0,2) - (patch1)(0,3));
        patch1.SetData(0, 0, 2 * (patch1)(0,1) - (patch1)(0,2));

        for (GLuint i = 0; i < 4; i++)
        {
            patch1.SetData(1, i, 2 * (patch1)(0,i) - (temp1[i]));
            patch1.SetData(2, i, 2 * (patch1)(1,i) - (patch1)(0,i));
            patch1.SetData(3, i, 2 * (patch1)(2,i) - (patch1)(1,i));
        }

        // W

        DCoordinate3 temp2[4];
        BiquarticPatch3 patch2;

        temp2[0] = (*_attributes[_op.firstInd].patch)(3,1);
        temp2[1] = 2 * (temp2[0]) - (*_attributes[_op.firstInd].patch)(2,1);
        temp2[2] = 2 * (temp2[1]) - (temp2[0]);
        temp2[3] = 2 * (temp2[2]) - (temp2[1]);

        patch2.SetData(0, 3, (*_attributes[_op.firstInd].patch)(3,0));
        patch2.SetData(1, 3, 2 * (patch2)(0,3) - (*_attributes[_op.firstInd].patch)(2,0));
        patch2.SetData(2, 3, 2 * (patch2)(1,3) - (patch2)(0,3));
        patch2.SetData(3, 3, 2 * (patch2)(2,3) - (patch2)(1,3));

        for (GLuint i = 0; i < 4; i++)
        {
            patch2.SetData(i, 2, 2 * (patch2)(i,3) - (temp2[i]));
            patch2.SetData(i, 1, 2 * (patch2)(i,2) - (patch2)(i,3));
            patch2.SetData(i, 0, 2 * (patch2)(i,1) - (patch2)(i,2));
        }

        // calculating average

        for (GLuint i = 0; i < 4; i++)
        {
            for (GLuint j = 0; j < 4; j++)
            {
                 _op.newAttr->patch->SetData(i, j, ((patch1)(i,j) + (patch2)(i,j)) / 2);
            }
        }

        _op.secondDir = NE;
    }

    GLvoid BiquarticCompositeSurface3::continue_NW()
    {
        // N

        DCoordinate3 temp1[4];
        BiquarticPatch3 patch1;

        temp1[3] = (*_attributes[_op.firstInd].patch)(1,0);
        temp1[2] = 2 * (temp1[3]) - (*_attributes[_op.firstInd].patch)(1,1);
        temp1[1] = 2 * (temp1[2]) - (temp1[3]);
        temp1[0] = 2 * (temp1[1]) - (temp1[2]);

        patch1.SetData(3, 3, (*_attributes[_op.firstInd].patch)(0,0));
        patch1.SetData(3, 2, 2 * (patch1)(3,3) - (*_attributes[_op.firstInd].patch)(0,1));
        patch1.SetData(3, 1, 2 * (patch1)(3,2) - (patch1)(3,3));
        patch1.SetData(3, 0, 2 * (patch1)(3,1) - (patch1)(3,2));

        for (GLuint i = 0; i < 4; i++)
        {
            patch1.SetData(2, i, 2 * (patch1)(3,i) - (temp1[i]));
            patch1.SetData(1, i, 2 * (patch1)(2,i) - (patch1)(3,i));
            patch1.SetData(0, i, 2 * (patch1)(1,i) - (patch1)(2,i));
        }

        // W

        DCoordinate3 temp2[4];
        BiquarticPatch3 patch2;

        temp2[3] = (*_attributes[_op.firstInd].patch)(0,1);
        temp2[2] = 2 * (temp2[3]) - (*_attributes[_op.firstInd].patch)(1,1);
        temp2[1] = 2 * (temp2[2]) - (temp2[3]);
        temp2[0] = 2 * (temp2[1]) - (temp2[2]);

        patch2.SetData(3, 3, (*_attributes[_op.firstInd].patch)(0,0));
        patch2.SetData(2, 3, 2 * (patch2)(3,3) - (*_attributes[_op.firstInd].patch)(1,0));
        patch2.SetData(1, 3, 2 * (patch2)(2,3) - (patch2)(3,3));
        patch2.SetData(0, 3, 2 * (patch2)(1,3) - (patch2)(2,3));

        for (GLuint i = 0; i < 4; i++)
        {
            patch2.SetData(i, 2, 2 * (patch2)(i,3) - (temp2[i]));
            patch2.SetData(i, 1, 2 * (patch2)(i,2) - (patch2)(i,3));
            patch2.SetData(i, 0, 2 * (patch2)(i,1) - (patch2)(i,2));
        }

        // calculating average
        for (GLuint i = 0; i < 4; i++)
        {
            for (GLuint j = 0; j < 4; j++)
            {
                _op.newAttr->patch->SetData(i, j, ((patch1)(i,j) + (patch2)(i,j)) / 2);
            }
        }

        _op.secondDir = SE;
    }
}
