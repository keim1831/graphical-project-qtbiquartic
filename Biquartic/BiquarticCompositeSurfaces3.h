# pragma once

#include "BiquarticPatch3.h"
#include <Core/TriangulatedMeshes3.h>
#include <Core/Materials.h>
#include <Core/ShaderPrograms.h>

using namespace std;

namespace cagd
{
    class BiquarticCompositeSurface3
    {
    public:
        enum Direction {N, NW, W, SW, S, SE, E, NE, NoDir};

        class PatchAttributes
        {
        public:
            BiquarticPatch3     *patch;
            TriangulatedMesh3   *img;
            Material            *material;
            PatchAttributes     *neighbours[8];
            Direction           neighboursDir[8];
            RowMatrix<GenericCurve3*> *u_iso_lines;
            RowMatrix<GenericCurve3*> *v_iso_lines;

            PatchAttributes();
            PatchAttributes(const PatchAttributes&);
            PatchAttributes& operator=(const PatchAttributes&);
            ~PatchAttributes();
        };

        struct OperationPreview
        {
            PatchAttributes* newAttr = nullptr, *attr1 = nullptr, *attr2 = nullptr;
            Direction firstDir = NoDir, secondDir = NoDir;
            int firstInd = -1, secondInd = -1;
        };

    protected:
        std::vector<PatchAttributes> _attributes;
        ShaderProgram *_shaders;
        GLfloat _reflection_lines_unif_params[3] = {5.0f, 2.0f, 1.0f};
        GLfloat _toon_def_color[4] = {0.0f, 0.0f, 0.0f, 0.5f};
        int _alpha_on = 0;

        GLuint _u_div_point_count = 40;
        GLuint _v_div_point_count = 40;
        GLuint _iso_line_count_u = 6;
        GLuint _iso_line_count_v = 6;
        GLdouble moveSize = 0.1;
        int shader_ind = -1;
        int _tex_ind = 0;

        OperationPreview _op;

        //join
        GLvoid join_NN(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_SS(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_EE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_WW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);

        GLvoid join_NS(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_NE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);

        GLvoid join_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_EW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);

        GLvoid join_N_NE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_N_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_N_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_N_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);

        GLvoid join_S_NE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_S_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_S_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_S_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);

        GLvoid join_E_NE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_E_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_E_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_E_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);

        GLvoid join_W_NE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_W_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_W_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_W_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);

        GLvoid join_NE_NE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_NE_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_NE_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_NE_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);

        GLvoid join_NW_NW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_NW_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_NW_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);

        GLvoid join_SE_SE(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);
        GLvoid join_SE_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);

        GLvoid join_SW_SW(const GLuint &patch_ind1, const GLuint &patch_ind2, GLuint newIndex);

        GLvoid continue_N();
        GLvoid continue_S();
        GLvoid continue_W();
        GLvoid continue_E();
        GLvoid continue_NE();
        GLvoid continue_SE();
        GLvoid continue_SW();
        GLvoid continue_NW();

        GLvoid generateIsoLinesUpdateVBO(PatchAttributes* attr);
        Direction getNextDirClockwise(Direction dir);
        PatchAttributes* changeAllNeighboursClockwise(PatchAttributes* pattr1, PatchAttributes* pattr2, PatchAttributes *nextNeighbour, DCoordinate3 commonPoint0, DCoordinate3 commonPoint1,
                                            DCoordinate3 otherPoint0, DCoordinate3 otherPoint1, Direction neighbourDir);
        PatchAttributes* changeAllNeighboursCounterClockwise(PatchAttributes* pattr1, PatchAttributes* pattr2, PatchAttributes *nextNeighbour, DCoordinate3 commonPoint0, DCoordinate3 commonPoint1,
                                            DCoordinate3 otherPoint0, DCoordinate3 otherPoint1, Direction neighbourDir);
        Direction getNextDirCounterClockwise(Direction dir);
        Direction intToDir(const int& i);
        int materialToInt(const Material* mat);
        Material* intToMaterial(const int& i);

        Direction getNextDirClockwise8(Direction dir);
        Direction getNextDirCounterClockwise8(Direction dir);
        GLvoid changeAllDiagonalNeighbours(PatchAttributes *attr, Direction connDir,
                                           const DCoordinate3 &pCommon, const DCoordinate3 &pvCW, const DCoordinate3 &pvCCW);
        GLboolean setMergingPoints(PatchAttributes *attr1, PatchAttributes *attr2, Direction dir2,
                                DCoordinate3& p0, DCoordinate3& p1, DCoordinate3& p2, DCoordinate3& p3,
                                const DCoordinate3 &pv0, const DCoordinate3 &pv1, const DCoordinate3 &pv2, const DCoordinate3 &pv3,
                                GLboolean &cw, GLboolean &ccw);
        GLboolean setMergingPointsDiagonal(PatchAttributes *attr1, PatchAttributes *attr2, Direction dir2,
                                DCoordinate3& pm0, DCoordinate3& pm1, DCoordinate3& pm2,
                                GLboolean &cw, GLboolean &ccw);

    public:
        BiquarticCompositeSurface3(ShaderProgram shaders[], const size_t min_patch_count = 1000);
        ~BiquarticCompositeSurface3();
        GLboolean insertNewIsolatedPatch();
        GLboolean continueExistingPatchPreview(const GLuint &index, Direction dir);
        GLvoid continueExistingPatch();
        GLboolean joinExistingPatches(const GLuint &patch_ind1, Direction dir1,
                                   const GLuint &patch_ind2, Direction dir2);
        GLboolean mergeExistingPatches(const GLuint &patch_ind1, Direction dir1,
                                    const GLuint &patch_ind2, Direction dir2);
        GLvoid renderAllPatches(int selected, int render_data, int render_iso, int render_deriv, int render_normal, int row, int col, bool is_selected, int tex, bool preview);
        GLvoid renderSelectedPatch(PatchAttributes* pattr, Material &mat);
        GLvoid renderSelectedPatchLines(PatchAttributes* pattr, int render_data, int render_iso, int render_deriv, int render_normal);
        GLvoid renderClickableObjects();
        GLboolean addToDataPointValue(const GLuint &patchInd, const GLuint &dataPointRow, const GLuint &dataPointCol, const GLuint &pointComponentInd, const GLdouble &diff);
        GLboolean getDataPointValues(const GLuint &patchInd, const GLuint &dataPointRow, const GLuint &dataPointCol, GLdouble &x, GLdouble &y, GLdouble &z);
        Material* getMaterial(const GLuint &patchInd);
        GLboolean setMaterial(const GLuint &patchInd, Material *mat);

        GLboolean deletePatch(const GLuint &patchInd);

        GLuint getPatchCount()
        {
            return (GLuint) _attributes.size();
        }

        GLvoid moveSelectedPatch(const GLuint &patchInd, Direction dir);
        GLboolean setShader(const int shaderInd);
        int getShaderIndex();

        GLboolean saveSurface(const string& fname);
        GLboolean loadSurface(const string& fname);
        GLboolean setIsoLineCountU(const int& countu);
        GLboolean setIsoLineCountV(const int& countv);

        bool set_def_toon_color_r(GLfloat value);
        bool set_def_toon_color_g(GLfloat value);
        bool set_def_toon_color_b(GLfloat value);
        bool set_def_toon_color_a(GLfloat value);

        bool set_rl_scale(GLfloat value);
        bool set_rl_smooth(GLfloat value);
        bool set_rl_shade(GLfloat value);

        bool set_alpha_checked(int ch);

        GLboolean getPointInfo(GLdouble &x,  GLdouble &y,  GLdouble &z, double zoom, int &patchInd, int &row, int &col, int rotx, int roty, int rotz, double trax, double tray, double traz);
        GLvoid cancelOperation(const int &last_op);
    };
}
