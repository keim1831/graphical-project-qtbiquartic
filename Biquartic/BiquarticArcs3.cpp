#include "BiquarticArcs3.h"

namespace cagd
{
    // special constructor
    BiquarticArc3::BiquarticArc3() : LinearCombination3(0.0, 1.0, 4)
    {
    }

    // redeclare and define inherited pure virtual methods
    GLboolean BiquarticArc3::BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const
    {
        values.ResizeColumns(4);

        GLdouble u2 = u * u, u3 = u2 * u, u4 = u2 * u2, w = (1.0 - u);

        values[0] = w * w * w * w;
        values[1] = -u4 + 6.0 * u3 - 9.0 * u2 + 4.0 * u;
        values[2] = -u4 - 2.0 * u3 + 3.0 * u2;
        values[3] = u4;

        return GL_TRUE;
    }

    GLboolean BiquarticArc3::CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const
    {
        if (max_order_of_derivatives > 2 || max_order_of_derivatives < 0)
            return GL_FALSE;

        d.ResizeRows(max_order_of_derivatives + 1);
        d.LoadNullVectors();

        GLdouble u2 = u * u, u3 = u2 * u, u4 = u2 * u2, w = (1.0 - u), w2 = w * w;

        d[0] = w2 * w2 * _data[0] +
              (-u4 + 6.0 * u3 - 9.0 * u2 + 4.0 * u) * _data[1] +
              (-u4 - 2.0 * u3 + 3.0 * u2) * _data[2] +
               u4 * _data[3];

        if (max_order_of_derivatives >= 1)
        {
            d[1] = -4.0 * w2 * w * _data[0] +
                   (-4.0 * u3 + 18.0 * u2 - 18.0 * u + 4.0) * _data[1] +
                   (6.0 * u - 6.0 * u2 - 4.0 * u3) * _data[2] +
                    4.0 * u3 * _data[3];

            if (max_order_of_derivatives >= 2)
            {
                d[2] = 12.0 * w2 * _data[0] +
                       (-12.0 * u2 + 36.0 * u - 18.0) * _data[1] +
                       (-12.0 * u2 - 12.0 * u + 6.0) * _data[2] +
                        12.0 * u2 * _data[3];
            }
        }

        return GL_TRUE;
    }

    GLboolean BiquarticArc3::RenderSelectedPoint(GLuint point_selected) const
    {
        if (!_vbo_data)
            return GL_FALSE;

        glBegin(GL_POINTS);
        glVertex3dv(&_data[point_selected][0]);
        glEnd();

        return GL_TRUE;
    }
}
