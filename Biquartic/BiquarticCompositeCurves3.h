#pragma once

#include "BiquarticArcs3.h"
#include <Core/Colors4.h>
#include <Core/Constants.h>
#include <Core/Exceptions.h>
#include <Core/TensorProductSurfaces3.h>

using namespace std;

namespace cagd
{
    class BiquarticCompositeCurve3
    {
    public:
        enum Direction {LEFT, RIGHT};

        class ArcAttributes
        {
        public:
            BiquarticArc3   *arc;
            GenericCurve3   *img;
            Color4          *color;
            ArcAttributes   *previous, *next;

            ArcAttributes();
            ArcAttributes(const ArcAttributes&);
            ArcAttributes& operator=(const ArcAttributes&);
            ~ArcAttributes();
        };

        struct OperationPreview
        {
            ArcAttributes* newAttr = nullptr, *attr1 = nullptr, *attr2 = nullptr;
            Direction firstDir = RIGHT, secondDir = LEFT;
            int firstInd = -1, secondInd = -1;
        };

    protected:
        std::vector<ArcAttributes> _attributes;
        GLdouble _scale1 = 0.2;
        GLdouble _scale2 = 0.05;
        GLuint _div_point_count = 100;
        OperationPreview op;
        Color4 _color_of_affected;

    public:
        BiquarticCompositeCurve3(const size_t min_arc_count = 1000);
        ~BiquarticCompositeCurve3();

        GLboolean insertNewIsolatedArc();
        GLboolean continueExistingArcPreview(const GLuint &index, Direction dir);
        GLvoid continueExistingArc();
        GLboolean joinExistingArcsPreview(const GLuint &arc_ind1, Direction dir1, const GLuint &arc_ind2, Direction dir2);
        GLvoid joinExistingArcs();
        GLboolean mergeExistingArcsPreview(const GLuint &arc_ind1, Direction dir1, const GLuint &arc_ind2, Direction dir2);
        GLvoid mergeExistingArcs();
        GLvoid cancelOperation(const int &last_op);

        GLvoid renderAllArcs(const int &d1, const int &d2, int arcInd, int render_data, int selected_point, bool is_selected, bool preview);
        GLvoid renderClickableObjects();
        GLvoid renderSelectedArc(const int &d1, const int &d2, GLuint arcInd, int render_data);
        GLvoid renderSelectedArc(const int &d1, const int &d2, ArcAttributes* attr, int render_data, Color4 &color);

        GLboolean addToDataPointValue(const GLuint &arcInd, const GLuint &dataPointInd, const GLuint &pointComponentInd, const GLdouble &val_to_add);
        GLboolean getDataPointValues(const GLuint &arcInd, const GLuint &dataPointInd, GLdouble &x, GLdouble &y, GLdouble &z);
        GLboolean getColorComponents(const GLuint &arcInd, GLdouble &r, GLdouble &g, GLdouble &b);
        GLboolean changeColorComponentValue(const GLuint &arcInd, const GLuint &colorComponentInd, const GLdouble &val);
        GLboolean deleteArc(const GLuint &arcInd);

        GLboolean saveCurve(const string& fname);
        GLboolean loadCurve(const string& fname);

        GLuint getArcCount()
        {
            return (GLuint) _attributes.size();
        }
    };
}
