#include "BiquarticCompositeCurves3.h"
#include <iostream>
#include <fstream>
#include <QTransform>
#include <QVector3D>
#include <QMatrix4x4>

using namespace std;

namespace cagd
{
    BiquarticCompositeCurve3::ArcAttributes::ArcAttributes()
    {
        arc = new BiquarticArc3();
        color = new Color4(0.2f, 0.4f, 0.8f, 1.0f);
        img = nullptr;
        previous = nullptr;
        next = nullptr;
    }

    BiquarticCompositeCurve3::ArcAttributes::ArcAttributes(const ArcAttributes& aa)
    {
        if (aa.arc)
            this->arc = new BiquarticArc3(*(aa.arc));
        else
            arc = nullptr;

        if (aa.img)
            this->img = new GenericCurve3(*(aa.img));
        else
            img = nullptr;

        if (aa.color)
            this->color = new Color4(*(aa.color));
        else
            color = nullptr;

        this->previous = aa.previous;
        this->next = aa.next;
    }

    BiquarticCompositeCurve3::ArcAttributes& BiquarticCompositeCurve3::ArcAttributes::operator=(const ArcAttributes& aa)
    {
        if (this->arc)
            delete arc, arc = nullptr;
        if (aa.arc)
            this->arc = new BiquarticArc3(*(aa.arc));
        else
            arc = nullptr;

        if (this->img)
            delete img, img = nullptr;
        if (aa.img)
            this->img = new GenericCurve3(*(aa.img));
        else
            img = nullptr;

        if (this->color)
            delete color, color = nullptr;
        if (aa.color)
            this->color = new Color4(*(aa.color));
        else
            color = nullptr;

        this->previous = aa.previous;
        this->next = aa.next;

        return *this;
    }

    BiquarticCompositeCurve3::ArcAttributes::~ArcAttributes()
    {
        if (arc)
            delete arc, arc = nullptr;
        if (img)
            delete img, img = nullptr;
        if (color)
            delete color, color = nullptr;
    }

    BiquarticCompositeCurve3::~BiquarticCompositeCurve3()
    {
        _attributes.clear();
    }

    BiquarticCompositeCurve3::BiquarticCompositeCurve3(const size_t min_arc_count) : _color_of_affected(0.95f, 0.31f, 0.22f)
    {
        _attributes.reserve(min_arc_count);
        ArcAttributes firstAttr;

        if (firstAttr.arc)
        {
            cout << "Biquartic arc created" << endl;
            GLdouble step = PI / 6.0;
            for (GLuint i = 0; i <= 3; ++i)
            {
                DCoordinate3 &cp_i = (*firstAttr.arc)[i];
                GLdouble u = i * step;
                cp_i[0] = cos(u);
                cp_i[1] = sin(u);
                cp_i[2] = 2.0 * rand() / (GLdouble) (RAND_MAX);
            }
            firstAttr.img = firstAttr.arc->GenerateImage(2, _div_point_count);
        }

        _attributes.push_back(firstAttr);
        _attributes[0].arc->UpdateVertexBufferObjectsOfData();
        _attributes[0].img->UpdateVertexBufferObjects(_scale1, _scale2);
        cout << "First arc\n";
    }

    GLvoid BiquarticCompositeCurve3::renderAllArcs(const int &d1, const int &d2, int selectedArcIndex, int render_data, int selected_point, bool is_selected, bool preview)
    {
        int index = 0;

        if (!preview)
        {
            cout << "Render - no preview\n";
            for (vector<ArcAttributes>::iterator it = _attributes.begin(); it != _attributes.end(); ++it, ++index)
            {
                glLineWidth(0.1f);

                if (index == selectedArcIndex)
                {
                    if (is_selected)
                    {
                        glPointSize(8.0);
                        glColor3f(0.12f, 0.89f, 0.34f);
                        it->arc->RenderSelectedPoint(selected_point);
                    }

                    glLineWidth(3.5f);
                }

                renderSelectedArc(d1, d2, &(*it), render_data, *it->color);
            }
            return;
        }

        if (op.newAttr)
        {
            cout << "Render - continue/join preview\n";
            glLineWidth(0.1f);
            for (vector<ArcAttributes>::iterator it = _attributes.begin(); it != _attributes.end(); ++it, ++index)
            {
                renderSelectedArc(d1, d2, &(*it), render_data, *it->color);
            }

            if (op.newAttr->arc && render_data)
            {
                glColor3f(0.9f, 0.4f, 0.5f);
                op.newAttr->arc->RenderData(GL_LINE_STRIP);

                glPointSize(5.0);
                op.newAttr->arc->RenderData(GL_POINTS);
            }

            if (op.newAttr->img)
            {
                glLineWidth(3.5f);
                renderSelectedArc(d1, d2, op.newAttr, render_data, _color_of_affected);
            }
            return;
        }

        cout << "Render - merge preview\n";

        for (vector<ArcAttributes>::iterator it = _attributes.begin(); it != _attributes.end(); ++it, ++index)
        {
            glLineWidth(0.1f);

            if (index != op.firstInd && index != op.secondInd)
            {
                renderSelectedArc(d1, d2, &(*it), render_data, *it->color);
            }
        }

        glLineWidth(3.5f);
        renderSelectedArc(d1, d2, op.attr1, render_data, _color_of_affected);
        glLineWidth(3.5f);
        renderSelectedArc(d1, d2, op.attr2, render_data, _color_of_affected);
    }

    GLvoid BiquarticCompositeCurve3::renderClickableObjects()
    {
        int index = 0;

        glColor3f(0.9f, 0.4f, 0.5f);
        glPointSize(5.0);

        for (vector<ArcAttributes>::iterator it = _attributes.begin(); it != _attributes.end(); ++it, ++index)
        {
            if (it->arc)
            {
                for (int i = 0; i < 4; ++i)
                {
                    glLoadName(index * 4 + i);
                    it->arc->RenderSelectedPoint(i);
                }
            }
        }
    }

    GLvoid BiquarticCompositeCurve3::renderSelectedArc(const int &d1, const int &d2, GLuint selectedArcIndex, int render_data)
    {
        if (selectedArcIndex < 0 || selectedArcIndex >= _attributes.size())
            return;

        ArcAttributes* attr = &_attributes[selectedArcIndex];

        glLineWidth(3.5f);

        if (attr->arc && render_data)
        {
            glColor3f(0.9f, 0.4f, 0.5f);
            attr->arc->RenderData(GL_LINE_STRIP);

            glPointSize(5.0);
            attr->arc->RenderData(GL_POINTS);
        }

        if (attr->img)
        {
            glColor4fv(&(*attr->color)[0]);

            attr->img->RenderDerivatives(0, GL_LINE_STRIP);

            if (d1)
            {
                glColor3f(0.5f, 0.2f, 0.2f);
                attr->img->RenderDerivatives(1, GL_LINES);
            }
            if (d2)
            {
                glColor3f(0.7f, 0.4f, 0.2f);
                attr->img->RenderDerivatives(2, GL_LINES);
            }
        }
    }

    GLvoid BiquarticCompositeCurve3::renderSelectedArc(const int &d1, const int &d2, ArcAttributes* attr, int render_data, Color4 &color)
    {
        if (attr->img)
        {
            glColor4fv(&color[0]);
            attr->img->RenderDerivatives(0, GL_LINE_STRIP);

            glLineWidth(0.1f);

            if (d1)
            {
                glColor3f(0.5f, 0.2f, 0.2f);
                attr->img->RenderDerivatives(1, GL_LINES);
            }
            if (d2)
            {
                glColor3f(0.7f, 0.4f, 0.2f);
                attr->img->RenderDerivatives(2, GL_LINES);
            }
        }

        if (attr->arc && render_data)
        {
            glColor3f(0.9f, 0.4f, 0.5f);
            attr->arc->RenderData(GL_LINE_STRIP);

            glPointSize(5.0);
            attr->arc->RenderData(GL_POINTS);
        }
    }


    GLboolean BiquarticCompositeCurve3::addToDataPointValue(const GLuint &arcInd, const GLuint &dataPointInd, const GLuint &pointComponentInd, const GLdouble &diff)
    {
        if (arcInd < 0 || arcInd >= _attributes.size())
            return GL_FALSE;

        ArcAttributes* attr = &_attributes[arcInd];
        DCoordinate3 &cp = (*attr->arc)[dataPointInd];

        GLdouble val = cp[pointComponentInd] + diff;
        ArcAttributes* neighbour = nullptr;

        // C1 continuity

        if (dataPointInd == 0 && attr->previous)
        {
            DCoordinate3 &p = (*attr->arc)[dataPointInd + 1];
            p[pointComponentInd] += diff;

            neighbour = attr->previous;

            if (neighbour->next == attr &&
                (*attr->arc)[0][0] == (*neighbour->arc)[3][0] &&
                (*attr->arc)[0][1] == (*neighbour->arc)[3][1] &&
                (*attr->arc)[0][2] == (*neighbour->arc)[3][2])
            {
                DCoordinate3 &p3 = (*neighbour->arc)[3];
                p3[pointComponentInd] = val;

                DCoordinate3 &p2 = (*neighbour->arc)[2];
                p2[pointComponentInd] += diff;
            }
            else if (neighbour->previous == attr &&
                 (*attr->arc)[0][0] == (*neighbour->arc)[0][0] &&
                 (*attr->arc)[0][1] == (*neighbour->arc)[0][1] &&
                 (*attr->arc)[0][2] == (*neighbour->arc)[0][2])
            {
                DCoordinate3 &p0 = (*neighbour->arc)[0];
                p0[pointComponentInd] = val;

                DCoordinate3 &p1 = (*neighbour->arc)[1];
                p1[pointComponentInd] += diff;
            }
        }
        else if (dataPointInd == 1 && attr->previous)
        {
            neighbour = attr->previous;

            if (neighbour->next == attr &&
                (*attr->arc)[0][0] == (*neighbour->arc)[3][0] &&
                (*attr->arc)[0][1] == (*neighbour->arc)[3][1] &&
                (*attr->arc)[0][2] == (*neighbour->arc)[3][2])
            {
                DCoordinate3 &p2 = (*neighbour->arc)[2];
                p2[pointComponentInd] -= diff;
            }
            else if (neighbour->previous == attr &&
                 (*attr->arc)[0][0] == (*neighbour->arc)[0][0] &&
                 (*attr->arc)[0][1] == (*neighbour->arc)[0][1] &&
                 (*attr->arc)[0][2] == (*neighbour->arc)[0][2])
            {
                DCoordinate3 &p1 = (*neighbour->arc)[1];
                p1[pointComponentInd] -= diff;
            }
        }
        else if (dataPointInd == 2 && attr->next)
        {
            neighbour = attr->next;

            if (neighbour->next == attr &&
                (*attr->arc)[3][0] == (*neighbour->arc)[3][0] &&
                (*attr->arc)[3][1] == (*neighbour->arc)[3][1] &&
                (*attr->arc)[3][2] == (*neighbour->arc)[3][2])
            {
                DCoordinate3 &p2 = (*neighbour->arc)[2];
                p2[pointComponentInd] -= diff;
            }
            else if (neighbour->previous == attr &&
                 (*attr->arc)[3][0] == (*neighbour->arc)[0][0] &&
                 (*attr->arc)[3][1] == (*neighbour->arc)[0][1] &&
                 (*attr->arc)[3][2] == (*neighbour->arc)[0][2])
            {
                DCoordinate3 &p1 = (*neighbour->arc)[1];
                p1[pointComponentInd] -= diff;
            }
        }
        else if (dataPointInd == 3 && attr->next)
        {
            DCoordinate3 &p = (*attr->arc)[dataPointInd - 1];
            p[pointComponentInd] += diff;

            neighbour = attr->next;

            if (neighbour->next == attr &&
                (*attr->arc)[3][0] == (*neighbour->arc)[3][0] &&
                (*attr->arc)[3][1] == (*neighbour->arc)[3][1] &&
                (*attr->arc)[3][2] == (*neighbour->arc)[3][2])
            {
                DCoordinate3 &p3 = (*neighbour->arc)[3];
                p3[pointComponentInd] = val;

                DCoordinate3 &p2 = (*neighbour->arc)[2];
                p2[pointComponentInd] += diff;
            }
            else if (neighbour->previous == attr &&
                 (*attr->arc)[3][0] == (*neighbour->arc)[0][0] &&
                 (*attr->arc)[3][1] == (*neighbour->arc)[0][1] &&
                 (*attr->arc)[3][2] == (*neighbour->arc)[0][2])
            {
                DCoordinate3 &p0 = (*neighbour->arc)[0];
                p0[pointComponentInd] = val;

                DCoordinate3 &p1 = (*neighbour->arc)[1];
                p1[pointComponentInd] += diff;
            }
        }

        cp[pointComponentInd] = val;

        attr->arc->UpdateVertexBufferObjectsOfData();

        if (attr->img)
            delete attr->img;
        attr->img = _attributes[arcInd].arc->GenerateImage(2, _div_point_count);
        attr->img->UpdateVertexBufferObjects(_scale1, _scale2);

        if (neighbour)
        {
            neighbour->arc->UpdateVertexBufferObjectsOfData();
            if (neighbour->img)
                delete neighbour->img;
            neighbour->img = neighbour->arc->GenerateImage(2, _div_point_count);
            neighbour->img->UpdateVertexBufferObjects(_scale1, _scale2);
        }

        return GL_TRUE;
    }

    GLboolean BiquarticCompositeCurve3::getDataPointValues(const GLuint &arcInd, const GLuint &dataPointInd, GLdouble &x, GLdouble &y, GLdouble &z)
    {
        if (arcInd < 0 || arcInd >= _attributes.size())
            return GL_FALSE;

        DCoordinate3 &p = (*_attributes[arcInd].arc)[dataPointInd];

        x = p[0];
        y = p[1];
        z = p[2];

        return GL_TRUE;
    }

    GLboolean BiquarticCompositeCurve3::getColorComponents(const GLuint &arcInd, GLdouble &r, GLdouble &g, GLdouble &b)
    {
        if (arcInd < 0 || arcInd >= _attributes.size())
            return GL_FALSE;

        Color4 &c = (*_attributes[arcInd].color);

        r = c[0];
        g = c[1];
        b = c[2];

        return GL_TRUE;
    }

    GLboolean BiquarticCompositeCurve3::changeColorComponentValue(const GLuint &arcInd, const GLuint &colorComponentInd, const GLdouble &val)
    {
        if (arcInd < 0 || arcInd >= _attributes.size() || colorComponentInd < 0 || colorComponentInd > 3)
            return GL_FALSE;

        Color4 &c = (*_attributes[arcInd].color);
        c[colorComponentInd] = val;

        return GL_TRUE;
    }

    GLboolean BiquarticCompositeCurve3::insertNewIsolatedArc()
    {
        ArcAttributes newAttr;

        if (newAttr.arc)
        {
            GLdouble step = PI / 6.0;
            GLdouble offset = -2.5 + 5.0 * rand() / (GLdouble) RAND_MAX;
            for (GLuint i = 0; i <= 3; ++i)
            {
                DCoordinate3 &cp_i = (*newAttr.arc)[i];
                GLdouble u = i * step;
                cp_i[0] = cos(u);
                cp_i[1] = sin(u) + offset;
                cp_i[2] = 2.0 * rand() / (GLdouble) (RAND_MAX);
            }

            newAttr.img = newAttr.arc->GenerateImage(2, _div_point_count);
        }

        _attributes.push_back(newAttr);

        _attributes[_attributes.size() - 1].arc->UpdateVertexBufferObjectsOfData();
        _attributes[_attributes.size() - 1].img->UpdateVertexBufferObjects(_scale1, _scale2);

        return GL_TRUE;
    }

    GLboolean BiquarticCompositeCurve3::joinExistingArcsPreview(const GLuint &arc_ind1, Direction dir1,
                               const GLuint &arc_ind2, Direction dir2)
    {
        if ((arc_ind1 == arc_ind2 && dir1 == dir2) || arc_ind1 >= _attributes.size() || arc_ind2 >= _attributes.size() ||
            (dir1 == RIGHT && _attributes[arc_ind1].next) || (dir1 == LEFT && _attributes[arc_ind1].previous) ||
            (dir2 == RIGHT && _attributes[arc_ind2].next) || (dir2 == LEFT && _attributes[arc_ind2].previous))
            return GL_FALSE;

        op.attr1 = &_attributes[arc_ind1];
        op.attr2 = &_attributes[arc_ind2];

        op.newAttr = new ArcAttributes();

        op.firstInd = arc_ind1;
        op.secondInd = arc_ind2;

        op.firstDir = dir1;
        op.secondDir = dir2;

        if (dir1 == RIGHT)
        {
            // _data[0]
            DCoordinate3 &q0 = (*op.newAttr->arc)[0];
            q0 = (*op.attr1->arc)[3];

            // _data[1]
            DCoordinate3 &q1 = (*op.newAttr->arc)[1];
            q1 = 2.0 * (*op.attr1->arc)[3] - (*op.attr1->arc)[2];

            if (dir2 == LEFT)
            {
                DCoordinate3 &q3 = (*op.newAttr->arc)[3];
                q3 = (*op.attr2->arc)[0];

                DCoordinate3 &q2 = (*op.newAttr->arc)[2];
                q2 = 2.0 * (*op.attr2->arc)[0] - (*op.attr2->arc)[1];
            }
            else
            {
                DCoordinate3 &q3 = (*op.newAttr->arc)[3];
                q3 = (*op.attr2->arc)[3];

                DCoordinate3 &q2 = (*op.newAttr->arc)[2];
                q2 = 2.0 * (*op.attr2->arc)[3] - (*op.attr2->arc)[2];
            }
        }
        else
        {
            // _data[3]
            DCoordinate3 &q3 = (*op.newAttr->arc)[3];
            q3 = (*op.attr1->arc)[0];

            // _data[2]
            DCoordinate3 &q2 = (*op.newAttr->arc)[2];
            q2 = 2.0 * (*op.attr1->arc)[0] - (*op.attr1->arc)[1];

            if (dir2 == LEFT)
            {
                DCoordinate3 &q0 = (*op.newAttr->arc)[0];
                q0 = (*op.attr2->arc)[0];

                DCoordinate3 &q1 = (*op.newAttr->arc)[1];
                q1 = 2.0 * (*op.attr2->arc)[0] - (*op.attr2->arc)[1];
            }
            else
            {
                DCoordinate3 &q0 = (*op.newAttr->arc)[0];
                q0 = (*op.attr2->arc)[3];

                DCoordinate3 &q1 = (*op.newAttr->arc)[1];
                q1 = 2.0 * (*op.attr2->arc)[3] - (*op.attr2->arc)[2];
            }
        }

        op.newAttr->img = op.newAttr->arc->GenerateImage(2, _div_point_count);
        op.newAttr->arc->UpdateVertexBufferObjectsOfData();
        op.newAttr->img ->UpdateVertexBufferObjects(_scale1, _scale2);

        return GL_TRUE;
    }

    GLvoid BiquarticCompositeCurve3::joinExistingArcs()
    {
        _attributes.push_back((*op.newAttr));
        int newInd = (int) _attributes.size() - 1;

        if (op.firstDir == RIGHT)
        {
            op.attr1->next = &_attributes[newInd];

            _attributes[newInd].previous = op.attr1;
            _attributes[newInd].next = op.attr2;

            if (op.secondDir == LEFT)
            {
                op.attr2->previous = &_attributes[newInd];
            }
            else
            {
                op.attr2->next = &_attributes[newInd];
            }
        }
        else
        {
            op.attr1->previous = &_attributes[newInd];

            _attributes[newInd].previous = op.attr2;
            _attributes[newInd].next = op.attr1;

            if (op.secondDir == LEFT)
            {
                op.attr2->previous = &_attributes[newInd];
            }
            else
            {
                op.attr2->next = &_attributes[newInd];
            }
        }

        _attributes[newInd].img = _attributes[newInd].arc->GenerateImage(2, _div_point_count);
        _attributes[newInd].arc->UpdateVertexBufferObjectsOfData();
        _attributes[newInd].img->UpdateVertexBufferObjects(_scale1, _scale2);

        delete op.newAttr;
        op.newAttr = nullptr;

        op.attr1 = nullptr;
        op.attr2 = nullptr;

        op.firstInd = -1;
        op.secondInd = -1;
    }

    GLboolean BiquarticCompositeCurve3::mergeExistingArcsPreview(const GLuint &arc_ind1, Direction dir1,
                                    const GLuint &arc_ind2, Direction dir2)
    {
        if (arc_ind1 == arc_ind2 || arc_ind1 >= _attributes.size() || arc_ind2 >= _attributes.size() ||
            (dir1 == RIGHT && _attributes[arc_ind1].next) || (dir1 == LEFT && _attributes[arc_ind1].previous) ||
            (dir2 == RIGHT && _attributes[arc_ind2].next) || (dir2 == LEFT && _attributes[arc_ind2].previous))
                return GL_FALSE;

        ArcAttributes* attr1 = &_attributes[arc_ind1];
        ArcAttributes* attr2 = &_attributes[arc_ind2];

        op.attr1 = new ArcAttributes(_attributes[arc_ind1]);
        op.attr2 = new ArcAttributes(_attributes[arc_ind2]);

        op.firstInd = arc_ind1;
        op.secondInd = arc_ind2;

        if (dir1 == RIGHT)
        {
            DCoordinate3 &p3 = (*op.attr1->arc)[3];
            op.attr1->next = attr2;

            if (dir2 == LEFT)
            {
                DCoordinate3 &r0 = (*op.attr2->arc)[0];
                DCoordinate3 newP = 0.5 * ((*op.attr1->arc)[2] + (*op.attr2->arc)[1]);

                p3 = newP;
                r0 = newP;

                op.attr2->previous = attr1;
            }
            else
            {
                DCoordinate3 &r3 = (*op.attr2->arc)[3];
                DCoordinate3 newP = 0.5 * ((*op.attr1->arc)[2] + (*op.attr2->arc)[2]);

                p3 = newP;
                r3 = newP;

                op.attr2->next = attr1;
            }
        }
        else
        {
            DCoordinate3 &p0 = (*op.attr1->arc)[0];
            op.attr1->previous = attr2;

            if (dir2 == LEFT)
            {
                DCoordinate3 &r0 = (*op.attr2->arc)[0];
                DCoordinate3 newP = 0.5 * ((*op.attr1->arc)[1] + (*op.attr2->arc)[1]);

                p0 = newP;
                r0 = newP;

                op.attr2->previous = attr1;
            }
            else
            {
                DCoordinate3 &r3 = (*op.attr2->arc)[3];
                DCoordinate3 newP = 0.5 * ((*op.attr1->arc)[1] + (*op.attr2->arc)[2]);

                p0 = newP;
                r3 = newP;

                op.attr2->next = attr1;
            }
        }

        op.attr1->arc->UpdateVertexBufferObjectsOfData();
        if (op.attr1->img)
            delete op.attr1->img;
        op.attr1->img = op.attr1->arc->GenerateImage(2, _div_point_count);
        if (op.attr1->img)
            op.attr1->img->UpdateVertexBufferObjects(_scale1, _scale2);

        op.attr2->arc->UpdateVertexBufferObjectsOfData();
        if (op.attr2->img)
            delete op.attr2->img;
        op.attr2->img = op.attr2->arc->GenerateImage(2, _div_point_count);
        if (op.attr2->img)
            op.attr2->img->UpdateVertexBufferObjects(_scale1, _scale2);

        return GL_TRUE;
    }

    GLvoid BiquarticCompositeCurve3::mergeExistingArcs()
    {
        ArcAttributes* attr1 = &_attributes[op.firstInd];
        ArcAttributes* attr2 = &_attributes[op.secondInd];

        _attributes[op.firstInd] = *op.attr1;
        _attributes[op.secondInd] = *op.attr2;

        delete op.attr1, op.attr1 = nullptr;
        delete op.attr2, op.attr2 = nullptr;

        op.firstInd = -1;
        op.secondInd = -1;

        attr1->arc->UpdateVertexBufferObjectsOfData();
        if (attr1->img)
            delete attr1->img;
        attr1->img = attr1->arc->GenerateImage(2, _div_point_count);
        if (attr1->img)
            attr1->img->UpdateVertexBufferObjects(_scale1, _scale2);

        attr2->arc->UpdateVertexBufferObjectsOfData();
        if (attr2->img)
            delete attr2->img;
        attr2->img = attr2->arc->GenerateImage(2, _div_point_count);
        if (attr2->img)
            attr2->img->UpdateVertexBufferObjects(_scale1, _scale2);
    }

    GLboolean BiquarticCompositeCurve3::continueExistingArcPreview(const GLuint &index, Direction dir)
    {
        if (index >= _attributes.size() || index < 0)
            return GL_FALSE;

        ArcAttributes *attr = &_attributes[index];

        if ((dir == LEFT && attr->previous) || (dir == RIGHT && attr->next))
            return GL_FALSE;

        op.firstDir = dir;
        op.firstInd = index;
        if (op.newAttr != nullptr)
            delete  op.newAttr;

        op.newAttr = new ArcAttributes();

        DCoordinate3 &r3 = (*op.newAttr->arc)[3];
        DCoordinate3 &r2 = (*op.newAttr->arc)[2];
        DCoordinate3 &r1 = (*op.newAttr->arc)[1];
        DCoordinate3 &r0 = (*op.newAttr->arc)[0];

        if (dir == LEFT)
        {
            DCoordinate3 diff = (*attr->arc)[0] - (*attr->arc)[1];
            r3 = (*attr->arc)[0];
            r2 = r3 + diff;
            r1 = r2 + diff;
            r0 = r1 + diff;
        }
        else
        {
            DCoordinate3 diff = (*attr->arc)[3] - (*attr->arc)[2];
            r0 = (*attr->arc)[3];
            r1 = r0 + diff;
            r2 = r1 + diff;
            r3 = r2 + diff;
        }

        op.newAttr->arc->UpdateVertexBufferObjectsOfData();
        op.newAttr->img = op.newAttr->arc->GenerateImage(2, _div_point_count);
        op.newAttr->img->UpdateVertexBufferObjects(_scale1, _scale2);

        return GL_TRUE;
    }

    GLvoid BiquarticCompositeCurve3::continueExistingArc()
    {
        _attributes.push_back(*op.newAttr);
        int newInd = (int) _attributes.size() - 1;
        ArcAttributes *attr = &_attributes[op.firstInd];

        if (op.firstDir == LEFT)
        {
            attr->previous = &_attributes[newInd];
            _attributes[newInd].next = attr;
        }
        else
        {
            attr->next = &_attributes[newInd];
            _attributes[newInd].previous = attr;
        }

        _attributes[newInd].arc->UpdateVertexBufferObjectsOfData();
        _attributes[newInd].img = _attributes[newInd].arc->GenerateImage(2, _div_point_count);
        _attributes[newInd].img->UpdateVertexBufferObjects(_scale1, _scale2);

        delete op.newAttr, op.newAttr = nullptr;
    }

    GLvoid BiquarticCompositeCurve3::cancelOperation(const int &last_op)
    {
        switch(last_op)
        {
        // continue
        case 0:
            delete op.newAttr;
            op.newAttr = nullptr;
            return;
        // join
        case 1:
            delete op.newAttr;
            op.newAttr = nullptr;

            op.attr1 = nullptr;
            op.attr2 = nullptr;

            op.firstInd = -1;
            op.secondInd = -1;
            return;
        // merge
        case 2:
            delete op.attr1, op.attr1 = nullptr;
            delete op.attr2, op.attr2 = nullptr;

            op.firstInd = -1;
            op.secondInd = -1;
            return;
        default:
            return;
        }
    }

    GLboolean BiquarticCompositeCurve3::deleteArc(const GLuint &arcInd)
    {
        if (arcInd >= _attributes.size() || arcInd < 0)
            return GL_FALSE;

        ArcAttributes *lastAttr = &_attributes[_attributes.size() - 1];
        ArcAttributes *attrToDelete = &_attributes[arcInd];

        if (attrToDelete->previous)
        {
            if (attrToDelete->previous->next == attrToDelete)
            {
                attrToDelete->previous->next = nullptr;
            }
            else
            {
                attrToDelete->previous->previous = nullptr;
            }
        }

        if (attrToDelete->next)
        {
            if (attrToDelete->next->next == attrToDelete)
            {
                attrToDelete->next->next = nullptr;
            }
            else
            {
                attrToDelete->next->previous = nullptr;
            }
        }


        if (lastAttr != attrToDelete)
        {
            _attributes[arcInd] = (*lastAttr);

            if (_attributes[arcInd].arc)
                _attributes[arcInd].arc->UpdateVertexBufferObjectsOfData();
            if (_attributes[arcInd].img)
                _attributes[arcInd].img->UpdateVertexBufferObjects(_scale1, _scale2);

            if (lastAttr->previous)
            {
                if (lastAttr->previous->previous == lastAttr)
                {
                    lastAttr->previous->previous = &_attributes[arcInd];
                }
                else
                {
                    lastAttr->previous->next = &_attributes[arcInd];
                }
            }

            if (lastAttr->next)
            {
                if (lastAttr->next->previous == lastAttr)
                {
                    lastAttr->next->previous = &_attributes[arcInd];
                }
                else
                {
                    lastAttr->next->next = &_attributes[arcInd];
                }
            }
        }

        _attributes.resize(_attributes.size() - 1);

        return GL_TRUE;
    }

    GLboolean BiquarticCompositeCurve3::loadCurve(const string& fname)
    {
        cout << "load\n";

        fstream in(fname.c_str(), ios_base::in);

        if (!in.is_open())
            return GL_FALSE;

        GLuint n;
        in >> n;

        _attributes.resize(n);

        for (GLuint i = 0; i < n; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                in >> (*_attributes[i].arc)[j];
            }

            for (int j = 0; j < 4; ++j)
            {
                in >> (*_attributes[i].color)[j];
            }

            int prev_ind, next_ind;
            in >> prev_ind >> next_ind;

            if (prev_ind == -1)
            {
                _attributes[i].previous = nullptr;
            }
            else
            {
                _attributes[i].previous = &_attributes[prev_ind];
            }

            if (next_ind == -1)
            {
                _attributes[i].next = nullptr;
            }
            else
            {
                _attributes[i].next = &_attributes[next_ind];
            }

            _attributes[i].arc->UpdateVertexBufferObjectsOfData();

            if (_attributes[i].img)
                delete _attributes[i].img;

            _attributes[i].img = _attributes[i].arc->GenerateImage(2, _div_point_count);
            _attributes[i].img->UpdateVertexBufferObjects(_scale1, _scale2);
        }

        in.close();

        return GL_TRUE;
    }

    GLboolean BiquarticCompositeCurve3::saveCurve(const string& fname)
    {
        cout << "save\n";

        if (_attributes.size() == 0)
        {
            cout << "No arcs!\n";
            return GL_FALSE;
        }

        fstream out(fname.c_str(), ios_base::out);

        if (!out.is_open())
            return GL_FALSE;

        // nr of arcs
        out << _attributes.size() << "\n\n";

        for (vector<ArcAttributes>::iterator it = _attributes.begin(); it != _attributes.end(); ++it)
        {
            // data points
            for (GLuint i = 0; i < 4; ++i)
            {
                out << (*it->arc)[i] << " ";
            }
            out << "\n";

            // color
            for (GLuint i = 0; i < 4; ++i)
            {
                out << (*it->color)[i] << " ";
            }
            out << "\n";

            int ind_prev = -1;
            int ind_next = -1;

            for (GLuint i = 0; i < _attributes.size(); ++i)
            {
                if (&_attributes[i] == it->previous)
                    ind_prev = i;
                if (&_attributes[i] == it->next)
                    ind_next = i;
            }

            out << ind_prev << " " << ind_next << "\n\n";
        }

        out.close();

        return GL_TRUE;
    }
}
