#pragma once

#include <QPushButton>

namespace cagd
{
    class ActionButton : public QPushButton
    {
         Q_OBJECT
    public:
       ActionButton(QWidget *parent = 0);

    protected:
        bool event(QEvent *event);

    public slots:
        void invalidOperationError();

    signals:
        void hoverEnter();
        void hoverLeave();
    };
}
