#include "ActionButton.h"
#include <QHoverEvent>
#include <iostream>

using namespace std;

namespace cagd
{
    ActionButton::ActionButton(QWidget *parent) : QPushButton(parent)
    {
        this->setStyleSheet("QPushButton {background-color: #58a181}");
        setMouseTracking(true);
        setAttribute(Qt::WA_Hover);
    }

    void ActionButton::invalidOperationError()
    {
        QFont font = this->font();
        font.setBold(false);
        this->setFont(font);
        this->setStyleSheet("QPushButton {background-color: #d14f4f}");
        repaint();
    }

    bool ActionButton::event(QEvent *e)
    {
        QFont font = this->font();

        switch (e->type())
        {
        case QEvent::HoverEnter:

            font.setBold(true);
            this->setFont(font);
            repaint();

            emit hoverEnter();
            return true;
        case QEvent::HoverLeave:

            font.setBold(false);
            this->setFont(font);
            this->setStyleSheet("QPushButton {background-color: #58a181}");
            repaint();

            emit hoverLeave();
            return true;
        default:
            break;
        }
        return QWidget::event(e);
    }
}
