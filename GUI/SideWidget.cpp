#include "SideWidget.h"

namespace cagd
{
    SideWidget::SideWidget(QWidget *parent): QWidget(parent)
    {
        setupUi(this);

        QPalette p = rotate_x_slider->palette();
        p.setColor(QPalette::Highlight, QColor(99,168,134));
        rotate_x_slider->setPalette(p);

        p = rotate_y_slider->palette();
        p.setColor(QPalette::Highlight, QColor(99,168,134));
        rotate_y_slider->setPalette(p);

        p = rotate_z_slider->palette();
        p.setColor(QPalette::Highlight, QColor(99,168,134));
        rotate_z_slider->setPalette(p);


        arc_r->palette();
        p.setColor(QPalette::Highlight, QColor(255,50,10).lighter());
        arc_r->setPalette(p);

        p = arc_g->palette();
        p.setColor(QPalette::Highlight, QColor(50,255,10).lighter());
        arc_g->setPalette(p);
    }
}
