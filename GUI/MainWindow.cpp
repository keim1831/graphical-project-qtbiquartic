#include "MainWindow.h"

namespace cagd
{
    MainWindow::MainWindow(QWidget *parent): QMainWindow(parent)
    {
        setupUi(this);

    /*

      the structure of the main window's central widget

     *---------------------------------------------------*
     |                 central widget                    |
     |                                                   |
     |  *---------------------------*-----------------*  |
     |  |     rendering context     |   scroll area   |  |
     |  |       OpenGL widget       | *-------------* |  |
     |  |                           | | side widget | |  |
     |  |                           | |             | |  |
     |  |                           | |             | |  |
     |  |                           | *-------------* |  |
     |  *---------------------------*-----------------*  |
     |                                                   |
     *---------------------------------------------------*

    */
        _side_widget = new SideWidget(this);

        _scroll_area = new QScrollArea(this);
        _scroll_area->setWidget(_side_widget);
        _scroll_area->setSizePolicy(_side_widget->sizePolicy());
        _scroll_area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

        _side_widget->arc_change_info->setToolTip("To change the position of a data point, click on the point, then by pressing the following buttons, select which component you want to modify:\n\t(1) Ctrl -> x\n\t(2) Alt -> y\n\t(3) Shift ->z\nKeep it pressed, then use the scroll wheel for modification.");
        _side_widget->patch_change_info->setToolTip("To change the position of a data point, click on the point, then by pressing the following buttons, select which component you want to modify:\n\t(1) Ctrl -> x\n\t(2) Alt -> y\n\t(3) Shift ->z\nKeep it pressed, then use the scroll wheel for modification.");

        _gl_widget = new GLWidget(this);
        _side_widget->shaderTabW->setTabText(3, "Reflection\nlines");
        _side_widget->shaderTabW->setStyleSheet("QTabBar::tab { height: 40px; }");

        centralWidget()->setLayout(new QHBoxLayout());
        centralWidget()->layout()->addWidget(_gl_widget);
        centralWidget()->layout()->addWidget(_scroll_area);

        // creating a signal slot mechanism between the rendering context and the side widget
        connect(_side_widget->rotate_x_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
        connect(_side_widget->rotate_y_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
        connect(_side_widget->rotate_z_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));

        connect(_side_widget->zoom_factor_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));

        connect(_side_widget->trans_x_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_x(double)));
        connect(_side_widget->trans_y_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_y(double)));
        connect(_side_widget->trans_z_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_z(double)));

        // ARCS

        // selected arc
        connect(_side_widget->arc_ind, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_arc_index(int)));

        // data points
        connect(_gl_widget, SIGNAL(arc_changed(int)), _side_widget->arc_ind, SLOT(setValue(int)));
        connect(_gl_widget, SIGNAL(point_changed(int)), _side_widget->c_point_ind, SLOT(setValue(int)));

        // insert/delete
        connect(_side_widget->newArcButton, SIGNAL(clicked()), _gl_widget, SLOT(add_new_arc()));
        connect(_side_widget->deleteArcButton, SIGNAL(clicked()), _gl_widget, SLOT(delete_arc()));

        // operations with 2 arcs: merge & join
        connect(_side_widget->join_dir1, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_operation_dir1(int)));
        connect(_side_widget->join_dir2, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_operation_dir2(int)));
        connect(_side_widget->join1, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_operation_ind1(int)));
        connect(_side_widget->join2, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_operation_ind2(int)));
        // join
        connect(_side_widget->joinButton, SIGNAL(clicked()), _gl_widget, SLOT(handle_join()));
        connect(_side_widget->joinButton, SIGNAL(hoverEnter()), _gl_widget, SLOT(joinPreview()));
        connect(_side_widget->joinButton, SIGNAL(hoverLeave()), _gl_widget, SLOT(unsetPreview()));
        // merge
        connect(_side_widget->mergeButton, SIGNAL(clicked()), _gl_widget, SLOT(handle_merge()));
        connect(_side_widget->mergeButton, SIGNAL(hoverEnter()), _gl_widget, SLOT(mergePreview()));
        connect(_side_widget->mergeButton, SIGNAL(hoverLeave()), _gl_widget, SLOT(unsetPreview()));
        // continue
        connect(_side_widget->continueCombo, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_continue_dir(int)));
        connect(_side_widget->continueButton, SIGNAL(clicked()), _gl_widget, SLOT(handle_continue()));
        connect(_side_widget->continueButton, SIGNAL(hoverEnter()), _gl_widget, SLOT(continuePreview()));
        connect(_side_widget->continueButton, SIGNAL(hoverLeave()), _gl_widget, SLOT(unsetPreview()));

        // invalid
        connect(_gl_widget, SIGNAL(arcInvalidContinue()), _side_widget->continueButton, SLOT(invalidOperationError()));
        connect(_gl_widget, SIGNAL(arcInvalidJoin()), _side_widget->joinButton, SLOT(invalidOperationError()));
        connect(_gl_widget, SIGNAL(arcInvalidMerge()), _side_widget->mergeButton, SLOT(invalidOperationError()));

        // derivatives
        connect(_side_widget->deriv1, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_deriv1(int)));
        connect(_side_widget->deriv2, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_deriv2(int)));

        // GLWidget signals
        connect(_gl_widget, SIGNAL(color_has_changed_r(int)), _side_widget->arc_r, SLOT(setValue(int)));
        connect(_gl_widget, SIGNAL(color_has_changed_g(int)), _side_widget->arc_g, SLOT(setValue(int)));
        connect(_gl_widget, SIGNAL(color_has_changed_b(int)), _side_widget->arc_b, SLOT(setValue(int)));

        // color
        connect(_side_widget->arc_r, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_color_r(int)));
        connect(_side_widget->arc_g, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_color_g(int)));
        connect(_side_widget->arc_b, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_color_b(int)));

        // render
        connect(_side_widget->render_data, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_render_data(int)));

        // tabs (Arcs/Patches/Lights & Shaders)
        connect(_side_widget->tabWidget, SIGNAL(currentChanged(int)), _gl_widget, SLOT(set_geometry_type(int)));


        // PATCHES

        // data points
        connect(_side_widget->point_row, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_patch_row_index(int)));
        connect(_side_widget->point_col, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_patch_col_index(int)));

        connect(_gl_widget, SIGNAL(patch_changed(int)), _side_widget->patch_ind, SLOT(setValue(int)));
        connect(_gl_widget, SIGNAL(row_changed(int)), _side_widget->point_row, SLOT(setValue(int)));
        connect(_gl_widget, SIGNAL(col_changed(int)), _side_widget->point_col, SLOT(setValue(int)));

        // material
        connect(_gl_widget, SIGNAL(material_changed(int)), _side_widget->materialCombo, SLOT(setCurrentIndex(int)));
        connect(_side_widget->materialCombo, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_material(int)));

        // selected patch
        connect(_side_widget->patch_ind, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_patch_index(int)));

        // insert/delete
        connect(_side_widget->newPatchButton, SIGNAL(clicked()), _gl_widget, SLOT(add_new_patch()));
        connect(_side_widget->deletePatchButton, SIGNAL(clicked()), _gl_widget, SLOT(delete_patch()));

        // merge
        connect(_side_widget->mergePatchDir1, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_merge_patch_dir1(int)));
        connect(_side_widget->mergePatchDir2, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_merge_patch_dir2(int)));
        connect(_side_widget->mergePatch1, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_merge_patch_ind1(int)));
        connect(_side_widget->mergePatch2, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_merge_patch_ind2(int)));
        connect(_side_widget->mergePatchButton, SIGNAL(clicked()), _gl_widget, SLOT(handle_patch_merge()));

        // render
        connect(_side_widget->render_net, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_render_control_net(int)));
        connect(_side_widget->render_iso, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_render_iso(int)));
        connect(_side_widget->render_deriv, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_render_iso_deriv(int)));
        connect(_side_widget->render_normals, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_render_normals(int)));

        // join
        connect(_side_widget->joinPatchDir1, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_join_patch_dir1(int)));
        connect(_side_widget->joinPatchDir2, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_join_patch_dir2(int)));
        connect(_side_widget->joinPatch1, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_join_patch_ind1(int)));
        connect(_side_widget->joinPatch2, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_join_patch_ind2(int)));
        connect(_side_widget->joinPatchButton, SIGNAL(clicked()), _gl_widget, SLOT(handle_patch_join()));

        // continue
        connect(_side_widget->continuePatchButton, SIGNAL(clicked()), _gl_widget, SLOT(handle_continue_patch()));
        connect(_side_widget->continuePatchDir, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_continue_patch_dir(int)));
        connect(_side_widget->continuePatchButton, SIGNAL(hoverEnter()), _gl_widget, SLOT(continuePatchPreview()));
        connect(_side_widget->continuePatchButton, SIGNAL(hoverLeave()), _gl_widget, SLOT(unsetPreview()));
        connect(_gl_widget, SIGNAL(patchInvalidContinue()), _side_widget->continuePatchButton, SLOT(invalidOperationError()));

        // move
        connect(_side_widget->moveUp, SIGNAL(clicked()), _gl_widget, SLOT(move_up()));
        connect(_side_widget->moveDown, SIGNAL(clicked()), _gl_widget, SLOT(move_down()));
        connect(_side_widget->moveLeft, SIGNAL(clicked()), _gl_widget, SLOT(move_left()));
        connect(_side_widget->moveRight, SIGNAL(clicked()), _gl_widget, SLOT(move_right()));

        // shaders
        connect(_side_widget->shaderTabW, SIGNAL(currentChanged(int)), _gl_widget, SLOT(set_shader(int)));

        connect(_side_widget->toon_r, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_def_toon_color_r(int)));
        connect(_side_widget->toon_g, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_def_toon_color_g(int)));
        connect(_side_widget->toon_b, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_def_toon_color_b(int)));
        connect(_side_widget->toon_a, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_def_toon_color_a(int)));

        connect(_side_widget->rl_scale, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_rl_scale(double)));
        connect(_side_widget->rl_smooth, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_rl_smooth(double)));
        connect(_side_widget->rl_shade, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_rl_shade(double)));

        connect(_side_widget->alpha_on, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_alpha_checked(int)));


        // light types
        connect(_side_widget->lightTypes, SIGNAL(currentChanged(int)), _gl_widget, SLOT(set_light(int)));

        // iso line count
        connect(_side_widget->isoCountU, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_iso_count_u(int)));
        connect(_side_widget->isoCountV, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_iso_count_v(int)));

        // texture coordinates
        connect(_side_widget->use_tex, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_tex_checked(int)));
        connect(_side_widget->texImg, SIGNAL(clicked()), _gl_widget, SLOT(change_tex_img()));

        // directional light attributes
        connect(_side_widget->dl_intensity_ind, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_dl_intensity_ind(int)));

        connect(_side_widget->dl_x, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_dl_position_x(double)));
        connect(_side_widget->dl_y, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_dl_position_y(double)));
        connect(_side_widget->dl_z, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_dl_position_z(double)));

        connect(_side_widget->dl_r, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_dl_r(double)));
        connect(_side_widget->dl_g, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_dl_g(double)));
        connect(_side_widget->dl_b, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_dl_b(double)));
        connect(_side_widget->dl_a, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_dl_a(double)));

        connect(_gl_widget, SIGNAL(dl_light_r_changed(double)), _side_widget->dl_r, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(dl_light_g_changed(double)), _side_widget->dl_g, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(dl_light_b_changed(double)), _side_widget->dl_b, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(dl_light_a_changed(double)), _side_widget->dl_a, SLOT(setValue(double)));


        // point light attributes
        connect(_side_widget->pl_intensity_ind, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_pl_intensity_ind(int)));

        connect(_side_widget->pl_x, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_pl_position_x(double)));
        connect(_side_widget->pl_y, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_pl_position_y(double)));
        connect(_side_widget->pl_z, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_pl_position_z(double)));
        connect(_side_widget->pl_w, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_pl_position_w(double)));

        connect(_side_widget->pl_r, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_pl_r(double)));
        connect(_side_widget->pl_g, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_pl_g(double)));
        connect(_side_widget->pl_b, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_pl_b(double)));
        connect(_side_widget->pl_a, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_pl_a(double)));

        connect(_gl_widget, SIGNAL(pl_light_r_changed(double)), _side_widget->pl_r, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(pl_light_g_changed(double)), _side_widget->pl_g, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(pl_light_b_changed(double)), _side_widget->pl_b, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(pl_light_a_changed(double)), _side_widget->pl_a, SLOT(setValue(double)));

        connect(_side_widget->pl_ca, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_pl_constant_attenuation(double)));
        connect(_side_widget->pl_la, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_pl_linear_attenuation(double)));
        connect(_side_widget->pl_qa, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_pl_quadratic_attenuation(double)));

        // spot light attributes
        connect(_side_widget->sl_intensity_ind, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_sl_intensity_ind(int)));

        connect(_side_widget->sl_x, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_position_x(double)));
        connect(_side_widget->sl_y, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_position_y(double)));
        connect(_side_widget->sl_z, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_position_z(double)));
        connect(_side_widget->sl_w, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_position_w(double)));

        connect(_side_widget->sl_r, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_r(double)));
        connect(_side_widget->sl_g, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_g(double)));
        connect(_side_widget->sl_b, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_b(double)));
        connect(_side_widget->sl_a, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_a(double)));

        connect(_gl_widget, SIGNAL(sl_light_r_changed(double)), _side_widget->sl_r, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(sl_light_g_changed(double)), _side_widget->sl_g, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(sl_light_b_changed(double)), _side_widget->sl_b, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(sl_light_a_changed(double)), _side_widget->sl_a, SLOT(setValue(double)));

        connect(_side_widget->sl_ca, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_constant_attenuation(double)));
        connect(_side_widget->sl_la, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_linear_attenuation(double)));
        connect(_side_widget->sl_qa, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_quadratic_attenuation(double)));

        connect(_side_widget->sl_cutoff, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_spot_cutoff(double)));
        connect(_side_widget->sl_exp, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_spot_exponent(double)));

        connect(_side_widget->sl_dir_x, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_spot_dir_x(double)));
        connect(_side_widget->sl_dir_y, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_spot_dir_y(double)));
        connect(_side_widget->sl_dir_z, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_sl_spot_dir_z(double)));
    }

    //--------------------------------
    // implementation of private slots
    //--------------------------------
    void MainWindow::on_action_Quit_triggered()
    {
        qApp->exit(0);
    }

    void MainWindow::on_action_Save_triggered()
    {
        _gl_widget->save();
    }

    void MainWindow::on_action_Open_triggered()
    {
        _gl_widget->open();
    }
}
