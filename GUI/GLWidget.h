#pragma once

#include <GL/glew.h>
#include <QGLWidget>
#include <QGLFormat>
#include <Biquartic/BiquarticCompositeCurves3.h>
#include <Biquartic/BiquarticCompositeSurfaces3.h>
#include <Core/Lights.h>

namespace cagd
{
    class GLWidget: public QGLWidget
    {
        Q_OBJECT

    private:

        // variables defining the projection matrix
        double       _aspect;            // aspect ratio of the rendering window
        double       _fovy;              // field of view in direction y
        double       _z_near, _z_far;    // distance of near and far clipping planes

        // variables defining the model-view matrix
        double       _eye[3], _center[3], _up[3];

        // variables needed by transformations
        int         _angle_x, _angle_y, _angle_z;
        double      _zoom;
        double      _trans_x, _trans_y, _trans_z;

        int _geometry_type = 1;

        // biquartic composite curve
        BiquarticCompositeCurve3 *_bcc;
        int                       _arc_ind = 0;
        int                       _data_point_ind = 0;

        // join and merge
        int _op_ind1 = 0;
        int _op_ind2 = 0;
        BiquarticCompositeCurve3::Direction _op_dir1 = BiquarticCompositeCurve3::LEFT;
        BiquarticCompositeCurve3::Direction _op_dir2 = BiquarticCompositeCurve3::LEFT;

        // render derivatives
        int _deriv1 = 0;
        int _deriv2 = 0;

        // continue
        BiquarticCompositeCurve3::Direction _cont_dir = BiquarticCompositeCurve3::LEFT;

        // render data points
        int _render_data = 1;

        // biquartic composite surface
        BiquarticCompositeSurface3 *_bcs;
        int _patch_ind = 0;
        int _data_point_row_ind = 0;
        int _data_point_col_ind = 0;

        int _render_control_net = 1;
        int _render_iso = 0;
        int _render_iso_deriv = 0;
        int _render_normals = 0;

        // join
        int _join_patch_ind1 = 0;
        int _join_patch_ind2 = 0;
        BiquarticCompositeSurface3::Direction _join_patch_dir1 = BiquarticCompositeSurface3::N;
        BiquarticCompositeSurface3::Direction _join_patch_dir2 = BiquarticCompositeSurface3::N;

        // merge
        int _merge_patch_ind1 = 0;
        int _merge_patch_ind2 = 0;
        BiquarticCompositeSurface3::Direction _merge_patch_dir1 = BiquarticCompositeSurface3::N;
        BiquarticCompositeSurface3::Direction _merge_patch_dir2 = BiquarticCompositeSurface3::N;

        // continue
        BiquarticCompositeSurface3::Direction _cont_patch_dir = BiquarticCompositeSurface3::N;

        // shaders
        ShaderProgram _shaders[4];

        // texture
        int _use_texture = 0;
        GLuint _texName = 0;

        // light types
        int _light_index = 0;

        DirectionalLight *_dl;
        PointLight *_pl;
        SpotLight *_sl;

        GLboolean dlOn = GL_TRUE;
        GLboolean plOn = GL_FALSE;
        GLboolean slOn = GL_FALSE;

        int _dl_intensity_ind = 0;
        int _pl_intensity_ind = 0;
        int _sl_intensity_ind = 0;

        // mouse events
        bool pointSelected = false;
        GLdouble _reposition_unit = 0.05;

        // action previews
        bool _preview = false;
        bool _successful_operation = false;
        int _last_operation = -1;
        bool _last_op_finalized = false;

    public:
        // special and default constructor
        // the format specifies the properties of the rendering window
        GLWidget(QWidget* parent = 0, const QGLFormat& format = QGL::Rgba | QGL::DepthBuffer | QGL::DoubleBuffer);
        ~GLWidget();

        // redeclared virtual functions
        void initializeGL() override;
        void paintGL() override;
        void resizeGL(int w, int h) override;

        void save();
        void open();

    public slots:
        // public event handling methods/slots
        void set_angle_x(int value);
        void set_angle_y(int value);
        void set_angle_z(int value);

        void set_zoom_factor(double value);

        void set_trans_x(double value);
        void set_trans_y(double value);
        void set_trans_z(double value);

        void set_arc_index(int index);

        void set_patch_data_point_x(double val);
        void set_patch_data_point_y(double val);
        void set_patch_data_point_z(double val);

        void set_operation_ind1(int val);
        void set_operation_ind2(int val);
        void set_operation_dir1(int val);
        void set_operation_dir2(int val);

        void handle_join();

        void add_new_arc();
        void delete_arc();

        void set_deriv1(int ch);
        void set_deriv2(int ch);

        void handle_merge();

        void set_continue_dir(int val);
        void handle_continue();

        void set_render_data(int ch);

        void set_color_r(int);
        void set_color_g(int);
        void set_color_b(int);

        void set_geometry_type(int);

        void set_render_control_net(int ch);
        void set_render_iso(int ch);
        void set_render_iso_deriv(int ch);
        void set_render_normals(int ch);

        void set_patch_row_index(int val);
        void set_patch_col_index(int val);

        void set_material(int val);
        void set_patch_index(int val);

        void add_new_patch();
        void delete_patch();

        void set_merge_patch_ind1(int val);
        void set_merge_patch_ind2(int val);
        void set_merge_patch_dir1(int val);
        void set_merge_patch_dir2(int val);
        void handle_patch_merge();

        void set_join_patch_ind1(int val);
        void set_join_patch_ind2(int val);
        void set_join_patch_dir1(int val);
        void set_join_patch_dir2(int val);
        void handle_patch_join();

        void set_continue_patch_dir(int val);
        void handle_continue_patch();

        void move_up();
        void move_left();
        void move_down();
        void move_right();

        void set_shader(int);
        void set_light(int);
        void set_iso_count_u(int);
        void set_iso_count_v(int);

        // texture
        void set_tex_checked(int);
        void change_tex_img();

        // lights

        // directional light
        void set_dl_intensity_ind(int);

        void set_dl_position_x(double);
        void set_dl_position_y(double);
        void set_dl_position_z(double);

        void set_dl_r(double);
        void set_dl_g(double);
        void set_dl_b(double);
        void set_dl_a(double);

        // point light
        void set_pl_intensity_ind(int);

        void set_pl_position_x(double);
        void set_pl_position_y(double);
        void set_pl_position_z(double);
        void set_pl_position_w(double);

        void set_pl_r(double);
        void set_pl_g(double);
        void set_pl_b(double);
        void set_pl_a(double);

        void set_pl_constant_attenuation(double);
        void set_pl_linear_attenuation(double);
        void set_pl_quadratic_attenuation(double);

        // spot light
        void set_sl_intensity_ind(int);

        void set_sl_position_x(double);
        void set_sl_position_y(double);
        void set_sl_position_z(double);
        void set_sl_position_w(double);

        void set_sl_r(double);
        void set_sl_g(double);
        void set_sl_b(double);
        void set_sl_a(double);

        void set_sl_constant_attenuation(double);
        void set_sl_linear_attenuation(double);
        void set_sl_quadratic_attenuation(double);

        void set_sl_spot_cutoff(double);
        void set_sl_spot_exponent(double);

        void set_sl_spot_dir_x(double);
        void set_sl_spot_dir_y(double);
        void set_sl_spot_dir_z(double);

        // shader attributes
        void set_def_toon_color_r(int value);
        void set_def_toon_color_g(int value);
        void set_def_toon_color_b(int value);
        void set_def_toon_color_a(int value);

        void set_rl_scale(double value);
        void set_rl_smooth(double value);
        void set_rl_shade(double value);

        void set_alpha_checked(int ch);

        // arc operation previews
        void continuePreview();
        void joinPreview();
        void mergePreview();
        void unsetPreview();

        // patch operation previews
        void continuePatchPreview();

     signals:

        void color_has_changed_r(int);
        void color_has_changed_g(int);
        void color_has_changed_b(int);

        void material_changed(int);

        void patch_changed(int);
        void row_changed(int);
        void col_changed(int);

        void arc_changed(int);
        void point_changed(int);

        // signals for directional light (for intensities - r, g, b, a)
        void dl_light_r_changed(double);
        void dl_light_g_changed(double);
        void dl_light_b_changed(double);
        void dl_light_a_changed(double);

        // signals for point light (for intensities - r, g, b, a)
        void pl_light_r_changed(double);
        void pl_light_g_changed(double);
        void pl_light_b_changed(double);
        void pl_light_a_changed(double);

        // signals for spot light (for intensities - r, g, b, a)
        void sl_light_r_changed(double);
        void sl_light_g_changed(double);
        void sl_light_b_changed(double);
        void sl_light_a_changed(double);

        // invalid operation signals
        void arcInvalidContinue();
        void arcInvalidJoin();
        void arcInvalidMerge();

        void patchInvalidContinue();

     private:

        void mousePressEvent(QMouseEvent *event) override;
        void wheelEvent(QWheelEvent *event) override;

        void _createBiquarticCompositeCurve();
        void _renderBiquarticCompositeCurve();
        void _destroyBiquarticCompositeCurve();

        void _createBiquarticCompositeSurface();
        void _renderBiquarticCompositeSurface();
        void _destroyBiquarticCompositeSurface();

        int _getMaterialIndex(Material* mat);
        BiquarticCompositeSurface3::Direction _getPatchDirection(int index);
        void _installShaders();
        void _initializeLights();
        void _destroyLights();
        bool _initializeTexture(const char* fname);
    };
}
