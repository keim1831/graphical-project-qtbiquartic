#include "GLWidget.h"

#if !defined(__APPLE__)
#include <GL/glu.h>
#endif

#include <QFileDialog>
#include <QOpenGLTexture>
#include <QMouseEvent>
#include <iostream>
using namespace std;

#include <Core/Exceptions.h>

namespace cagd
{
    //--------------------------------
    // special and default constructor
    //--------------------------------
    GLWidget::GLWidget(QWidget *parent, const QGLFormat &format): QGLWidget(format, parent)
    {
    }

    GLWidget::~GLWidget()
    {
        _destroyBiquarticCompositeCurve();
        _destroyBiquarticCompositeSurface();
        _destroyLights();
    }

    //--------------------------------------------------------------------------------------
    // this virtual function is called once before the first call to paintGL() or resizeGL()
    //--------------------------------------------------------------------------------------
    void GLWidget::initializeGL()
    {
        // creating a perspective projection matrix
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = (double)width() / (double)height();
        _z_near = 1.0;
        _z_far  = 1000.0;
        _fovy   = 45.0;

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

        // setting the model view matrix
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        _eye[0] = _eye[1] = 0.0; _eye[2] = 6.0;
        _center[0] = _center[1] = _center[2] = 0.0;
        _up[0] = _up[2] = 0.0; _up[1] = 1.0;

        gluLookAt(_eye[0], _eye[1], _eye[2], _center[0], _center[1], _center[2], _up[0], _up[1], _up[2]);

        // enabling the depth test
        glEnable(GL_DEPTH_TEST);

        // setting the background color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        // initial values of transformation parameters
        _angle_x = _angle_y = _angle_z = 0.0;
        _trans_x = _trans_y = _trans_z = 0.0;
        _zoom = 1.0;

        // ...

        try
        {
            // initializing the OpenGL Extension Wrangler library
            GLenum error = glewInit();

            if (error != GLEW_OK)
            {
                throw Exception("Could not initialize the OpenGL Extension Wrangler Library!");
            }

            if (!glewIsSupported("GL_VERSION_2_0"))
            {
                throw Exception("Your graphics card is not compatible with OpenGL 2.0+! "
                                "Try to update your driver or buy a new graphics adapter!");
            }

            // create and store your geometry in display lists or vertex buffer objects
            // ...
            _createBiquarticCompositeCurve();
            _installShaders();
            _createBiquarticCompositeSurface();
            _initializeLights();
        }
        catch (Exception &e)
        {
            e.show();
        }
    }

    //-----------------------
    // the rendering function
    //-----------------------
    void GLWidget::paintGL()
    {
        // clears the color and depth buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // stores/duplicates the original model view matrix
        glPushMatrix();

            // applying transformations
            glRotatef(_angle_x, 1.0, 0.0, 0.0);
            glRotatef(_angle_y, 0.0, 1.0, 0.0);
            glRotatef(_angle_z, 0.0, 0.0, 1.0);
            glTranslated(_trans_x, _trans_y, _trans_z);
            glScaled(_zoom, _zoom, _zoom);

            // render your geometry (this is oldest OpenGL rendering technique, later we will use some advanced methods)
            if (_geometry_type == 0)
                _renderBiquarticCompositeCurve();
            else
            {
                if (_use_texture)
                {
                    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
                    glBindTexture(GL_TEXTURE_2D, _texName);
                }
                _renderBiquarticCompositeSurface();
            }

        // pops the current matrix stack, replacing the current matrix with the one below it on the stack,
        // i.e., the original model view matrix is restored
        glPopMatrix();
    }

    //----------------------------------------------------------------------------
    // when the main window is resized one needs to redefine the projection matrix
    //----------------------------------------------------------------------------
    void GLWidget::resizeGL(int w, int h)
    {
        // setting the new size of the rendering context
        glViewport(0, 0, w, h);

        // redefining the projection matrix
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = (double)w / (double)h;

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

        // switching back to the model view matrix
        glMatrixMode(GL_MODELVIEW);

        updateGL();
    }

    //-----------------------------------
    // implementation of the public slots
    //-----------------------------------

    void GLWidget::set_angle_x(int value)
    {
        if (_angle_x != value)
        {
            _angle_x = value;
            updateGL();
        }
    }

    void GLWidget::set_angle_y(int value)
    {
        if (_angle_y != value)
        {
            _angle_y = value;
            updateGL();
        }
    }

    void GLWidget::set_angle_z(int value)
    {
        if (_angle_z != value)
        {
            _angle_z = value;
            updateGL();
        }
    }

    void GLWidget::set_zoom_factor(double value)
    {
        if (_zoom != value)
        {
            _zoom = value;
            updateGL();
        }
    }

    void GLWidget::set_trans_x(double value)
    {
        if (_trans_x != value)
        {
            _trans_x = value;
            updateGL();
        }
    }

    void GLWidget::set_trans_y(double value)
    {
        if (_trans_y != value)
        {
            _trans_y = value;
            updateGL();
        }
    }

    void GLWidget::set_trans_z(double value)
    {
        if (_trans_z != value)
        {
            _trans_z = value;
            updateGL();
        }
    }

    void GLWidget::set_operation_ind1(int val)
    {
        _op_ind1 = val;
    }

    void GLWidget::set_operation_ind2(int val)
    {
        _op_ind2 = val;
    }

    void GLWidget::set_operation_dir1(int index)
    {
        if (index == 0)
            _op_dir1 = BiquarticCompositeCurve3::LEFT;
        else
            _op_dir1 = BiquarticCompositeCurve3::RIGHT;
    }

    void GLWidget::set_operation_dir2(int index)
    {
        if (index == 0)
            _op_dir2 = BiquarticCompositeCurve3::LEFT;
        else
            _op_dir2 = BiquarticCompositeCurve3::RIGHT;
    }

    void GLWidget::set_material(int val)
    {
        Material *mat;

        switch(val)
        {
        case 1:
            mat = &MatFBGold;
            break;
        case 2:
            mat = &MatFBSilver;
            break;
        case 3:
            mat = &MatFBEmerald;
            break;
        case 4:
            mat = &MatFBPearl;
            break;
        case 5:
            mat = &MatFBTurquoise;
            break;
        default:
            mat = &MatFBBrass;
            break;
        }

        _bcs->setMaterial(_patch_ind, mat);
        updateGL();
    }

    void GLWidget::set_shader(int val)
    {
        _bcs->setShader(val - 1);
        updateGL();
    }

    void GLWidget::set_light(int val)
    {
        switch (val) {
            case 0:
                dlOn = GL_TRUE;
                slOn = GL_FALSE;
                plOn = GL_FALSE;
                updateGL();
                break;
            case 1:
                dlOn = GL_FALSE;
                slOn = GL_FALSE;
                plOn = GL_TRUE;
                updateGL();
                break;
            case 2:
                dlOn = GL_FALSE;
                slOn = GL_TRUE;
                plOn = GL_FALSE;
                updateGL();
                break;
            default:
                break;
        }
    }

    void GLWidget::set_arc_index(int index)
    {
        if (_arc_ind != index)
        {
            _arc_ind = index;

            GLdouble r = 0.0, g = 0.0, b = 0.0;
            _bcc->getColorComponents(_arc_ind, r, g, b);

            emit color_has_changed_r(r * 100);
            emit color_has_changed_g(g * 100);
            emit color_has_changed_b(b * 100);

            updateGL();
        }
    }

    void GLWidget::set_patch_data_point_x(double val)
    {
         _bcs->addToDataPointValue(_patch_ind, _data_point_row_ind, _data_point_col_ind, 0, val);
         updateGL();
    }

    void GLWidget::set_patch_data_point_y(double val)
    {
         _bcs->addToDataPointValue(_patch_ind, _data_point_row_ind, _data_point_col_ind, 1, val);
         updateGL();
    }

    void GLWidget::set_patch_data_point_z(double val)
    {
         _bcs->addToDataPointValue(_patch_ind, _data_point_row_ind, _data_point_col_ind, 2, val);
         updateGL();
    }

    void GLWidget::set_patch_row_index(int val)
    {
        _data_point_row_ind = val;
    }

    void GLWidget::set_patch_col_index(int val)
    {
        _data_point_col_ind = val;
    }

    void GLWidget::add_new_arc()
    {
        _bcc->insertNewIsolatedArc();

        updateGL();
    }

    void GLWidget::delete_arc()
    {
        if (_bcc->deleteArc(_arc_ind))
        {
            pointSelected = false;

            _arc_ind = -1;
            emit arc_changed(-1);

            emit color_has_changed_r(0);
            emit color_has_changed_g(0);
            emit color_has_changed_b(0);

            updateGL();
        }
    }

    void GLWidget::handle_join()
    {
        if (_successful_operation)
        {
            _bcc->joinExistingArcs();
            _last_op_finalized = true;
            _preview = false;

            _successful_operation = false;
            emit arcInvalidJoin();
            updateGL();
        }
    }

    void GLWidget::handle_merge()
    {
        if (_successful_operation)
        {
            _bcc->mergeExistingArcs();
            _last_op_finalized = true;
            _preview = false;

            _successful_operation = false;
            emit arcInvalidMerge();
            updateGL();
        }
    }

    void GLWidget::set_deriv1(int ch)
    {
        if(_deriv1 != ch)
        {
            _deriv1 = ch;
            updateGL();
        }
    }

    void GLWidget::set_deriv2(int ch)
    {
        if(_deriv2 != ch)
        {
            _deriv2 = ch;
            updateGL();
        }
    }

    void GLWidget::set_continue_dir(int index)
    {
        if (index == 0)
            _cont_dir = BiquarticCompositeCurve3::LEFT;
        else
            _cont_dir = BiquarticCompositeCurve3::RIGHT;
    }

    void GLWidget::handle_continue()
    {
        if (_successful_operation)
        {
            _bcc->continueExistingArc();
            _last_op_finalized = true;
            _preview = false;
            _successful_operation = false;
            emit arcInvalidContinue();
            updateGL();
        }
    }

    void GLWidget::set_render_data(int ch)
    {
        if(_render_data != ch)
        {
            _render_data = ch;
            updateGL();
        }
    }

    void GLWidget::set_render_control_net(int ch)
    {
        if(_render_control_net != ch)
        {
            _render_control_net = ch;
            updateGL();
        }
    }

    void GLWidget::set_render_iso(int ch)
    {
        if(_render_iso != ch)
        {
            _render_iso = ch;
            updateGL();
        }
    }

    void GLWidget::set_render_iso_deriv(int ch)
    {
        if(_render_iso_deriv != ch)
        {
            _render_iso_deriv = ch;
            updateGL();
        }
    }

    void GLWidget::set_render_normals(int ch)
    {
        if(_render_normals != ch)
        {
            _render_normals = ch;
            updateGL();
        }
    }

    void GLWidget::set_color_r(int val)
    {
        _bcc->changeColorComponentValue(_arc_ind, 0, val / 100.0);
        updateGL();
    }

    void GLWidget::set_color_g(int val)
    {
        _bcc->changeColorComponentValue(_arc_ind, 1, val / 100.0);
        updateGL();
    }

    void GLWidget::set_color_b(int val)
    {
        _bcc->changeColorComponentValue(_arc_ind, 2, val / 100.0);
        updateGL();
    }

    void GLWidget::set_geometry_type(int val)
    {
        if (_geometry_type != val)
        {
            _geometry_type = val;
            updateGL();
        }
    }

    void GLWidget::set_patch_index(int index)
    {
        if (_patch_ind != index)
        {
            _patch_ind = index;

            Material* mat = _bcs->getMaterial(_patch_ind);
            int ind = _getMaterialIndex(mat);
            emit material_changed(ind);

            updateGL();
        }
    }

    void GLWidget::add_new_patch()
    {
        _bcs->insertNewIsolatedPatch();
        updateGL();
    }

    void GLWidget::delete_patch()
    {
        if (_bcs->deletePatch(_patch_ind))
        {
            Material* mat = _bcs->getMaterial(_patch_ind);
            int ind = _getMaterialIndex(mat);
            emit material_changed(ind);

            updateGL();
        }
    }

    void GLWidget::handle_patch_merge()
    {
        if (_bcs->mergeExistingPatches(_merge_patch_ind1, _merge_patch_dir1, _merge_patch_ind2, _merge_patch_dir2))
        {
            updateGL();
        }
    }

    void GLWidget::set_merge_patch_ind1(int val)
    {
        _merge_patch_ind1 = val;
    }

    void GLWidget::set_merge_patch_ind2(int val)
    {
        _merge_patch_ind2 = val;
    }

    void GLWidget::set_merge_patch_dir1(int index)
    {
        _merge_patch_dir1 = _getPatchDirection(index);
    }

    void GLWidget::set_merge_patch_dir2(int index)
    {
        _merge_patch_dir2 = _getPatchDirection(index);
    }

    // join
    void GLWidget::handle_patch_join()
    {
        if (_bcs->joinExistingPatches(_join_patch_ind1, _join_patch_dir1, _join_patch_ind2, _join_patch_dir2))
        {
            Material* mat = _bcs->getMaterial(_patch_ind);
            int ind = _getMaterialIndex(mat);
            emit material_changed(ind);

            updateGL();
        }
    }

    void GLWidget::set_join_patch_ind1(int val)
    {
        _join_patch_ind1 = val;
    }

    void GLWidget::set_join_patch_ind2(int val)
    {
        _join_patch_ind2 = val;
    }

    void GLWidget::set_join_patch_dir1(int index)
    {
        _join_patch_dir1 = _getPatchDirection(index);
    }

    void GLWidget::set_join_patch_dir2(int index)
    {
        _join_patch_dir2 = _getPatchDirection(index);
    }

    // continue
    void GLWidget::handle_continue_patch()
    {
        if (_successful_operation)
        {
            _bcs->continueExistingPatch();
            _last_op_finalized = true;
            _preview = false;
            _successful_operation = false;
            emit patchInvalidContinue();
            updateGL();
        }
    }

    void GLWidget::set_continue_patch_dir(int index)
    {
        _cont_patch_dir = _getPatchDirection(index);
    }

    // move
    void GLWidget::move_up()
    {
        _bcs->moveSelectedPatch(_patch_ind, BiquarticCompositeSurface3::N);
        updateGL();
    }
    void GLWidget::move_left()
    {
        _bcs->moveSelectedPatch(_patch_ind, BiquarticCompositeSurface3::W);
        updateGL();
    }

    void GLWidget::move_down()
    {
        _bcs->moveSelectedPatch(_patch_ind, BiquarticCompositeSurface3::S);
        updateGL();
    }

    void GLWidget::move_right()
    {
        _bcs->moveSelectedPatch(_patch_ind, BiquarticCompositeSurface3::E);
        updateGL();
    }

    void GLWidget::set_iso_count_u(int count)
    {
        if(_bcs->setIsoLineCountU(count))
        {
            updateGL();
        }
    }

    void GLWidget::set_iso_count_v(int count)
    {
        if(_bcs->setIsoLineCountV(count))
        {
            updateGL();
        }
    }

    void GLWidget::set_tex_checked(int val)
    {
        if (_use_texture != val)
        {
            _use_texture = val;
            updateGL();
        }
    }

    void GLWidget::change_tex_img()
    {
        QString fileName = QFileDialog::getOpenFileName(this,
               tr("Load Texture image"), "",
               tr("Image Files (*.png *.PNG *.jpg *.JPG *.jpeg *.bmp)"));
        if (!fileName.isEmpty())
        {
            if (_initializeTexture(fileName.toStdString().c_str()) && _use_texture)
                updateGL();
        }
    }

    // lights
    void GLWidget::GLWidget::set_dl_position_x(double val)
    {
        if (_dl->set_position_x(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_dl_position_y(double val)
    {
        if (_dl->set_position_y(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_dl_position_z(double val)
    {
        if (_dl->set_position_z(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_dl_r(double val)
    {
        switch (_dl_intensity_ind)
        {
        case 0:
            if (_dl->set_ambient_r(val))
            {
                updateGL();
            }
            break;

        case 1:
            if (_dl->set_diffuse_r(val))
            {
                updateGL();
            }
            break;

        case 2:
            if (_dl->set_specular_r(val))
            {
                updateGL();
            }
            break;
        }
    }

    void GLWidget::set_dl_g(double val)
    {
        switch (_dl_intensity_ind)
        {
        case 0:
            if (_dl->set_ambient_g(val))
            {
                updateGL();
            }
            break;

        case 1:
            if (_dl->set_diffuse_g(val))
            {
                updateGL();
            }
            break;

        case 2:
            if (_dl->set_specular_g(val))
            {
                updateGL();
            }
            break;
        }
    }

    void GLWidget::set_dl_b(double val)
    {
        switch (_dl_intensity_ind)
        {
        case 0:
            if (_dl->set_ambient_b(val))
            {
                updateGL();
            }
            break;

        case 1:
            if (_dl->set_diffuse_b(val))
            {
                updateGL();
            }
            break;

        case 2:
            if (_dl->set_specular_b(val))
            {
                updateGL();
            }
            break;
        }
    }

    void GLWidget::set_dl_a(double val)
    {
        switch (_dl_intensity_ind)
        {
        case 0:
            if (_dl->set_ambient_a(val))
            {
                updateGL();
            }
            break;

        case 1:
            if (_dl->set_diffuse_a(val))
            {
                updateGL();
            }
            break;

        case 2:
            if (_dl->set_specular_a(val))
            {
                updateGL();
            }
            break;
        }
    }

    void GLWidget::set_dl_intensity_ind(int val)
    {
        if (val != _dl_intensity_ind)
        {
            _dl_intensity_ind = val;

            switch(_dl_intensity_ind)
            {
            case 0:
                emit dl_light_r_changed(_dl->get_ambient_r());
                emit dl_light_g_changed(_dl->get_ambient_g());
                emit dl_light_b_changed(_dl->get_ambient_b());
                emit dl_light_a_changed(_dl->get_ambient_a());
                break;
            case 1:
                emit dl_light_r_changed(_dl->get_diffuse_r());
                emit dl_light_g_changed(_dl->get_diffuse_g());
                emit dl_light_b_changed(_dl->get_diffuse_b());
                emit dl_light_a_changed(_dl->get_diffuse_a());
                break;
            case 2:
                emit dl_light_r_changed(_dl->get_specular_r());
                emit dl_light_g_changed(_dl->get_specular_g());
                emit dl_light_b_changed(_dl->get_specular_b());
                emit dl_light_a_changed(_dl->get_specular_a());
                break;
            }
        }
    }

    // point light
    void GLWidget::GLWidget::set_pl_position_x(double val)
    {
        if (_pl->set_position_x(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_pl_position_y(double val)
    {
        if (_pl->set_position_y(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_pl_position_z(double val)
    {
        if (_pl->set_position_z(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_pl_position_w(double val)
    {
        if (_pl->set_position_w(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_pl_intensity_ind(int val)
    {
        if (val != _pl_intensity_ind)
        {
            _pl_intensity_ind = val;

            switch(_pl_intensity_ind)
            {
            case 0:
                emit pl_light_r_changed(_pl->get_ambient_r());
                emit pl_light_g_changed(_pl->get_ambient_g());
                emit pl_light_b_changed(_pl->get_ambient_b());
                emit pl_light_a_changed(_pl->get_ambient_a());
                break;
            case 1:
                emit pl_light_r_changed(_pl->get_diffuse_r());
                emit pl_light_g_changed(_pl->get_diffuse_g());
                emit pl_light_b_changed(_pl->get_diffuse_b());
                emit pl_light_a_changed(_pl->get_diffuse_a());
                break;
            case 2:
                emit pl_light_r_changed(_pl->get_specular_r());
                emit pl_light_g_changed(_pl->get_specular_g());
                emit pl_light_b_changed(_pl->get_specular_b());
                emit pl_light_a_changed(_pl->get_specular_a());
                break;
            }
        }
    }

    void GLWidget::set_pl_r(double val)
    {
        switch (_pl_intensity_ind)
        {
        case 0:
            if (_pl->set_ambient_r(val))
            {
                updateGL();
            }
            break;

        case 1:
            if (_pl->set_diffuse_r(val))
            {
                updateGL();
            }
            break;

        case 2:
            if (_pl->set_specular_r(val))
            {
                updateGL();
            }
            break;
        }
    }

    void GLWidget::set_pl_g(double val)
    {
        switch (_pl_intensity_ind)
        {
        case 0:
            if (_pl->set_ambient_g(val))
            {
                updateGL();
            }
            break;

        case 1:
            if (_pl->set_diffuse_g(val))
            {
                updateGL();
            }
            break;

        case 2:
            if (_pl->set_specular_g(val))
            {
                updateGL();
            }
            break;
        }
    }

    void GLWidget::set_pl_b(double val)
    {
        switch (_pl_intensity_ind)
        {
        case 0:
            if (_pl->set_ambient_b(val))
            {
                updateGL();
            }
            break;

        case 1:
            if (_pl->set_diffuse_b(val))
            {
                updateGL();
            }
            break;

        case 2:
            if (_pl->set_specular_b(val))
            {
                updateGL();
            }
            break;
        }
    }

    void GLWidget::set_pl_a(double val)
    {
        switch (_pl_intensity_ind)
        {
        case 0:
            if (_pl->set_ambient_a(val))
            {
                updateGL();
            }
            break;

        case 1:
            if (_pl->set_diffuse_a(val))
            {
                updateGL();
            }
            break;

        case 2:
            if (_pl->set_specular_a(val))
            {
                updateGL();
            }
            break;
        }
    }

    void GLWidget::set_pl_constant_attenuation(double val)
    {
        if (_pl->set_constant_attenuation(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_pl_linear_attenuation(double val)
    {
        if (_pl->set_linear_attenuation(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_pl_quadratic_attenuation(double val)
    {
        if (_pl->set_quadratic_attenuation(val))
        {
            updateGL();
        }
    }


    // spot light
    void GLWidget::GLWidget::set_sl_position_x(double val)
    {
        if (_sl->set_position_x(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_sl_position_y(double val)
    {
        if (_sl->set_position_y(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_sl_position_z(double val)
    {
        if (_sl->set_position_z(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_sl_position_w(double val)
    {
        if (_sl->set_position_w(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_sl_intensity_ind(int val)
    {
        if (val != _sl_intensity_ind)
        {
            _sl_intensity_ind = val;

            switch(_sl_intensity_ind)
            {
            case 0:
                emit sl_light_r_changed(_sl->get_ambient_r());
                emit sl_light_g_changed(_sl->get_ambient_g());
                emit sl_light_b_changed(_sl->get_ambient_b());
                emit sl_light_a_changed(_sl->get_ambient_a());
                break;
            case 1:
                emit sl_light_r_changed(_sl->get_diffuse_r());
                emit sl_light_g_changed(_sl->get_diffuse_g());
                emit sl_light_b_changed(_sl->get_diffuse_b());
                emit sl_light_a_changed(_sl->get_diffuse_a());
                break;
            case 2:
                emit sl_light_r_changed(_sl->get_specular_r());
                emit sl_light_g_changed(_sl->get_specular_g());
                emit sl_light_b_changed(_sl->get_specular_b());
                emit sl_light_a_changed(_sl->get_specular_a());
                break;
            }
        }
    }

    void GLWidget::set_sl_r(double val)
    {
        switch (_sl_intensity_ind)
        {
        case 0:
            if (_sl->set_ambient_r(val))
            {
                updateGL();
            }
            break;

        case 1:
            if (_sl->set_diffuse_r(val))
            {
                updateGL();
            }
            break;

        case 2:
            if (_sl->set_specular_r(val))
            {
                updateGL();
            }
            break;
        }
    }

    void GLWidget::set_sl_g(double val)
    {
        switch (_sl_intensity_ind)
        {
        case 0:
            if (_sl->set_ambient_g(val))
            {
                updateGL();
            }
            break;

        case 1:
            if (_sl->set_diffuse_g(val))
            {
                updateGL();
            }
            break;

        case 2:
            if (_sl->set_specular_g(val))
            {
                updateGL();
            }
            break;
        }
    }

    void GLWidget::set_sl_b(double val)
    {
        switch (_sl_intensity_ind)
        {
        case 0:
            if (_sl->set_ambient_b(val))
            {
                updateGL();
            }
            break;

        case 1:
            if (_sl->set_diffuse_b(val))
            {
                updateGL();
            }
            break;

        case 2:
            if (_sl->set_specular_b(val))
            {
                updateGL();
            }
            break;
        }
    }

    void GLWidget::set_sl_a(double val)
    {
        switch (_sl_intensity_ind)
        {
        case 0:
            if (_sl->set_ambient_a(val))
            {
                updateGL();
            }
            break;

        case 1:
            if (_sl->set_diffuse_a(val))
            {
                updateGL();
            }
            break;

        case 2:
            if (_sl->set_specular_a(val))
            {
                updateGL();
            }
            break;
        }
    }

    void GLWidget::set_sl_constant_attenuation(double val)
    {
        if (_sl->set_constant_attenuation(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_sl_linear_attenuation(double val)
    {
        if (_sl->set_linear_attenuation(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_sl_quadratic_attenuation(double val)
    {
        if (_sl->set_quadratic_attenuation(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_sl_spot_cutoff(double val)
    {
        if (_sl->set_spot_cutoff(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_sl_spot_exponent(double val)
    {
        if (_sl->set_spot_exponent(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_sl_spot_dir_x(double val)
    {
        if (_sl->set_spot_direction_x(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_sl_spot_dir_y(double val)
    {
        if (_sl->set_spot_direction_y(val))
        {
            updateGL();
        }
    }

    void GLWidget::set_sl_spot_dir_z(double val)
    {
        if (_sl->set_spot_direction_z(val))
        {
            updateGL();
        }
    }

    // shader attributes
    void GLWidget::set_def_toon_color_r(int value)
    {
        if (_bcs->set_def_toon_color_r((GLfloat) value / 100.0f))
        {
            updateGL();
        }
    }

    void GLWidget::set_def_toon_color_g(int value)
    {
        if (_bcs->set_def_toon_color_g((GLfloat) value / 100.0f))
        {
            updateGL();
        }
    }

    void GLWidget::set_def_toon_color_b(int value)
    {
        if (_bcs->set_def_toon_color_b((GLfloat) value / 100.0f))
        {
            updateGL();
        }
    }

    void GLWidget::set_def_toon_color_a(int value)
    {
        if (_bcs->set_def_toon_color_a((GLfloat) value / 100.0f))
        {
            updateGL();
        }
    }

    void GLWidget::set_rl_scale(double value)
    {
        if (_bcs->set_rl_scale(value))
        {
            updateGL();
        }
    }

    void GLWidget::set_rl_smooth(double value)
    {
        if (_bcs->set_rl_smooth(value))
        {
            updateGL();
        }
    }

    void GLWidget::set_rl_shade(double value)
    {
        if (_bcs->set_rl_shade(value))
        {
            updateGL();
        }
    }

    void GLWidget::set_alpha_checked(int ch)
    {
        if (_bcs->set_alpha_checked(ch))
        {
            updateGL();
        }
    }

    void GLWidget::continuePreview()
    {
        if (_bcc->continueExistingArcPreview(_arc_ind, _cont_dir))
        {
            _successful_operation = true;
            _preview = true;
            _last_operation = 0;
            _last_op_finalized = false;
        }
        else
        {
            _successful_operation = false;
            _preview = false;
            _last_operation = -1;
            emit arcInvalidContinue();
        }
        updateGL();
    }

    void GLWidget::continuePatchPreview()
    {
        if (_bcs->continueExistingPatchPreview(_patch_ind, _cont_patch_dir))
        {
            _successful_operation = true;
            _preview = true;
            _last_operation = 0;
            _last_op_finalized = false;
        }
        else
        {
            _successful_operation = false;
            _preview = false;
            _last_operation = -1;
            emit patchInvalidContinue();
        }
        updateGL();
    }

    void GLWidget::joinPreview()
    {
        if (_bcc->joinExistingArcsPreview(_op_ind1, _op_dir1, _op_ind2, _op_dir2))
        {
            _successful_operation = true;
            _preview = true;
            _last_operation = 1;
            _last_op_finalized = false;
        }
        else
        {
            _successful_operation = false;
            _preview = false;
            _last_operation = -1;
            emit arcInvalidJoin();
        }
        updateGL();
    }

    void GLWidget::mergePreview()
    {
        if (_bcc->mergeExistingArcsPreview(_op_ind1, _op_dir1, _op_ind2, _op_dir2))
        {
            _successful_operation = true;
            _preview = true;
            _last_operation = 2;
            _last_op_finalized = false;
        }
        else
        {
            _successful_operation = false;
            _preview = false;
            _last_operation = -1;
            emit arcInvalidMerge();
        }
        updateGL();
    }

    void GLWidget::unsetPreview()
    {
        if (!_last_op_finalized)
        {
            if (_geometry_type == 0)
            {
                _bcc->cancelOperation(_last_operation);
            }
            else
            {
                _bcs->cancelOperation(_last_operation);
            }
        }

        _preview = false;
        _successful_operation = false;
        updateGL();
    }

    // other functions
    BiquarticCompositeSurface3::Direction GLWidget::_getPatchDirection(int index)
    {
        switch (index)
        {
        case 0:
            return BiquarticCompositeSurface3::N;
        case 1:
            return BiquarticCompositeSurface3::NW;
        case 2:
            return BiquarticCompositeSurface3::W;
        case 3:
            return BiquarticCompositeSurface3::SW;
        case 4:
            return BiquarticCompositeSurface3::S;
        case 5:
            return BiquarticCompositeSurface3::SE;
        case 6:
            return BiquarticCompositeSurface3::E;
        case 7:
            return BiquarticCompositeSurface3::NE;
        default:
            return BiquarticCompositeSurface3::N;
        }
    }

    void GLWidget::_createBiquarticCompositeCurve()
    {
        _bcc = new BiquarticCompositeCurve3();

        double r, g, b;
        _bcc->getColorComponents(_arc_ind, r, g, b);
        emit color_has_changed_r(r * 100);
        emit color_has_changed_g(g * 100);
        emit color_has_changed_b(b * 100);
    }

    void GLWidget::_renderBiquarticCompositeCurve()
    {
        _bcc->renderAllArcs(_deriv1, _deriv2, _arc_ind, _render_data, _data_point_ind, pointSelected, _preview);
    }

    void GLWidget::_destroyBiquarticCompositeCurve()
    {
        if (_bcc)
        {
            delete _bcc;
            _bcc = nullptr;
        }
    }

    void GLWidget::_createBiquarticCompositeSurface()
    {
        _bcs = new BiquarticCompositeSurface3(_shaders, 100);
    }

    void GLWidget::_renderBiquarticCompositeSurface()
    {
        if(dlOn)
            _dl->Enable();
        else if(slOn)
            _sl->Enable();
        else if(plOn)
            _pl->Enable();


        _bcs->renderAllPatches(_patch_ind, _render_control_net, _render_iso, _render_iso_deriv, _render_normals,
                               _data_point_row_ind, _data_point_col_ind, pointSelected, _use_texture, _preview);


        if(dlOn)
           _dl->Disable();
        else if(slOn)
           _sl->Disable();
        else if(plOn)
           _pl->Disable();
    }

    void GLWidget::_destroyBiquarticCompositeSurface()
    {
        if (_bcs)
        {
            delete _bcs;
            _bcs = nullptr;
        }
    }

    int GLWidget::_getMaterialIndex(Material *mat)
    {
        if (mat == &MatFBBrass)
            return 0;
        if (mat == &MatFBGold)
            return 1;
        if (mat == &MatFBSilver)
            return 2;
        if (mat == &MatFBEmerald)
            return 3;
        if (mat == &MatFBPearl)
            return 4;
        if (mat == &MatFBTurquoise)
            return 5;
        return 0;
    }

    void GLWidget::_installShaders()
    {
        if (!_shaders[0].InstallShaders("Shaders/directional_light.vert", "Shaders/directional_light.frag"))
            throw Exception("Could not install directional light shaders!");
        if (!_shaders[1].InstallShaders("Shaders/two_sided_lighting.vert", "Shaders/two_sided_lighting.frag"))
            throw Exception("Could not install two sided directional light shaders!");
        if (!_shaders[2].InstallShaders("Shaders/reflection_lines.vert", "Shaders/reflection_lines.frag"))
            throw Exception("Could not install reflection line shaders!");
        if (!_shaders[3].InstallShaders("Shaders/toon.vert", "Shaders/toon.frag"))
            throw Exception("Could not install toonifying shaders!");
    }

    void GLWidget::_initializeLights()
    {
        // _dl
        HCoordinate3 position(0.0f, 0.0f, 1.0f, 0.0f);
        Color4 ambient_intensity(0.4f, 0.4f, 0.4f, 1.0f);
        Color4 diffuse_intensity(0.8f, 0.8f, 0.8f, 1.0f);
        Color4 specular_intensity(1.0f, 1.0f, 1.0f, 1.0f);

        // _pl
        HCoordinate3 position_pl(3.0f, 1.0f, 5.0f, 1.0f);
        Color4 ambient_intensity_pl(1.0f, 1.0f, 0.0f, 1.0f);
        GLfloat constant_attenuation_pl = 1.0f;
        GLfloat linear_attenuation_pl = 0.02f;
        GLfloat quadratic_attenuation_pl = 0.03f;

        // _sl
        HCoordinate3 position_sl(0.0f, 0.0f, 3.0f, 1.0f);
        Color4 ambient_intensity_sl(0.0f, 0.6f, 0.2f, 1.0f);
        GLfloat constant_attenuation_sl = 1.0f;
        GLfloat linear_attenuation_sl = 0.02f;
        GLfloat quadratic_attenuation_sl = 0.03f;
        HCoordinate3 spot_direction(0.0f, 0.0f, -1.0f, 1.0f);
        GLfloat spot_cutoff = 30.0f;
        GLfloat spot_exponent = 0.0f;

        _dl = new DirectionalLight(
                    GL_LIGHT0,
                    position,
                    ambient_intensity,
                    diffuse_intensity,
                    specular_intensity
            );

        _pl = new PointLight(
                    GL_LIGHT1,
                    position_pl,
                    ambient_intensity_pl,
                    diffuse_intensity,
                    specular_intensity,
                    constant_attenuation_pl,
                    linear_attenuation_pl,
                    quadratic_attenuation_pl
            );
        _sl = new SpotLight(
                    GL_LIGHT2,
                    position_sl,
                    ambient_intensity_sl,
                    diffuse_intensity,
                    specular_intensity,
                    constant_attenuation_sl,
                    linear_attenuation_sl,
                    quadratic_attenuation_sl,
                    spot_direction,
                    spot_cutoff,
                    spot_exponent
            );

        _dl->Disable();
        _pl->Disable();
        _sl->Disable();
    }

    void GLWidget::_destroyLights()
    {
        if (_dl)
            delete _dl, _dl = nullptr;
        if (_pl)
            delete _pl, _pl = nullptr;
        if (_sl)
            delete _sl, _sl = nullptr;
    }

    bool GLWidget::_initializeTexture(const char* fname)
    {
        QImage img(fname);

        if (img.isNull())
            return false;

        const GLuint h = img.height();
        const GLuint w = img.width();

        const uchar *imgbits = img.bits();

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

        glGenTextures(1, &_texName);
        glBindTexture(GL_TEXTURE_2D, _texName);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, imgbits);

        return true;
    }

    // file handling

    void GLWidget::save()
    {
        if (_geometry_type == 0)
        {
            QString fileName = QFileDialog::getSaveFileName(this,
                   tr("Save Composite Curve"), "",
                   tr("Composite Curve (*.compc)"));
            if (!fileName.isEmpty())
            {
                _bcc->saveCurve(fileName.toStdString());
            }
            return;
        }

        QString fileName = QFileDialog::getSaveFileName(this,
               tr("Save Composite Surface"), "",
               tr("Composite Surface (*.comps)"));
        if (!fileName.isEmpty())
        {
            _bcs->saveSurface(fileName.toStdString());
        }
    }

    void GLWidget::open()
    {
        if (_geometry_type == 0)
        {
            QString fileName = QFileDialog::getOpenFileName(this,
                   tr("Load Composite Curve"), "",
                   tr("Composite Curve (*.compc)"));
            if (!fileName.isEmpty())
            {
                pointSelected = false;
                _bcc->loadCurve(fileName.toStdString());

                GLdouble r = 0.0, g = 0.0, b = 0.0;
                _bcc->getColorComponents(_arc_ind, r, g, b);

                emit color_has_changed_r(r * 100);
                emit color_has_changed_g(g * 100);
                emit color_has_changed_b(b * 100);

                updateGL();
            }
            return;
        }

        QString fileName = QFileDialog::getOpenFileName(this,
               tr("Load Composite Surface"), "",
               tr("Composite Surface (*.comps)"));
        if (!fileName.isEmpty())
        {
            pointSelected = false;
            _bcs->loadSurface(fileName.toStdString());

            Material* mat = _bcs->getMaterial(_patch_ind);
            int ind = _getMaterialIndex(mat);
            emit material_changed(ind);

            updateGL();
        }
    }

    // mouse events

    void GLWidget::mousePressEvent(QMouseEvent *event)
    {
        event->accept();

        if (event->button() == Qt::LeftButton)
        {
            GLint viewport[4];
            glGetIntegerv(GL_VIEWPORT, viewport);

            // curves
            GLuint arc_count = _bcc->getArcCount();
            if (_geometry_type == 0 && _render_data && arc_count > 0)
            {
                GLuint size = 4 * arc_count;
                GLuint *pick_buffer = new GLuint[size];
                glSelectBuffer(size, pick_buffer);

                glRenderMode(GL_SELECT);

                glInitNames();
                glPushName(0);

                GLfloat projection_matrix[16];
                glGetFloatv(GL_PROJECTION_MATRIX, projection_matrix);

                glMatrixMode(GL_PROJECTION);

                glPushMatrix();

                glLoadIdentity();
                gluPickMatrix((GLdouble)event->x(), (GLdouble)(viewport[3] - event->y()), 5.0, 5.0, viewport);

                glMultMatrixf(projection_matrix);

                glMatrixMode(GL_MODELVIEW);

                glPushMatrix();

                    // rotating around the coordinate axes
                    glRotatef(_angle_x, 1.0, 0.0, 0.0);
                    glRotatef(_angle_y, 0.0, 1.0, 0.0);
                    glRotatef(_angle_z, 0.0, 0.0, 1.0);

                    // translate
                    glTranslated(_trans_x, _trans_y, _trans_z);

                    // scaling
                    glScalef(_zoom, _zoom, _zoom);

                    // render only the clickable geometries
                    _bcc->renderClickableObjects();

                glPopMatrix();

                glMatrixMode(GL_PROJECTION);
                glPopMatrix();

                glMatrixMode(GL_MODELVIEW);

                int hit_count = glRenderMode(GL_RENDER);

                if (hit_count > 0)
                {
                    GLuint closest_selected = pick_buffer[3];
                    GLuint closest_depth    = pick_buffer[1];

                    for (int i = 1; i < hit_count; ++i)
                    {
                        GLuint offset = i * 4;
                        if (pick_buffer[offset + 1] < closest_depth)
                        {
                            closest_selected = pick_buffer[offset + 3];
                            closest_depth    = pick_buffer[offset + 1];
                        }
                    }

                    int old_arc_ind = _arc_ind;

                    _arc_ind = closest_selected / 4;
                    _data_point_ind = closest_selected % 4;

                    if (old_arc_ind != _arc_ind)
                    {
                        emit arc_changed(_arc_ind);

                        GLdouble r = 0.0, g = 0.0, b = 0.0;
                        _bcc->getColorComponents(_arc_ind, r, g, b);

                        emit color_has_changed_r(r * 100);
                        emit color_has_changed_g(g * 100);
                        emit color_has_changed_b(b * 100);
                    }

                    emit point_changed(_data_point_ind);
                    pointSelected = true;
                }
                else
                {
                    _arc_ind = -1;
                    emit arc_changed(_arc_ind);
                    if (_data_point_ind != 0)
                    {
                        _data_point_ind = 0;
                        emit point_changed(0);
                    }
                    pointSelected = false;
                }

                delete[] pick_buffer;
            }
            // surfaces
            else if (_geometry_type != 0 && _render_control_net)
            {
                GLuint size = 16 * _bcs->getPatchCount();
                GLuint *pick_buffer = new GLuint[size];
                glSelectBuffer(size, pick_buffer);

                glRenderMode(GL_SELECT);

                glInitNames();
                glPushName(0);

                GLfloat projection_matrix[16];
                glGetFloatv(GL_PROJECTION_MATRIX, projection_matrix);

                glMatrixMode(GL_PROJECTION);

                glPushMatrix();

                glLoadIdentity();
                gluPickMatrix((GLdouble)event->x(), (GLdouble)(viewport[3] - event->y()), 5.0, 5.0, viewport);

                glMultMatrixf(projection_matrix);

                glMatrixMode(GL_MODELVIEW);

                glPushMatrix();

                    // rotating around the coordinate axes
                    glRotatef(_angle_x, 1.0, 0.0, 0.0);
                    glRotatef(_angle_y, 0.0, 1.0, 0.0);
                    glRotatef(_angle_z, 0.0, 0.0, 1.0);

                    // translate
                    glTranslated(_trans_x, _trans_y, _trans_z);

                    // scaling
                    glScalef(_zoom, _zoom, _zoom);

                    // render only the clickable geometries
                    _bcs->renderClickableObjects();

                glPopMatrix();

                glMatrixMode(GL_PROJECTION);
                glPopMatrix();

                glMatrixMode(GL_MODELVIEW);

                int hit_count = glRenderMode(GL_RENDER);

                if (hit_count > 0)
                {
                    GLuint closest_selected = pick_buffer[3];
                    GLuint closest_depth    = pick_buffer[1];


                    for (int i = 1; i < hit_count; ++i)
                    {
                        GLuint offset = i * 4;
                        if (pick_buffer[offset + 1] < closest_depth)
                        {
                            closest_selected = pick_buffer[offset + 3];
                            closest_depth    = pick_buffer[offset + 1];
                        }
                    }

                    int old_patch_ind = _patch_ind;

                    _patch_ind = closest_selected / 16;
                    int point = closest_selected % 16;

                    _data_point_row_ind = point / 4;
                    _data_point_col_ind = point % 4;

                    if (old_patch_ind != _patch_ind)
                    {
                        emit patch_changed(_patch_ind);

                        Material* mat = _bcs->getMaterial(_patch_ind);
                        int ind = _getMaterialIndex(mat);
                        emit material_changed(ind);
                    }

                    emit row_changed(_data_point_row_ind);
                    emit col_changed(_data_point_col_ind);

                    pointSelected = true;
                }
                else
                {
                    _patch_ind = -1;
                    emit patch_changed(_patch_ind);

                    _data_point_row_ind = 0;
                    _data_point_col_ind = 0;

                    emit row_changed(_data_point_row_ind);
                    emit col_changed(_data_point_col_ind);

                    pointSelected = false;
                }

                delete[] pick_buffer;
            }

            updateGL();
        }
    }

    void GLWidget::wheelEvent(QWheelEvent *event)
    {
        event->accept();

        if (pointSelected)
        {
            // curves
            if (_geometry_type == 0 && _render_data)
            {
                // wheel + Ctrl
                if (event->modifiers() & Qt::ControlModifier)
                {
                    _bcc->addToDataPointValue(_arc_ind, _data_point_ind, 0, event->delta() / 120.0 * _reposition_unit);
                }

                // wheel + Alt
                if (event->modifiers() & Qt::AltModifier)
                {
                    _bcc->addToDataPointValue(_arc_ind, _data_point_ind, 1, event->delta() / 120.0 * _reposition_unit);
                }

                // wheel + Shift
                if (event->modifiers() & Qt::ShiftModifier)
                {
                    _bcc->addToDataPointValue(_arc_ind, _data_point_ind, 2, event->delta() / 120.0 * _reposition_unit);
                }
            }
            // surfaces
            else if (_geometry_type != 0 && _render_control_net)
            {
                // wheel + Ctrl
                if (event->modifiers() & Qt::ControlModifier)
                {
                    _bcs->addToDataPointValue(_patch_ind, _data_point_row_ind, _data_point_col_ind, 0, event->delta() / 120.0 * _reposition_unit);
                }

                // wheel + Alt
                if (event->modifiers() & Qt::AltModifier)
                {
                    _bcs->addToDataPointValue(_patch_ind, _data_point_row_ind, _data_point_col_ind, 1, event->delta() / 120.0 * _reposition_unit);
                }

                // wheel + Shift
                if (event->modifiers() & Qt::ShiftModifier)
                {
                    _bcs->addToDataPointValue(_patch_ind, _data_point_row_ind, _data_point_col_ind, 2, event->delta() / 120.0 * _reposition_unit);
                }
            }
            updateGL();
        }
    }
}
