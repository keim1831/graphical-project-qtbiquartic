#pragma once

#include "Colors4.h"
#include "HCoordinates3.h"
#include <GL/glew.h>

namespace cagd
{
    class DirectionalLight
    {
    protected:
        GLenum       _light_index;
        HCoordinate3 _position;
        Color4       _ambient_intensity, _diffuse_intensity, _specular_intensity;
    public:
        DirectionalLight(
            GLenum              light_index,
            const HCoordinate3& position,
            const Color4&       ambient_intensity,
            const Color4&       diffuse_intensity,
            const Color4&       specular_intensity);

        void Enable();
        void Disable();

        bool set_position_x(GLfloat val);
        bool set_position_y(GLfloat val);
        bool set_position_z(GLfloat val);
        bool set_position_w(GLfloat val);

        bool set_ambient_r(GLfloat val);
        bool set_ambient_g(GLfloat val);
        bool set_ambient_b(GLfloat val);
        bool set_ambient_a(GLfloat val);

        bool set_diffuse_r(GLfloat val);
        bool set_diffuse_g(GLfloat val);
        bool set_diffuse_b(GLfloat val);
        bool set_diffuse_a(GLfloat val);

        bool set_specular_r(GLfloat val);
        bool set_specular_g(GLfloat val);
        bool set_specular_b(GLfloat val);
        bool set_specular_a(GLfloat val);

        GLfloat get_position_x();
        GLfloat get_position_y();
        GLfloat get_position_z();
        GLfloat get_position_w();

        GLfloat get_ambient_r();
        GLfloat get_ambient_g();
        GLfloat get_ambient_b();
        GLfloat get_ambient_a();

        GLfloat get_diffuse_r();
        GLfloat get_diffuse_g();
        GLfloat get_diffuse_b();
        GLfloat get_diffuse_a();

        GLfloat get_specular_r();
        GLfloat get_specular_g();
        GLfloat get_specular_b();
        GLfloat get_specular_a();
    };

    class PointLight: public DirectionalLight
    {
    protected:
       GLfloat _constant_attenuation,
                _linear_attenuation,
                _quadratic_attenuation;
    public:
        PointLight(
            GLenum              light_index,
            const HCoordinate3& position,
            const Color4&       ambient_intensity,
            const Color4&       diffuse_intensity,
            const Color4&       specular_intensity,
            GLfloat             constant_attenuation,
            GLfloat             linear_attenuation,
            GLfloat             quadratic_attenuation);

        bool set_constant_attenuation(GLfloat val);
        bool set_linear_attenuation(GLfloat val);
        bool set_quadratic_attenuation(GLfloat val);

        GLfloat get_constant_attenuation();
        GLfloat get_linear_attenuation();
        GLfloat get_quadratic_attenuation();
    };

    class SpotLight: public PointLight
    {
    protected:
        HCoordinate3 _spot_direction;
        GLfloat      _spot_cutoff, _spot_exponent;
    public:
        SpotLight(
            GLenum              light_index,
            const HCoordinate3& position,
            const Color4&       ambient_intensity,
            const Color4&       diffuse_intensity,
            const Color4&       specular_intensity,
            GLfloat             constant_attenuation,
            GLfloat             linear_attenuation,
            GLfloat             quadratic_attenuation,
            const HCoordinate3& spot_direction,
            GLfloat             spot_cutoff,
            GLfloat             spot_exponent);

        bool set_spot_direction_x(GLfloat val);
        bool set_spot_direction_y(GLfloat val);
        bool set_spot_direction_z(GLfloat val);
        bool set_spot_direction_w(GLfloat val);
        bool set_spot_cutoff(GLfloat val);
        bool set_spot_exponent(GLfloat val);

        GLfloat get_spot_direction_x();
        GLfloat get_spot_direction_y();
        GLfloat get_spot_direction_z();
        GLfloat get_spot_direction_w();
        GLfloat get_spot_cutoff();
        GLfloat get_spot_exponent();
    };
}
