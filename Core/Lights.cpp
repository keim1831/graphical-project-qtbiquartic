#include "Exceptions.h"
#include "Lights.h"

using namespace cagd;

// directional light
DirectionalLight::DirectionalLight(
    GLenum              light_index,
    const HCoordinate3& position,
    const Color4&       ambient_intensity,
    const Color4&       diffuse_intensity,
    const Color4&       specular_intensity):

    _light_index(light_index),
    _position(position),
    _ambient_intensity(ambient_intensity),
    _diffuse_intensity(diffuse_intensity),
    _specular_intensity(specular_intensity)
{
    glLightfv(light_index, GL_POSITION, &_position.x());
    glLightfv(light_index, GL_AMBIENT,  &_ambient_intensity.r());
    glLightfv(light_index, GL_DIFFUSE,  &_diffuse_intensity.r());
    glLightfv(light_index, GL_SPECULAR, &_specular_intensity.r());
}

void DirectionalLight::Enable()
{
    glEnable(_light_index);
}

void DirectionalLight::Disable()
{
    glDisable(_light_index);
}

bool DirectionalLight::set_position_x(GLfloat val)
{
    if (_position.x() != val)
    {
        _position.x() = val;
        glLightfv(_light_index, GL_POSITION, &_position.x());
        return true;
    }
    return false;
}

bool DirectionalLight::set_position_y(GLfloat val)
{
    if (_position.y() != val)
    {
        _position.y() = val;
        glLightfv(_light_index, GL_POSITION, &_position.x());
        return true;
    }
    return false;
}

bool DirectionalLight::set_position_z(GLfloat val)
{
    if (_position.z() != val)
    {
        _position.z() = val;
        glLightfv(_light_index, GL_POSITION, &_position.x());
        return true;
    }
    return false;
}

bool DirectionalLight::set_position_w(GLfloat val)
{
    if (_position.w() != val)
    {
        _position.w() = val;
        glLightfv(_light_index, GL_POSITION, &_position.x());
        return true;
    }
    return false;
}

bool DirectionalLight::set_ambient_r(GLfloat val)
{
    if (_ambient_intensity.r() != val)
    {
        _ambient_intensity.r() = val;
        glLightfv(_light_index, GL_AMBIENT,  &_ambient_intensity.r());
        return true;
    }
    return false;
}

bool DirectionalLight::set_ambient_g(GLfloat val)
{
    if (_ambient_intensity.g() != val)
    {
        _ambient_intensity.g() = val;
        glLightfv(_light_index, GL_AMBIENT,  &_ambient_intensity.r());
        return true;
    }
    return false;
}

bool DirectionalLight::set_ambient_b(GLfloat val)
{
    if (_ambient_intensity.b() != val)
    {
        _ambient_intensity.b() = val;
        glLightfv(_light_index, GL_AMBIENT,  &_ambient_intensity.r());
        return true;
    }
    return false;
}

bool DirectionalLight::set_ambient_a(GLfloat val)
{
    if (_ambient_intensity.a() != val)
    {
        _ambient_intensity.a() = val;
        glLightfv(_light_index, GL_AMBIENT,  &_ambient_intensity.r());
        return true;
    }
    return false;
}

bool DirectionalLight::set_diffuse_r(GLfloat val)
{
    if (_diffuse_intensity.r() != val)
    {
        _diffuse_intensity.r() = val;
        glLightfv(_light_index, GL_DIFFUSE,  &_diffuse_intensity.r());
        return true;
    }
    return false;
}

bool DirectionalLight::set_diffuse_g(GLfloat val)
{
    if (_diffuse_intensity.g() != val)
    {
        _diffuse_intensity.g() = val;
        glLightfv(_light_index, GL_DIFFUSE,  &_diffuse_intensity.r());
        return true;
    }
    return false;
}

bool DirectionalLight::set_diffuse_b(GLfloat val)
{
    if (_diffuse_intensity.b() != val)
    {
        _diffuse_intensity.b() = val;
        glLightfv(_light_index, GL_DIFFUSE,  &_diffuse_intensity.r());
        return true;
    }
    return false;
}

bool DirectionalLight::set_diffuse_a(GLfloat val)
{
    if (_diffuse_intensity.a() != val)
    {
        _diffuse_intensity.a() = val;
        glLightfv(_light_index, GL_DIFFUSE,  &_diffuse_intensity.r());
        return true;
    }
    return false;
}

bool DirectionalLight::set_specular_r(GLfloat val)
{
    if (_specular_intensity.r() != val)
    {
        _specular_intensity.r() = val;
        glLightfv(_light_index, GL_SPECULAR, &_specular_intensity.r());
        return true;
    }
    return false;
}

bool DirectionalLight::set_specular_g(GLfloat val)
{
    if (_specular_intensity.g() != val)
    {
        _specular_intensity.g() = val;
        glLightfv(_light_index, GL_SPECULAR, &_specular_intensity.r());
        return true;
    }
    return false;
}

bool DirectionalLight::set_specular_b(GLfloat val)
{
    if (_specular_intensity.b() != val)
    {
        _specular_intensity.b() = val;
        glLightfv(_light_index, GL_SPECULAR, &_specular_intensity.r());
        return true;
    }
    return false;
}

bool DirectionalLight::set_specular_a(GLfloat val)
{
    if (_specular_intensity.a() != val)
    {
        _specular_intensity.a() = val;
        glLightfv(_light_index, GL_SPECULAR, &_specular_intensity.r());
        return true;
    }
    return false;
}

GLfloat DirectionalLight::get_position_x()
{
    return _position.x();
}

GLfloat DirectionalLight::get_position_y()
{
   return _position.y();
}

GLfloat DirectionalLight::get_position_z()
{
    return _position.z();
}

GLfloat DirectionalLight::get_position_w()
{
    return _position.w();
}

GLfloat DirectionalLight::get_ambient_r()
{
    return _ambient_intensity.r();
}

GLfloat DirectionalLight::get_ambient_g()
{
    return _ambient_intensity.g();
}

GLfloat DirectionalLight::get_ambient_b()
{
    return _ambient_intensity.b();
}

GLfloat DirectionalLight::get_ambient_a()
{
    return _ambient_intensity.a();
}

GLfloat DirectionalLight::get_diffuse_r()
{
    return _diffuse_intensity.r();
}

GLfloat DirectionalLight::get_diffuse_g()
{
    return _diffuse_intensity.g();
}

GLfloat DirectionalLight::get_diffuse_b()
{
    return _diffuse_intensity.b();
}

GLfloat DirectionalLight::get_diffuse_a()
{
    return _diffuse_intensity.a();
}

GLfloat DirectionalLight::get_specular_r()
{
    return _specular_intensity.r();
}

GLfloat DirectionalLight::get_specular_g()
{
    return _specular_intensity.g();
}

GLfloat DirectionalLight::get_specular_b()
{
    return _specular_intensity.b();
}

GLfloat DirectionalLight::get_specular_a()
{
    return _specular_intensity.a();
}

// point light
PointLight::PointLight(
    GLenum              light_index,
    const HCoordinate3& position,
    const Color4&       ambient_intensity,
    const Color4&       diffuse_intensity,
    const Color4&       specular_intensity,
    GLfloat             constant_attenuation,
    GLfloat             linear_attenuation,
    GLfloat             quadratic_attenuation):

    DirectionalLight(
            light_index,
            position,
            ambient_intensity, diffuse_intensity, specular_intensity),
    _constant_attenuation(constant_attenuation),
    _linear_attenuation(linear_attenuation),
    _quadratic_attenuation(quadratic_attenuation)
{
    if (position.w() == 0.0)
        throw Exception("PointLight::PointLight - Wrong position.");

    glLightf(_light_index, GL_SPOT_CUTOFF, 180.0);
    glLightf(_light_index, GL_CONSTANT_ATTENUATION,  _constant_attenuation);
    glLightf(_light_index, GL_LINEAR_ATTENUATION,    _linear_attenuation);
    glLightf(_light_index, GL_QUADRATIC_ATTENUATION, _quadratic_attenuation);
}

bool PointLight::set_constant_attenuation(GLfloat val)
{
    if (_constant_attenuation != val)
    {
        _constant_attenuation = val;
        glLightf(_light_index, GL_CONSTANT_ATTENUATION,  _constant_attenuation);
        return true;
    }
    return false;
}

bool PointLight::set_linear_attenuation(GLfloat val)
{
    if (_linear_attenuation != val)
    {
        _linear_attenuation = val;
        glLightf(_light_index, GL_LINEAR_ATTENUATION,    _linear_attenuation);
        return true;
    }
    return false;
}

bool PointLight::set_quadratic_attenuation(GLfloat val)
{
    if (_quadratic_attenuation != val)
    {
        _quadratic_attenuation = val;
        glLightf(_light_index, GL_QUADRATIC_ATTENUATION, _quadratic_attenuation);
        return true;
    }
    return false;
}

GLfloat PointLight::get_constant_attenuation()
{
    return _constant_attenuation;
}

GLfloat PointLight::get_linear_attenuation()
{
    return _linear_attenuation;
}

GLfloat PointLight::get_quadratic_attenuation()
{
    return _quadratic_attenuation;
}

// spotlight
SpotLight::SpotLight(
    GLenum              light_index,
    const HCoordinate3& position,
    const Color4&       ambient_intensity,
    const Color4&       diffuse_intensity,
    const Color4&       specular_intensity,
    GLfloat             constant_attenuation,
    GLfloat             linear_attenuation,
    GLfloat             quadratic_attenuation,
    const HCoordinate3& spot_direction,
    GLfloat             spot_cutoff,
    GLfloat             spot_exponent):
    PointLight(
            light_index, position,
            ambient_intensity, diffuse_intensity, specular_intensity,
            constant_attenuation, linear_attenuation, quadratic_attenuation),
    _spot_direction(spot_direction),
    _spot_cutoff(spot_cutoff),
    _spot_exponent(spot_exponent)
{
    if (position.w() == 0.0)
        throw Exception("Spotlight::Spotlight - Wrong position.");

    if (_spot_cutoff > 90.0)
        throw Exception("Spotlight::Spotlight - Wrong spot cutoff.");

    glLightfv(_light_index, GL_SPOT_DIRECTION,	&_spot_direction.x());
    glLightf (_light_index, GL_SPOT_CUTOFF,     _spot_cutoff);
    glLightf (_light_index, GL_SPOT_EXPONENT,	_spot_exponent);
}

bool SpotLight::set_spot_direction_x(GLfloat val)
{
    if (_spot_direction.x() != val)
    {
        _spot_direction.x() = val;
        glLightfv(_light_index, GL_SPOT_DIRECTION,	&_spot_direction.x());
        return true;
    }
    return false;
}

bool SpotLight::set_spot_direction_y(GLfloat val)
{
    if (_spot_direction.y() != val)
    {
        _spot_direction.y() = val;
        glLightfv(_light_index, GL_SPOT_DIRECTION,	&_spot_direction.x());
        return true;
    }
    return false;
}

bool SpotLight::set_spot_direction_z(GLfloat val)
{
    if (_spot_direction.z() != val)
    {
        _spot_direction.z() = val;
        glLightfv(_light_index, GL_SPOT_DIRECTION, &_spot_direction.x());
        return true;
    }
    return false;
}
bool SpotLight::set_spot_direction_w(GLfloat val)
{
    if (_spot_direction.w() != val)
    {
        _spot_direction.w() = val;
        glLightfv(_light_index, GL_SPOT_DIRECTION, &_spot_direction.x());
        return true;
    }
    return false;
}

bool SpotLight::set_spot_cutoff(GLfloat val)
{
    if (_spot_cutoff != val)
    {
        _spot_cutoff = val;
        glLightf (_light_index, GL_SPOT_CUTOFF, _spot_cutoff);
        return true;
    }
    return false;
}

bool SpotLight::set_spot_exponent(GLfloat val)
{
    if (_spot_exponent != val)
    {
        _spot_exponent = val;
        glLightf (_light_index, GL_SPOT_EXPONENT, _spot_exponent);
        return true;
    }
    return false;
}

GLfloat SpotLight::get_spot_direction_x()
{
    return _spot_direction.x();
}

GLfloat SpotLight::get_spot_direction_y()
{
    return _spot_direction.y();
}

GLfloat SpotLight::get_spot_direction_z()
{
    return _spot_direction.z();
}

GLfloat SpotLight::get_spot_direction_w()
{
    return _spot_direction.w();
}

GLfloat SpotLight::get_spot_cutoff()
{
    return _spot_cutoff;
}

GLfloat SpotLight::get_spot_exponent()
{
    return _spot_exponent;
}
